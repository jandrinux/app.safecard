<?php
App::uses('AppModel', 'Model');
/**
 * Notification Model
 *
 */
class Notification extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
