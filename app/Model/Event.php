<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 * @property User $User
 * @property Invitation $Invitation
 */
class Event extends AppModel {
    
    
    public function beforeSave($options = array()) {
        /*
        if(isset($this->data[$this->alias]['dfrom']) && isset($this->data[$this->alias]['hfrom'])){
            $this->data[$this->alias]['from'] = date('Y-m-d H:i:s', strtotime($this->data[$this->alias]['dfrom'].' '.$this->data[$this->alias]['hfrom']));
        }
        
        if(isset($this->data[$this->alias]['dto']) && isset($this->data[$this->alias]['hto'])){            
            $this->data[$this->alias]['to'] = date('Y-m-d H:i:s', strtotime($this->data[$this->alias]['dto'].' '.$this->data[$this->alias]['hto']));
        }
        return strtotime($this->data[$this->alias]['from']) < strtotime($this->data[$this->alias]['to']); */
        
        return true;
    }
	
	public $inserted_ids = array();
	
	function afterSave($created) {
        if($created) {
            $this->inserted_ids[] = $this->getInsertID();
        }
        return true;
    }
    
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'subject';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
		'from' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Debe ingresar una fecha valida',
			),
		),
		'to' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Debe ingresar una fecha valida',
			),
			'compareDates' => array(
				'rule' => array('compareDates'),
				'message' => 'Rango de fechas invalido.',
			)
		)
	);
        
	public function compareDates() { 
		return strtotime($this->data[$this->alias]['from']) < strtotime($this->data[$this->alias]['to']); 
	} 
        
        
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'House' => array(
			'className' => 'House',
			'foreignKey' => 'house_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Invitation' => array(
			'className' => 'Invitation',
			'foreignKey' => 'event_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	public $hasAndBelongsToMany = array(
		'Day' => array(
			'className' => 'Day',
			'joinTable' => 'days_events',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'day_id',
			'unique' => 'keepExisting',
			'with' => 'DaysEvent',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
	);
        
	public function totalEventosByHouse($house_id){
		return $this->find('count', array('conditions'=>array('house_id'=>$house_id)));
	}

}
