<?php
App::uses('AppModel', 'Model');
/**
 * Gate Model
 *
 * @property Condo $Condo
 */
class Student extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'full_name';

	public $belongsTo = array(
		'School' => array(
			'className' => 'Condo',
			'joinTable' => 'condo',
			'foreignKey' => 'school_id'
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'houses_users',
			'foreignKey' => 'student_id',
			'associationForeignKey' => 'user_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
}
