<?php
App::uses('AppModel', 'Model');
/**
 * Contact Model
 *
 * @property Organizer $Organizer
 * @property Contact $Contact
 * @property Type $Type
 */
class Contact extends AppModel {

/**
 * Display field
 *
 * @var string
 */
   // public $displayField = 'name';
    public $displayField = "full_name"; 
    public $actsAs = array(
        'Containable',
        'MultipleDisplayFields' => array( 
            'fields' => array('name', 'father_surname'), 
            'pattern' => '%s %s' 
    )); 

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'organizer_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar nombre',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'mobile' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar su celular',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Organizer' => array(
			'className' => 'User',
			'foreignKey' => 'organizer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Relation' => array(
			'className' => 'Relation',
			'foreignKey' => 'relation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
        
	public $hasMany = array('Invitation');
}
