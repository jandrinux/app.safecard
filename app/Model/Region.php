<?php
App::uses('AppModel', 'Model');
/**
 * Region Model
 *
 * @property Comuna $Comuna
 */
class Region extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'glosa';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Comuna' => array(
			'className' => 'Comuna',
			'foreignKey' => 'region_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
