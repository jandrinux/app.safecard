<?php
App::uses('AppModel', 'Model');
/**
 * Comuna Model
 *
 * @property Region $Region
 * @property Condo $Condo
 */
class Comuna extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'glosa';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Region' => array(
			'className' => 'Region',
			'foreignKey' => 'region_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Condo' => array(
			'className' => 'Condo',
			'foreignKey' => 'comuna_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
