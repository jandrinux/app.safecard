<?php
App::uses('AppModel', 'Model');
/**
 * House Model
 *
 * @property Condo $Condo
 * @property User $User
 */
class House extends AppModel {
    
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'condo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe seleccionar un condominio',
			),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Indique nombre',
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Condo' => array(
			'className' => 'Condo',
			'foreignKey' => 'condo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		) 
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'houses_users',
			'foreignKey' => 'house_id',
			'associationForeignKey' => 'user_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
	
	
	public function getCondominioByHouse($house_id){
		$this->id = $house_id;
		$house = $this->read(null,$this->id);
		return $house;
	}
}
