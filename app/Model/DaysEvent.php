<?php
App::uses('AppModel', 'Model');
/**
 * DaysEvent Model
 *
 * @property Day $Day
 * @property Event $Event
 */
class DaysEvent extends AppModel {
	
	public function beforeSave($options = array()) {
		
        if(isset($this->data[$this->alias]['hfrom'])){
            $this->data[$this->alias]['from'] = date('H:i:s', strtotime($this->data[$this->alias]['hfrom']));
        }
        
        if(isset($this->data[$this->alias]['hto'])){            
            $this->data[$this->alias]['to'] = date('H:i:s', strtotime($this->data[$this->alias]['hto']));
        }
        if(isset($this->data[$this->alias]['hfrom']) && isset($this->data[$this->alias]['hto'])){
			return strtotime($this->data[$this->alias]['hfrom']) < strtotime($this->data[$this->alias]['hto']); 
		} else {
			return true;
		}
        
        //return true;
    }
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Day' => array(
			'className' => 'Day',
			'foreignKey' => 'day_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Event' => array(
			'className' => 'Event',
			'foreignKey' => 'event_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
