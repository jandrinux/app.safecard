<?php
App::uses('AppModel', 'Model');
/**
 * Manual Model
 *
 */
class Manual extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Invitation' => array(
			'className' => 'Invitation',
			'foreignKey' => 'invitation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

