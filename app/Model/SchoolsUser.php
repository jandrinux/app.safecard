<?php
App::uses('AppModel', 'Model');
/**
 * HousesUser Model
 *
 * @property User $User
 * @property Relation $Relation
 * @property House $House
 * @property Responsible $Responsible
 */
class SchoolsUser extends AppModel {

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        
}
