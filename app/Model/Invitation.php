<?php
App::uses('AppModel', 'Model');
App::import('Vendor', 'AES');
/**
 * Invitation Model
 *
 * @property Event $Event
 * @property Organizer $Organizer
 * @property Guest $Guest
 */
class Invitation extends AppModel {

	public $inserted_ids = array();
	
	function afterSave($created) {
        if($created) {
            $this->inserted_ids[] = $this->getInsertID();
        }
        return true;
    }
	
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'event_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'event_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'organizer_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	
	public $hasMany = array('Log');
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Event' => array(
			'className' => 'Event',
			'foreignKey' => 'event_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Organizer' => array(
			'className' => 'User',
			'foreignKey' => 'organizer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Guest' => array(
			'className' => 'User',
			'foreignKey' => 'guest_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Contact' => array(
			'className' => 'Contact',
			'foreignKey' => 'contact_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Condo' => array(
			'className' => 'Condo',
			'foreignKey' => 'condo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'House' => array(
			'className' => 'House',
			'foreignKey' => 'house_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Student' => array(
			'className' => 'Student',
			'foreignKey' => 'student_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Relation' => array(
			'className' => 'Relation',
			'foreignKey' => 'relation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
	
	public function saveCode($invitation_id){
		$this->id = $invitation_id;
		$invitation = $this->read(null,$this->id);
		
		$name_guest = $mobile_guest = '';
		if(is_null($invitation['Guest']['mobile']) || $invitation['Guest']['mobile'] == ''){
			$name_guest = $invitation['Contact']['name'];
			$mobile_guest = $invitation['Contact']['mobile'];
		} else {
			$name_guest = $invitation['Guest']['name'].' '.$invitation['Guest']['father_surname'];
			$mobile_guest = $invitation['Guest']['mobile'];
		}
		if($invitation['Invitation']['relation_id']!= 1 && $invitation['Invitation']['relation_id']!= 2){
			$hours = ($invitation['Condo']['gmt']>0)? '-'.$invitation['Condo']['gmt'].' hour' : '+'.($invitation['Condo']['gmt']*-1).' hour';
			$start_date = date('ymdHi',strtotime($hours, strtotime($invitation['Event']['from'])));
			$end_date = date('ymdHi',strtotime($hours, strtotime($invitation['Event']['to'])));
		} else {
			$start_date = date('ymdHi', strtotime($invitation['Event']['from']));
			$end_date = date('ymdHi',strtotime($invitation['Event']['to']));
		}
		
		
		if(is_null($invitation['Event']['from'])){
			$start_date = "null";
		}
		if(is_null($invitation['Event']['to'])){
			$end_date = "null";
		}
		
		if(!is_null($invitation['Invitation']['house_id'])){
			$house_name = $invitation['House']['name'];
			$id_recinto = 1;
			$num_acceso = 0;
		} else {
			$house_name = $invitation['Student']['full_name'];
			$id_recinto = 3;
			$num_acceso = $invitation['Student']['stdschool_id'];
		}
		
		//date('ymdHi',strtotime($invitation['Event']['from'])).','.date('ymdHi',strtotime($invitation['Event']['to']))
		
		// $code = $this->id.','.$invitation['Invitation']['accessed'].',V,Q,'.str_pad($invitation['Condo']['id'],5,"0",STR_PAD_LEFT).'1,'.$invitation['Condo']['name'].','.$start_date.','.$end_date.','.$invitation['House']['name'].',0000000000,'.$invitation['Organizer']['mobile'].','.$invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname'].','.$mobile_guest.','.$name_guest.','.$invitation['Relation']['name'].',0,VIP,0';
		$code = stripAccents($this->id.','.$invitation['Invitation']['accessed'].',V,Q,'.$invitation['Condo']['id'].$id_recinto.',C,'.$start_date.','.$end_date.','.$house_name.','.$num_acceso.','.$invitation['Organizer']['mobile'].','.$invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname'].','.$mobile_guest.','.$name_guest.','.$invitation['Relation']['name'].',0,VIP,0');
		
		$aes = new AES($code, Configure::read('Security.passQR'), 128);
		$enc = $aes->encrypt();
		
		$QrXn = "QrX1";
		if($invitation['Invitation']['relation_id'] == 1){
			$QrXn = "QrX0";
		}
		
		if($this->saveField('aes',$QrXn.",".$enc)){
			$this->saveField('code',$code);
			return true;
		}
		return false;
	}
	
	public function saveInvalidCode($invitation_id){
		$this->id = $invitation_id;
		$invitation = $this->read(null,$this->id);
		
		$name_guest = $mobile_guest = '';
		if(is_null($invitation['Guest']['mobile']) || $invitation['Guest']['mobile'] == ''){
			$name_guest = $invitation['Contact']['name'];
			$mobile_guest = $invitation['Contact']['mobile'];
		} else {
			$name_guest = $invitation['Guest']['name'].' '.$invitation['Guest']['father_surname'];
			$mobile_guest = $invitation['Guest']['mobile'];
		}
		
		if(!is_null($invitation['Invitation']['house_id'])){
			$house_name = $invitation['House']['name'];
			$id_recinto = 1;
			$num_acceso = 0;
		} else {
			$house_name = $invitation['Student']['full_name'];
			$id_recinto = 3;
			$num_acceso = $invitation['Student']['stdschool_id'];
		}
		
		//$code = $this->id.',I,Q,'.$invitation['Condo']['id'].','.$invitation['Condo']['name'].','.date('ymdHi',strtotime($invitation['Event']['from'])).','.date('ymdHi',strtotime($invitation['Event']['to'])).','.$invitation['House']['name'].','.$invitation['Organizer']['mobile'].','.$invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname'].','.$invitation['Guest']['mobile'].','.$invitation['Guest']['name'].' '.$invitation['Guest']['father_surname'];
		// $code = $this->id.','.$invitation['Invitation']['accessed'].',I,Q,'.str_pad($invitation['Condo']['id'],5,"0",STR_PAD_LEFT).'1,'.$invitation['Condo']['name'].','.date('ymdHi',strtotime($invitation['Event']['from'])).','.date('ymdHi',strtotime($invitation['Event']['to'])).','.$invitation['House']['name'].',0000000000,'.$invitation['Organizer']['mobile'].','.$invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname'].','.$mobile_guest.','.$name_guest.','.$invitation['Relation']['name'].',0,VIP,0';
		$code = stripAccents($this->id.','.$invitation['Invitation']['accessed'].',I,Q,'.$invitation['Condo']['id'].$id_recinto.',C,'.date('ymdHi',strtotime($invitation['Event']['from'])).','.date('ymdHi',strtotime($invitation['Event']['to'])).','.$house_name.','.$num_acceso.','.$invitation['Organizer']['mobile'].','.$invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname'].','.$mobile_guest.','.$name_guest.','.$invitation['Relation']['name'].',0,VIP,0');
		
		$aes = new AES($code, Configure::read('Security.passQR'), 128);
		$enc = $aes->encrypt();
		if($this->saveField('aes',"QrX1,".$enc)){
			$this->saveField('code',$code);
			return true;
		}
		return false;
	}
}
