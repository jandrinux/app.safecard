<?php
App::uses('AppModel', 'Model');
/**
 * Role Model
 *
 * @property User $User
 */
class Role extends AppModel {
	
    public $actsAs = array('Acl' => array('type' => 'requester'));
/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'code' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Debe ingresar codigo de rol'
            ),
            'isUnique'=> array(
                'rule' => array('isUnique'),
                'message'=>'Codigo existente'
            )
        )
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
//    public $hasAndBelongsToMany = array(
//        'User' => array(
//            'className' => 'User',
//            'joinTable' => 'users_roles',
//            'foreignKey' => 'role_id',
//            'associationForeignKey' => 'user_id',
//            'unique' => 'keepExisting',
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'finderQuery' => '',
//            'deleteQuery' => '',
//            'insertQuery' => ''
//        )
//    );
	
    public function parentNode() {
        return null;
    }
}
