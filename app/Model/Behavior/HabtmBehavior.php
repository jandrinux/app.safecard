<?php
class HabtmBehavior extends ModelBehavior {

    public function beforeFind(Model $model, $options) {
    if (!isset($options['joins'])) {
      $options['joins'] = array();
    }
    if (!isset($options['habtm'])) {
      return $options;
    }
    $habtm = $options['habtm'];
    unset($options['habtm']);

    foreach($habtm as $m => $scope){
      $assoc = $model->hasAndBelongsToMany[$m];
      $bind = "{$assoc['with']}.{$assoc['foreignKey']} = {$model->alias}.{$model->primaryKey}";

      $options['joins'][] = array(
          'table' => $assoc['joinTable'],
          'alias' => $assoc['with'],
          'type' => 'inner',
          'foreignKey' => false,
          'conditions'=> array($bind)
      );

      $bind = $m.'.'.$model->{$m}->primaryKey.' = ';
      $bind .= "{$assoc['with']}.{$assoc['associationForeignKey']}";

      $options['joins'][] = array(
          'table' => $model->{$m}->table,
          'alias' => $m,
          'type' => 'inner',
          'foreignKey' => false,
          'conditions'=> array($bind) + (array)$scope,
      );
    }
    return $options;
    }

}

