<?php
App::uses('AppModel', 'Model');
/**
 * CondosRelation Model
 *
 * @property Condo $Condo
 * @property Relation $Relation
 */
class CondosRelation extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'alert_time';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Condo' => array(
			'className' => 'Condo',
			'foreignKey' => 'condo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Relation' => array(
			'className' => 'Relation',
			'foreignKey' => 'relation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
