<?php
App::uses('AppModel', 'Model');
/**
 * Gate Model
 *
 * @property Condo $Condo
 */
class Log extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'invitation_id';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Condo' => array(
			'className' => 'Condo',
			'foreignKey' => 'condo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Invitation' => array(
			'className' => 'Invitation',
			'foreignKey' => 'invitation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Service' => array(
			'className' => 'Service',
			'foreignKey' => 'service_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'House' => array(
			'className' => 'House',
			'foreignKey' => 'house_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Student' => array(
			'className' => 'Student',
			'foreignKey' => 'student_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Guard' => array(
			'className' => 'User',
			'foreignKey' => 'guard_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
