<?php
App::uses('AppModel', 'Model');
App::import('Component', 'SessionComponent');
/**
 * UsersRole Model
 *
 * @property User $User
 * @property Role $Role
 */
class CondosUser extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Condo' => array(
			'className' => 'Condo',
			'foreignKey' => 'condo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public function getCondominios(){
		//pr(SessionComponent::read("Auth.User"));
		$condos_id = array();
		
		if(SessionComponent::read("Auth.User.Role.code")!='SADM'){
			$condos = $this->find('all',array('conditions'=>array('user_id'=>SessionComponent::read("Auth.User.id"))));
			
			foreach($condos as $key=>$condo){
				$condos_id[] = $condo['CondosUser']['condo_id'];
			}
		} else {
			$condos = $this->Condo->find('all');
			foreach($condos as $key=>$condo){
				$condos_id[] = $condo['Condo']['id'];
			}
		}
		
		
		return $condos_id;
	}
}
