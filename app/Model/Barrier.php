<?php
App::uses('AppModel', 'Model');
/**
 * Barrier Model
 *
 * @property Condo $Condo
 */
class Barrier extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Condo' => array(
			'className' => 'Condo',
			'foreignKey' => 'condo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

