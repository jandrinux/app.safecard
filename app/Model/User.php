<?php
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
App::uses('AclComponent', 'Controller/Component');

define( 'API_ACCESS_KEY', 'AIzaSyCuTFWabl9ExkEWKrd4oYDB6j3s_8JUVo4' );
/**
 * User Model
 *
 * @property Role $Role
 */
class User extends AppModel {
	public $name = 'User';
	public $tienePropiedad = false;
	public $actsAs = array('Acl' => array('type' => 'requester'));	
	
	public function beforeSave($options = array()){
		if(isset($this->data['User']['password'])){
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']); 
		}
		
		/* if(isset($this->data['rut'])){
			$this->data['rut'] = formato_rut($this->data['rut']);
		} */
		return true; 
	}

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			/*'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar su nombre',
				//'required' => true,
				//'allowEmpty' => false,
				//'last'=>true
			),*/
			/*'onlyLetters' => array(
				'rule' => array('onlyLettersName'), 
				'message' => 'Ingrese solo letras'
			)*/
		),
		'father_surname' => array(
			/*'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar su apellido',
				'required' => true,
				'allowEmpty' => false,
				'last'=>true
			),*/
			/*'onlyLetters' => array(
				'rule' => array('onlyLettersFatherSurname'),
				'message' => 'Ingrese solo letras'
			)*/
		),
		'password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe ingresar un password',
				'allowEmpty' => false,
				'required' => false,
				'last'=>true
			),
			'maxlenght'=>array(
				'rule'	=> array('maxLength', 4),
				'message' => 'PIN debe contener 4 dígitos'
			),
			'minlenght'=>array(
				'rule'    => array('minLength', 4),
				'message' => 'PIN debe contener 4 dígitos'
			),
			'numeric'=>array(
				'rule' => array('numeric'),
				'message' => 'PIN debe contener sólo numéros'
			)
		),
		'cpassword' => array(
			'match' => array(
				'rule' => array('match'),
				'message' => 'Debes ingresar passwords iguales',
				'last'=>true
			)
		),
		'active' => array(
			'boolean' => array(
				'rule' => array('boolean'),
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Email incorrecto',
				'allowEmpty' => true,
				'required' => false,
				'last'=>true
			)
		),
		'mobile' => array(
			//'numeric' => 'numeric',
			'unique'=> array(
				'rule' => array('isUnique'),
				'message' => 'Celular ya registrado',
				'allowEmpty' => false,
				'required'=>true,
				'on' => 'create'
			),
			'propiedadRepetida' => array(
				'rule' => array('propiedadRepetida'),
				'message' => 'Número de celular ya está vinculado con la propiedad',
				'on' => 'update'
			)
		)
	);

	function propiedadRepetida(){
		//echo $this->tienePropiedad? "tiene":"no tiene"; exit;
		if($this->tienePropiedad){
			return false;
		}
		//return true;
		//return !$this->tienePropiedad;
	}

	function onlyLettersName($campo){
		if(ctype_alpha($this->data['User']['name'])){
			return true;
		} else {
			return false;
		}
	}

	function onlyLettersFatherSurname($campo){
		if(ctype_alpha($this->data['User']['father_surname'])){
			return true;
		} else {
			return false;
		}
	}


	function match(){
		if(strcmp($this->data['User']['password'],$this->data['User']['cpassword']) == 0){
			return true;
		}
	}
        
	function length(){
		if(strlen($this->data['User']['mobile']) == 8){
			return true;
		}
		return false;
	}
	
	function checkUnique($data, $fields){
		// check if the param contains multiple columns or a single one
		if (!is_array($fields)){
			$fields = array($fields);
		}
		 
		// go trough all columns and get their values from the parameters
		foreach($fields as $key){
			$unique[$key] = $this->data[$this->name][$key];
		}
		 
		// primary key value must be different from the posted value
		if (isset($this->data[$this->name][$this->primaryKey])){
			$unique[$this->primaryKey] = "<>" . $this->data[$this->name][$this->primaryKey];
		}
		 
		// use the model's isUnique function to check the unique rule
		return $this->isUnique($unique, false);
	}
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed        
        
	public $hasMany = array(
		'Contact','Event','HousesUser','UsersRole',
		'Invitation'=>array(
			'className'=>'Invitation',
			'foreignKey' => 'organizer_id'
		)
	);
	public $belongsTo = array('Role','Country');
/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Role' => array(
			'className' => 'Role',
			'joinTable' => 'users_roles',
			'foreignKey' => 'user_id',
			'associationForeignKey' => 'role_id',
			'unique' => 'keepExisting',
			'with' => 'UsersRole',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Condo' => array(
			'className' => 'Condo',
			'joinTable' => 'condos_users',
			'foreignKey' => 'user_id',
			'associationForeignKey' => 'condo_id',
			'unique' => 'keepExisting',
			'with' => 'CondosUser',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),  
		'House' => array(
			'className' => 'House',
			'joinTable' => 'houses_users',
			'foreignKey' => 'user_id',
			'associationForeignKey' => 'house_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Student' => array(
			'className' => 'Student',
			'joinTable' => 'houses_users',
			'foreignKey' => 'user_id',
			'associationForeignKey' => 'student_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'School' => array(
			'className' => 'Condo',
			'joinTable' => 'schools_users',
			'foreignKey' => 'school_id',
			'associationForeignKey' => 'school_id',
			'unique' => 'keepExisting',
			'with' => 'SchoolsUser',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		), 
	);
	
	function parentNode() {    
		if (!$this->id && empty($this->data)) {
			return null;
		}
		if (isset($this->data['User']['role_id'])) {
			$roleId = $this->data['User']['role_id'];
		} else {
			$roleId = $this->field('role_id');
		}
		if (!$roleId) {
			return null;
		} else {
			return array('Role' => array('id' => $roleId));
		}		
	}
	
	function bindNode($user) {    
		return array('model' => 'Role', 'foreign_key' => $user['User']['role_id']);
	}
	
	function sendCustomNotification($user_id = null, $message = "", $subtitle = ""){
		
		$this->id = $user_id;
		$flag = false;
		switch($this->field('OS')){
			case 'AND':
				
				$registrationIds = array($this->field('full_device'));
				 
				// prep the bundle
				$msg = array(
					'title'			=> 'Safecard',
					'message' 		=> $message,
					'subtitle'		=> $subtitle,
					'tickerText'	=> $message,
					'type' 			=> ""
				);
				 
				$fields = array(
					'registration_ids' 	=> $registrationIds,
					'data'				=> $msg
				);
				 
				$headers = array(
					'Authorization: key=' . API_ACCESS_KEY,
					'Content-Type: application/json'
				);
				
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$result = curl_exec($ch );
				
				curl_close( $ch );
				$flag = true;
				break;
				
			case 'IOS':
				$deviceToken = $this->field('full_device');
				// Put your private key's passphrase here:
				$passphrase = Configure::read('APNS.pass');//'beth.,2409';
				////////////////////////////////////////////////////////////////////////////////

				$ctx = stream_context_create();
				stream_context_set_option($ctx, 'ssl', 'local_cert', Configure::read('PathAppController').'ck.pem');
				stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
				
				// Open a connection to the APNS server
				$fp = stream_socket_client(
					Configure::read('APNS.url'), $err,
					$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

				// Create the payload body
				$body['aps'] = array(
					'alert' => array(
								"action-loc-key" => "para abrir Safecard",
								"body" => $message
							),
					'sound' => 'default',
					'badge' => 1
				);

				// Encode the payload as JSON
				$payload = json_encode($body);

				// Build the binary notification
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
				
				// Send it to the server
				$result = fwrite($fp, $msg, strlen($msg));
				
				// Close the connection to the server
				fclose($fp);
				
				$flag = true;
				break;
			case 'WPH':
			
				break;
		}
		return $flag;
		
	}
}
