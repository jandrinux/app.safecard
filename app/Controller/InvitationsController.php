<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'AES');
/**
 * Invitations Controller
 *
 * @property Invitation $Invitation
 */
class InvitationsController extends AppController {
	
	public $helpers = array("QrCode");
	public $components = array('Soap'); 
	
	public $paginate = array(
		'Invitation' => array(
			'limit'=>20,
		),
		'Log' => array(
			'limit' => 15,
			'order' => array(
				'Log.created' => 'DESC'
			)
		)
	);

	function beforeFilter() {
		$this->Auth->allow(array('update_repeat', 'QRGenerator'));
		parent::beforeFilter();
	}
	
	public function admin_dashboard(){
		$this->Invitation->Log->recursive = 1;
		
		$this->loadModel('User');
		$condos_user = ClassRegistry::init('CondosUser')->getCondominios(); 
		
		$filtro = array();
		$houses = array();
		$condo = array();
		if($this->request->is('post')){

			//Reinicio paginador en caso que uno de los filtros haya cambiado
			if(
				($this->request->data['Log']['condo_id']!=$this->Session->read('filter_condo_id')) OR
				(isset($this->request->data['Log']['house_id']) and $this->request->data['Log']['house_id']!=$this->Session->read('filter_house_id')) OR
				($this->request->data['Log']['start_date']!=$this->Session->read('filter_start_date')) OR
				($this->request->data['Log']['end_date']!=$this->Session->read('filter_end_date'))
			){
				$this->request->params['named']['page'] = 1;
			}

			$this->Session->write('filter_house_id','');
			$this->Session->write('filter_condo_id','');
			$this->Session->write('filter_search_name','');
			$this->Session->write('filter_start_date','');
			$this->Session->write('filter_end_date','');
			if(strtotime($this->request->data['Log']['start_date']) <= strtotime($this->request->data['Log']['end_date'])){

				if(isset($this->request->data['Log']['condo_id']) && $this->request->data['Log']['condo_id']>0){
					$filtro[] = array('Log.condo_id'=>$this->request->data['Log']['condo_id']);
					$this->Session->write('filter_condo_id',$this->request->data['Log']['condo_id']);
					$condo = $this->Invitation->Condo->read(null, $this->request->data['Log']['condo_id']);
				}

				if(isset($this->request->data['Log']['house_id']) && $this->request->data['Log']['house_id']>0){
					$filtro[] = array(
						'OR' => array(
							'Log.house_id' => $this->request->data['Log']['house_id'],
							'Invitation.house_id' => $this->request->data['Log']['house_id'],
							'Log.student_id' => $this->request->data['Log']['house_id'],
							'Invitation.student_id' => $this->request->data['Log']['house_id']
						)
					);
					$this->Session->write('filter_house_id', $this->request->data['Log']['house_id']);
				}
				
				if((isset($this->request->data['Log']['start_date']) && $this->request->data['Log']['start_date']!='') && (isset($this->request->data['Log']['end_date']) && $this->request->data['Log']['end_date']!='')){
					$filtro[] = array(
						'Log.created >= '=> date('Y-m-d 00:00:00', strtotime($this->request->data['Log']['start_date'])),
						'Log.created <= '=> date('Y-m-d 23:59:59', strtotime($this->request->data['Log']['end_date'])),
					);
					$this->Session->write('filter_start_date',$this->request->data['Log']['start_date']);
					$this->Session->write('filter_end_date',$this->request->data['Log']['end_date']);
				} else {
					$this->Session->setFlash(__('Debe seleccionar ambas fechas'), 'flash_warning');
				}
			} else {
				$this->Session->setFlash(__('Rango de fechas inválido'),'flash_warning');
			}
			
			if(empty($filtro)){
				$filtro[] = array(
					'Log.condo_id' => $this->Session->read('filter_condo_id')!=''? $this->Session->read('filter_condo_id'):$condos_user,
					'Log.created >= ' => $this->Session->read('filter_start_date')!=''? date('Y-m-d 00:00:00', strtotime($this->Session->read('filter_start_date'))):date('Y-m-d 00:00:00', strtotime('-7 days')),
					'Log.created <= ' => $this->Session->read('filter_end_date')!=''? date('Y-m-d 23:59:59', strtotime($this->Session->read('filter_end_date'))):date('Y-m-d 23:59:59')
				);
			}

		} else {
			//pr($this->Session->read('filter_house_id'));
			$filtro[] = array(
				'Log.condo_id' => $this->Session->read('filter_condo_id')!=''? $this->Session->read('filter_condo_id'):$condos_user,
				'OR' => array(
					'Log.house_id' => @$this->Session->read('filter_house_id'),
					'Invitation.house_id' => @$this->Session->read('filter_house_id'),
					'Log.student_id' => @$this->Session->read('filter_house_id'),
					'Invitation.student_id' => @$this->Session->read('filter_house_id')
				),
				'Log.created >= ' => $this->Session->read('filter_start_date')!=''? date('Y-m-d 00:00:00', strtotime($this->Session->read('filter_start_date'))):date('Y-m-d 00:00:00', strtotime('-7 days')),
				'Log.created <= ' => $this->Session->read('filter_end_date')!=''? date('Y-m-d 23:59:59', strtotime($this->Session->read('filter_end_date'))):date('Y-m-d 23:59:59')
			);
			$condo = $this->Invitation->Condo->read(null, @$this->Session->read('filter_condo_id'));
		}



		if(!empty($condo)){
			if(!$condo['Condo']['type']){
				$houses = $this->Invitation->Log->House->find('list', array('conditions'=>array('condo_id'=>$condo['Condo']['id'])));
				$houses = array(''=>'Todos') + $houses;
			} else {
				$houses = $this->Invitation->Log->Student->find('list', array('conditions'=>array('Student.school_id'=>$condo['Condo']['id'])));
				$houses = array(''=>'Todos') + $houses;
			}
		}
		
		if($this->Session->read('Auth.User.Role.code')=='CSJ'){
			$filtro[] = array(
				'OR'=>array(
					array('NOT'=>array('Log.invitation_id' => null),'Invitation.relation_id >'=> '2'),
					array('NOT'=>array('Log.service_id' => null))
					)
			);
			
		}

		$logs = $this->paginate($this->Invitation->Log,$filtro);
		
		$last_id = 0;
		foreach($logs as $key=>$log){
			

			if(is_null($log['Log']['invitation_id'])){

				$logs[$key]['Log']['house_name'] = !is_null($log['Log']['house_id'])? $log['House']['name']:$log['Student']['full_name'];//$this->Invitation->House->field("name");
				$logs[$key]['Log']['relation'] = !is_null($log['Log']['service_id'])? $log['Service']['glosa']:'--';
				$logs[$key]['Log']['guest_name'] = '--';
				$logs[$key]['Log']['guest_mobile'] = '';
				$logs[$key]['Log']['organizer_mobile'] = !is_null($log['Log']['guard_id'])? $log['Guard']['mobile']:'';
				$logs[$key]['Log']['organizer_name'] = !is_null($log['Log']['guard_id'])? $log['Guard']['name'].' '.$log['Guard']['father_surname']:'--';

			} else {
				$inv = $this->Invitation->read(null, $log['Log']['invitation_id']);

				$logs[$key]['Log']['house_name'] = $inv['House']['name'];
				$logs[$key]['Log']['organizer_name'] = $inv['Organizer']['name'].' '.$inv['Organizer']['father_surname'];
				$logs[$key]['Log']['organizer_mobile'] = $inv['Organizer']['mobile'];

				if(is_null($inv['Invitation']['guest_id'])){
					$logs[$key]['Log']['guest_name'] = $inv['Contact']['name'];
					$logs[$key]['Log']['guest_mobile'] = $inv['Contact']['mobile'];	
				} else {
					$logs[$key]['Log']['guest_name'] = $inv['Guest']['name'].' '.$inv['Guest']['father_surname'];
					$logs[$key]['Log']['guest_mobile'] = $inv['Guest']['mobile'];	
				}
				
				$logs[$key]['Log']['relation'] = $inv['Relation']['name'];
			}
			$last_id = $log['Log']['id']>$last_id ? $log['Log']['id'] : $last_id;
		}

		$this->set('logs',$logs); 
		$this->set(compact('houses'));
		




		$last_log = $this->Invitation->Log->find('first', array('conditions'=>$filtro, 'order'=>'Log.id DESC'));
		$this->set('last_id',empty($last_log['Log']['id'])? '' : $last_log['Log']['id']);
		
		//Estadisticas: Cargar en otra función via ajax para actualización constante
		$fecha = date('Y-m-d', strtotime(date('Y-m-d').' - 7 days'));
		$dias = array();
		for($i = 0; $fecha <= date('Y-m-d'); $i++){
			$dias[$i]['Day']['glosa'] = date('d F', strtotime($fecha));

		$condf = array(
					'Log.created <='=>$fecha.' 23:59:59', 
					'Log.created >='=>$fecha.' 00:00:00',
					'Log.condo_id' => isset($_SESSION['filter_condo_id'])?$_SESSION['filter_condo_id']:$condos_user
				);
//$db =& ConnectionManager::getDataSource('default');
//$db->showLog(); 

		if($this->Session->read('Auth.User.Role.code')=='CSJ'){
			$condf[] = array(
				'OR'=>array(
					array('NOT'=>array('Log.invitation_id' => null),'Invitation.relation_id >'=> '2'),
					array('NOT'=>array('Log.service_id' => null))
					)
			);
		}



			$dias[$i]['Day']['total_visitas'] = $this->Invitation->Log->find('count', array('conditions'=>$condf));
			
			$fecha = date('Y-m-d', strtotime($fecha.' +1 day'));
		}
		$this->set('dias',$dias);
		
		$relations = $this->Invitation->Relation->find('list');
		$relations_details = array();
		foreach($relations as $key=>$relation){
			$relations_details[$key]['Relation']['glosa'] = $relation;
			$relations_details[$key]['Relation']['total_visitas'] = $this->Invitation->Log->find('count', 
																	array('conditions'=> array(
																		'Invitation.relation_id'=>$key,
																		'Log.created >= ' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' -24 hours')),
																		'Log.created <= ' => date('Y-m-d H:i:s'),
																		'Log.condo_id' => isset($_SESSION['filter_condo_id'])?$_SESSION['filter_condo_id']:$condos_user
																	)));
		}

		$services = $this->Invitation->Log->Service->find('list');
		$services_details = array();
		foreach($services as $key=>$service){
			$services_details[$key]['Service']['glosa'] = $service;
			$services_details[$key]['Service']['total_visitas'] = $this->Invitation->Log->find('count', 
																	array('conditions'=> array(
																		'Log.service_id'=>$key,
																		'Log.created >= ' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' -24 hours')),
																		'Log.created <= ' => date('Y-m-d H:i:s'),
																		'Log.condo_id' => isset($_SESSION['filter_condo_id'])?$_SESSION['filter_condo_id']:$condos_user
																	)));
		}

		$this->set('relations_details',$relations_details);
		$this->set('services_details',$services_details);
		
		//$this->set('condos', $this->Invitation->Condo->find('list', array('order'=>'Condo.name ASC')));
		
	}
	
	public function get_last_logs($last_id = null){
		$this->layout = false;
		$this->autoRender = false;

		if($this->request->is('ajax')){
			$this->Invitation->Log->recursive = 1;
			
			if(!is_numeric($last_id) || $last_id < 1){
				$last_id = 0;
			}
			
			//Propiedades del usuario
			if(!in_array($this->Session->read('Auth.User.Role.code'), Configure::read('AppAdmin'))){
				$houses = $this->Invitation->Organizer->HousesUser->find('all',array(
					'conditions'=>array(
						'HousesUser.active'=>1, 
						'HousesUser.user_id' => $this->Session->read('Auth.User.id')
				)));
			} else {
				
				$houses = $this->Invitation->Organizer->HousesUser->find('all',array(
					'conditions'=>array(
						'HousesUser.active'=>1
					)
				));
			}

			$house_ids = $student_ids = array();
			foreach($houses as $house){
				if(!is_null($house['HousesUser']['house_id'])){
					$house_ids[] = $house['HousesUser']['house_id'];
				} else if(!is_null($house['HousesUser']['student_id'])){
					$student_ids[] = $house['HousesUser']['student_id'];
				}
				//$house_ids[] = !is_null($house['HousesUser']['house_id'])? $house['HousesUser']['house_id']:$house['HousesUser']['student_id'];
			}

			$filtro = array();
			if(!in_array($this->Session->read('Auth.User.Role.code'), Configure::read('AppAdmin'))){
				if($this->Session->read('filter_house_id') != '' && $this->Session->read('filter_house_id') > 0){
					$house = $this->Invitation->House->read(null, $this->Session->read('filter_house_id'));
					if($house['Condo']['type'] == 1){
						$filtro[] = array(
							'OR'=>array(
								'Invitation.student_id'=>$this->Session->read('filter_house_id'),
								'Log.student_id'=>$this->Session->read('filter_house_id')
							)
						);
					} else {
						$filtro[] = array(
							'OR'=>array(
								'Invitation.house_id'=>$this->Session->read('filter_house_id'),
								'Log.house_id'=>$this->Session->read('filter_house_id')
							)
						);
					}
				} else {
					$filtro[] = array(
						'OR' => array(
							'Log.house_id' => $house_ids,
							'Log.student_id' => $student_ids,
							'Invitation.house_id' => $house_ids,
							'Invitation.student_id' => $student_ids
						)
					);
				}

				if(empty($student_ids)){
					unset($filtro[0]['OR']['Invitation.student_id']);
					unset($filtro[0]['OR']['Log.student_id']);
				} 

				if(empty($house_ids)){
					unset($filtro[0]['OR']['Invitation.house_id']);
					unset($filtro[0]['OR']['Log.house_id']);
				}

			} else {
				if($this->Session->read('filter_house_id')!='' && $this->Session->read('filter_house_id') > 0){
					$filtro[] = array(
						'OR' => array(
							'Log.house_id' => $this->Session->read('filter_house_id'),
							'Log.student_id' => $this->Session->read('filter_house_id'),
							'Invitation.house_id' => $this->Session->read('filter_house_id'),
							'Invitation.student_id' => $this->Session->read('filter_house_id')
						)
					);
				}
			}
			
			if($this->Session->read('filter_condo_id')!='' && $this->Session->read('filter_condo_id')>0){
				$filtro[] = array('Log.condo_id' => $this->Session->read('filter_condo_id'));
			} else {
				$condos_ids = ClassRegistry::init('CondosUser')->getCondominios();
				$filtro[] = array('Log.condo_id' => $condos_ids);
			}

			if($this->Session->read('filter_start_date')!='' && $this->Session->read('filter_end_date')!=''){
				$filtro[] = array(
					'Log.created >= '=> date('Y-m-d 00:00:00', strtotime($this->Session->read('filter_start_date'))),
					'Log.created <= '=> date('Y-m-d 23:59:59', strtotime($this->Session->read('filter_end_date'))),
				);
			}
			
			/*if(in_array($this->Session->read('Auth.User.Role.code'), array('PROP'))){
				/*if($this->Session->read('filter_house_id')!='' && $this->Session->read('filter_house_id')>0){
					$filtro[] = array(
						'OR'=> array( 
							'Invitation.house_id' => $this->Session->read('filter_house_id'),
							'Invitation.student_id' => $this->Session->read('filter_house_id'),
							'Log.house_id' => $this->Session->read('filter_house_id'),
							'Log.student_id' => $this->Session->read('filter_house_id')
						)
					);
				}---/

				$house = $this->Invitation->House->read(null, $this->Session->read('filter_house_id'));
				if($house['Condo']['type'] == 1){
					$filtro[] = array(
						'OR'=>array(
							'Invitation.student_id'=>$this->Session->read('filter_house_id'),
							'Log.student_id'=>$this->Session->read('filter_house_id')
						)
					);
				} else {
					$filtro[] = array(
						'OR'=>array(
							'Invitation.house_id'=>$this->Session->read('filter_house_id'),
							'Log.house_id'=>$this->Session->read('filter_house_id')
						)
					);
				}
			}	*/
		if($this->Session->read('Auth.User.Role.code')=='CSJ'){
			$filtro[] = array(
								'OR'=>array(
									array('NOT'=>array('Log.invitation_id' => null),'Invitation.relation_id >'=> '2'),
									array('NOT'=>array('Log.service_id' => null))
									)
							);
		}
			if($this->Session->read('Auth.User.Role.code') == 'PROP'){
				$logs = $this->Invitation->Log->find('all', array(
					'conditions'=>array(
						'Log.id > '=>$last_id,
						$filtro
					),
					'order' => array('Log.id'=>'ASC')
				));
			} else {
				$logs = $this->Invitation->Log->find('all', array(
					'conditions'=>array(
						'Log.id > '=>$last_id,
						$filtro
					),
					'order' => array('Log.id'=>'ASC')
				));
			}



			//pr($filtro);
			$logstmp = array();
			foreach($logs as $key=>$log){

				switch($log['Log']['type']){
					case 0: 
						$logstmp[$key]['type'] = 'ic_arrow_out.png';
						$logstmp[$key]['type_id'] = 0;
						break;
					case 1:
						$logstmp[$key]['type'] = 'ic_arrow_in.png';
						$logstmp[$key]['type_id'] = 1;
						break;
					case 2:
						$logstmp[$key]['type'] = 'ic_warning.png';
						$logstmp[$key]['type_id'] = 2;
						break;
				}
				$logstmp[$key]['id'] = $log['Log']['id'];

				if(!is_null($log['Log']['invitation_id'])){
					//Invitaciones propietario
					$inv = $this->Invitation->read(null, $log['Log']['invitation_id']);
					
					$logstmp[$key]['condo_name'] = $inv['Condo']['name'];
					$logstmp[$key]['created'] = date('d M, H:i',strtotime($log['Log']['created']));
					
					
					$logstmp[$key]['house_name'] = $inv['House']['name'];
					$logstmp[$key]['organizer_name'] = $inv['Organizer']['name'].' '.$inv['Organizer']['father_surname'];
					$logstmp[$key]['organizer_mobile'] = $inv['Organizer']['mobile'];
					
					if(is_null($inv['Invitation']['guest_id'])){
						$logstmp[$key]['guest_name'] = $inv['Contact']['name'];
						$logstmp[$key]['guest_mobile'] = $inv['Contact']['mobile'];	
					} else {
						$logstmp[$key]['guest_name'] = $inv['Guest']['name'].' '.$inv['Guest']['father_surname'];
						$logstmp[$key]['guest_mobile'] = $inv['Guest']['mobile'];	
					}

					$logstmp[$key]['relation'] = $inv['Relation']['name'];
					
				} else {
					//Servicios condominio-propiedad
					
					//if(!is_null($log['Log']['service_id']) && in_array($log['Log']['house_id'],$house_ids)){

						$logstmp[$key]['condo_name'] = $log['Condo']['name'];
						$logstmp[$key]['created'] = date('d M, H:i',strtotime($log['Log']['created']));
						
						
						$logstmp[$key]['house_name'] = !is_null($log['Log']['house_id'])? $log['House']['name']:$log['Student']['full_name'];//$this->Invitation->House->field("name");
						
						if(!is_null($log['Log']['guard_id'])){
							$logstmp[$key]['organizer_name'] = $log['Guard']['name'].' '.$log['Guard']['father_surname'];
							$logstmp[$key]['organizer_mobile'] = $log['Guard']['mobile'];
						} else {
							$logstmp[$key]['organizer_name'] =  '--';
							$logstmp[$key]['organizer_mobile'] = '';
						}
						
						$logstmp[$key]['guest_name'] = '--';//$inv['Guest']['name'].' '.$inv['Guest']['father_surname'];
						$logstmp[$key]['guest_mobile'] = '';
						$logstmp[$key]['relation'] = !is_null($log['Log']['service_id'])? $log['Service']['glosa']:'--';
					//}
				}
			}
			echo json_encode($logstmp);
		}
	}
	
	public function dashboard(){
		$this->Invitation->Log->recursive = 1;
		
		//Propiedades del usuario
		$houses = $this->Invitation->Organizer->HousesUser->find('all',array('conditions'=>array('HousesUser.house_id NOT'=>NULL,'HousesUser.active'=>1, 'HousesUser.user_id' => $this->Session->read('Auth.User.id'))));
		$house_ids = array();
		foreach($houses as $house){
			$house_ids[] = $house['HousesUser']['house_id'];
		}

		//Alumnos del usuario
		$students = $this->Invitation->Organizer->HousesUser->find('all',array('conditions'=>array('HousesUser.student_id NOT'=>NULL,'HousesUser.active'=>1, 'HousesUser.user_id' => $this->Session->read('Auth.User.id'))));
		$student_ids = array();
		foreach($students as $student){
			$student_ids[] = $student['HousesUser']['student_id'];
		}

		if(empty($house_ids) && empty($student_ids)){
			$this->Session->setFlash(__('No es posible ingresar al dashboard ya que no posee propiedad ni alumno asociado'),'flash_warning');
			$this->redirect(array('controller'=>'contacts','action'=>'index'));
		}


		$condos_ids = ClassRegistry::init('CondosUser')->getCondominios();
        $this->set("condos_ids",$condos_ids);
        $condos_user = ClassRegistry::init('Condo')->find('list', array('conditions'=>array('Condo.id'=>$condos_ids)));

		$filtro = array(); 
		
		if($this->request->is('post')){
			
			//Reinicio paginador en caso que uno de los filtros haya cambiado
			if(
				($this->request->data['Log']['house_id']!=$this->Session->read('filter_house_id')) OR
				($this->request->data['Log']['start_date']!=$this->Session->read('filter_start_date')) OR
				($this->request->data['Log']['end_date']!=$this->Session->read('filter_end_date'))
			){
				$this->request->params['named']['page'] = 1;
			}

			$this->Session->write('filter_house_id','');		
			$this->Session->write('filter_start_date','');
			$this->Session->write('filter_end_date','');

			if(isset($this->request->data['Log']['house_id']) && $this->request->data['Log']['house_id']!=''){
				$house = $this->Invitation->House->read(null, $this->request->data['Log']['house_id']);
				if($house['Condo']['type'] == 1){
					$filtro[] = array(
						'OR'=>array(
							'Invitation.student_id'=>$this->request->data['Log']['house_id'],
							'Log.student_id'=>$this->request->data['Log']['house_id']
						)
					);
				} else {
					$filtro[] = array(
						'OR'=>array(
							'Invitation.house_id'=>$this->request->data['Log']['house_id'],
							'Log.house_id'=>$this->request->data['Log']['house_id']
						)
					);
				}
				
				$this->Session->write('filter_house_id',$this->request->data['Log']['house_id']);
			} else {
				$filtro[] = array(
					'OR' => array(
						'Invitation.house_id' => $house_ids,
						'Log.house_id' => $house_ids,
						'Invitation.student_id' => $student_ids,
						'Log.student_id' => $student_ids
					)
				);

				if(empty($student_ids)){
					unset($filtro[0]['OR']['Invitation.student_id']);
					unset($filtro[0]['OR']['Log.student_id']);
				} 

				if(empty($house_ids)){
					unset($filtro[0]['OR']['Invitation.house_id']);
					unset($filtro[0]['OR']['Log.house_id']);
				}

				$this->Session->write('filter_house_id','');
			}
			
			if((isset($this->request->data['Log']['start_date']) && $this->request->data['Log']['start_date']!='') && (isset($this->request->data['Log']['end_date']) && $this->request->data['Log']['end_date']!='')){
				$filtro[] = array(
					'Log.created >= '=> date('Y-m-d 00:00:00', strtotime($this->request->data['Log']['start_date'])),
					'Log.created <= '=> date('Y-m-d 23:59:59', strtotime($this->request->data['Log']['end_date'])),
				);
				$this->Session->write('filter_start_date',$this->request->data['Log']['start_date']);
				$this->Session->write('filter_end_date',$this->request->data['Log']['end_date']);
			} else {
				$this->Session->setFlash(__('Debe seleccionar ambas fechas'), 'flash_warning');
			}
		} else {
			$or = array();
			if($this->Session->read('filter_house_id')!=''){
				$house = $this->Invitation->House->read(null, $this->Session->read('filter_house_id'));
				if($house['Condo']['type'] == 1){
					$or = array(
						'Invitation.student_id'=>$this->Session->read('filter_house_id'),
						'Log.student_id'=>$this->Session->read('filter_house_id')
					);
				} else {
					$or = array(
						'Invitation.house_id'=>$this->Session->read('filter_house_id'),
						'Log.house_id'=>$this->Session->read('filter_house_id')
					);
				}
			} else {
				$or = array(
					'Invitation.house_id' => $house_ids,
					'Log.house_id' => $house_ids,
					'Invitation.student_id' => $student_ids,
					'Log.student_id' => $student_ids
				);
				
				if(empty($student_ids)){
					unset($or['Invitation.student_id']);
					unset($or['Log.student_id']);
				} 

				if(empty($house_ids)){
					unset($or['Invitation.house_id']);
					unset($or['Log.house_id']);
				}

			}

			$filtro[] = array(
				'OR'=> $or,
				'Log.created >= ' => $this->Session->read('filter_start_date')? date('Y-m-d 00:00:00', strtotime($this->Session->read('filter_start_date'))):date('Y-m-d 00:00:00', strtotime('-7 days')),
				'Log.created <= ' => $this->Session->read('filter_end_date')? date('Y-m-d 23:59:59', strtotime($this->Session->read('filter_end_date'))):date('Y-m-d 23:59:59')

			);
		}

		//Filtro por propiedades
		$logs = $this->paginate($this->Invitation->Log, $filtro);
		$last_id = 0;
		foreach($logs as $key=>$log){

			if(is_null($log['Log']['invitation_id'])){

				$logs[$key]['Log']['house_name'] = !is_null($log['Log']['house_id'])? $log['House']['name']:$log['Student']['full_name'];//$this->Invitation->House->field("name");
				$logs[$key]['Log']['relation'] = !is_null($log['Log']['service_id'])? $log['Service']['glosa']:'--';
				$logs[$key]['Log']['guest_name'] = '--';
				$logs[$key]['Log']['guest_mobile'] = '';
				$logs[$key]['Log']['organizer_mobile'] = !is_null($log['Log']['guard_id'])? $log['Guard']['mobile']:'';
				$logs[$key]['Log']['organizer_name'] = !is_null($log['Log']['guard_id'])? $log['Guard']['name'].' '.$log['Guard']['father_surname']:'--';

			} else {
				$inv = $this->Invitation->read(null, $log['Log']['invitation_id']);

				$logs[$key]['Log']['house_name'] = $inv['House']['name'];
				$logs[$key]['Log']['organizer_name'] = $inv['Organizer']['name'].' '.$inv['Organizer']['father_surname'];
				$logs[$key]['Log']['organizer_mobile'] = $inv['Organizer']['mobile'];

				if(is_null($inv['Invitation']['guest_id'])){
					$logs[$key]['Log']['guest_name'] = $inv['Contact']['name'];
					$logs[$key]['Log']['guest_mobile'] = $inv['Contact']['mobile'];	
				} else {
					$logs[$key]['Log']['guest_name'] = $inv['Guest']['name'].' '.$inv['Guest']['father_surname'];
					$logs[$key]['Log']['guest_mobile'] = $inv['Guest']['mobile'];	
				}
				
				$logs[$key]['Log']['relation'] = $inv['Relation']['name'];
			}
			
			$last_id = $log['Log']['id']>$last_id ? $log['Log']['id'] : $last_id;
		}

		$this->set('logs',$logs);
		$this->set('last_id',$last_id);
		
		$fecha = date('Y-m-d', strtotime(date('Y-m-d').' - 7 days'));
		$dias = array();
		for($i = 0; $fecha <= date('Y-m-d'); $i++){
			$dias[$i]['Day']['glosa'] = date('d F', strtotime($fecha));
			$dias[$i]['Day']['total_visitas'] = $this->Invitation->Log->find('count', 
													array('conditions'=>array(
														'Log.created <='=>$fecha.' 23:59:59', 'Log.created >='=>$fecha.' 00:00:00',
														'OR' => array(
															'Invitation.house_id' => isset($_SESSION['filter_house_id'])? $_SESSION['filter_house_id']:$house_ids,
															'Log.house_id'	=> isset($_SESSION['filter_house_id'])? $_SESSION['filter_house_id']:$house_ids
														)
													))
												);
			
			$fecha = date('Y-m-d', strtotime($fecha.' +1 day'));
		}
		$this->set('dias',$dias);
		
		$relations = $this->Invitation->Relation->find('list');
		$relations_details = array();
		foreach($relations as $key=>$relation){
			$relations_details[$key]['Relation']['glosa'] = $relation;
			$relations_details[$key]['Relation']['total_visitas'] = $this->Invitation->Log->find('count', 
																	array('conditions'=> array(
																		'Invitation.relation_id'=>$key,
																		'Log.created >= ' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' -24 hours')),
																		'Log.created <= ' => date('Y-m-d H:i:s'),
																		'Invitation.house_id' => $house_ids
																	)));
		}

		$services = $this->Invitation->Log->Service->find('list', array('conditions'=>array('code >= '=>20)));
		$services_details = array();
		foreach($services as $key=>$service){
			$services_details[$key]['Service']['glosa'] = $service;
			$services_details[$key]['Service']['total_visitas'] = $this->Invitation->Log->find('count', 
																	array('conditions'=> array(
																		'Log.service_id'=>$key,
																		'Log.created >= ' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' -24 hours')),
																		'Log.created <= ' => date('Y-m-d H:i:s'),
																		'Log.house_id' => $house_ids,
																		//'Log.condo_id' => isset($_SESSION['filter_condo_id'])?$_SESSION['filter_condo_id']:$condos_user
																	)));
		}

		// pr($relations_details);exit;
		$this->set('relations_details',$relations_details);
		$this->set('services_details',$services_details);

		$this->set('condos', $this->Invitation->Condo->find('list', array('order'=>'Condo.name ASC')));
		
	}






	public function admin_list(){
		$this->Invitation->recursive = 1;
		//$_SESSION['filter_admin_list'] = array();

 		$cond = array(
				'Invitation.sent_mobile' => 0,
				'Invitation.relation_id <> '=>1,
				'Invitation.status' => 1
				);
 
 		if($this->request->is('post')){
 			unset($_SESSION['filter_admin_list']);
 			$this->request->params['named']['page'] = 1;
 			$_SESSION['filter_admin_list'] = $this->request->data['Invitation'];

 		}
		//pr($_SESSION['filter_admin_list']);
 		

		if(isset($_SESSION['filter_admin_list']) and is_array($_SESSION['filter_admin_list'])){
			$this->request->data['Invitation'] = $_SESSION['filter_admin_list'];

			if(isset($this->request->data['Invitation']['tipo_inv']) and $this->request->data['Invitation']['tipo_inv'] == 'rec'){
				$filtro = array('Invitation.mobile' => $this->request->data['Invitation']['mobile']);
				$this->set('tipo_inv', $this->request->data['Invitation']['tipo_inv']);
				
			}elseif(isset($this->request->data['Invitation']['tipo_inv']) and $this->request->data['Invitation']['tipo_inv'] == 'env'){
				$filtro = array('Organizer.mobile' => $this->request->data['Invitation']['mobile']);
				$this->set('tipo_inv', $this->request->data['Invitation']['tipo_inv']);

			}elseif(isset($this->request->data['Invitation']['mobile'])){
				$filtro = array('OR'=>
								array('Invitation.mobile'=>$this->request->data['Invitation']['mobile'],
								'Organizer.mobile'=>$this->request->data['Invitation']['mobile'])
								);

			}
			if(isset($this->request->data['Invitation']['mobile']) and $this->request->data['Invitation']['mobile'])
				$this->set('mobile', $this->request->data['Invitation']['mobile']);



			if(isset($filtro) and count($filtro))
				$cond = array_merge($cond, $filtro);
			$this->set('post', 1);
		} 

		$cond = array_merge($cond,array('Invitation.guest_id <> Invitation.organizer_id'));
		
/*
		aplicar en paginador


		$invitations = $this->Invitation->find('all', array(
			'conditions'=> $cond
			,
			'fields'=>array("Organizer.mobile",
				"GROUP_CONCAT(day_id) as dias32",
				'mobile',
				'Guest.name','Guest.father_surname',
				'Organizer.name','Organizer.father_surname',
				'Guest.mobile','created','repeat_from',
				'repeat_to','Event.from',"repeat"),
			'recursive' => 0,
		   	'joins' => array(
		     	array(
		        	'table'=>'days_events',
		          	'type'=>'LEFT',
		          	'conditions' => array('Invitation.event_id'=>'days_events.event_id')
		      		)
				),
		   	'group' => 'Invitation.event_id',
		   	));


SELECT 
`Organizer`.`mobile`, GROUP_CONCAT(day_id) as dias
, `Invitation`.`mobile`, `Guest`.`name`, `Guest`.`father_surname`
, `Organizer`.`name`, `Organizer`.`father_surname`, `Guest`.`mobile`
, `Invitation`.`created`, `Invitation`.`repeat_from`, `Invitation`.`repeat_to`
, `Event`.`from`, `Invitation`.`repeat`, `Invitation`.`id` 
FROM 
`safecard`.`invitations` AS `Invitation` 
LEFT JOIN `safecard`.`days_events` ON (`Invitation`.`event_id` = 'days_events.event_id') 
LEFT JOIN `safecard`.`events` AS `Event` ON (`Invitation`.`event_id` = `Event`.`id`) 
LEFT JOIN `safecard`.`users` AS `Organizer` ON (`Invitation`.`organizer_id` = `Organizer`.`id`) 
LEFT JOIN `safecard`.`users` AS `Guest` ON (`Invitation`.`guest_id` = `Guest`.`id`) 
WHERE 
`Invitation`.`sent_mobile` = 0 AND `Invitation`.`relation_id` <> 1 
AND `Invitation`.`status` = 1 AND `Invitation`.`mobile` = '+56993188057' 
GROUP BY `Invitation`.`event_id`

*/

		$inv = $this->paginate($this->Invitation,$cond);
		$this->set('invitations', $inv);


		
		//$db =& ConnectionManager::getDataSource('default');
		//$db->showLog();
		if($this->request->data['Invitation']['mobile']){
			$db = ConnectionManager::getDataSource('default');
			$ed = $db->query("	SELECT event_id,GROUP_CONCAT(day_id) as grupo 
								FROM days_events 
								LEFT JOIN invitations USING(event_id) 
								LEFT JOIN users as guest ON invitations.guest_id=guest.id  
								LEFT JOIN users as organizer ON invitations.organizer_id=organizer.id 
								WHERE organizer.mobile='".$this->request->data['Invitation']['mobile']."' OR invitations.mobile='".$this->request->data['Invitation']['mobile']."'
								GROUP BY event_id;");

			$n = array('1','2','3','4','5','6','7');
			$d = array(' Lu',' Ma',' Mi',' Ju',' Vi',' Sa',' Do');

			foreach($ed as $val)
				$newed[$val['days_events']['event_id']] = str_replace($n,$d,$val[0]['grupo']);

			//pr($newed);
			$this->set('dias', $newed);
		}
	}
	
/**
 * admin_index method
 *
 * @return void
 */
	public function index() {
		$this->Invitation->recursive = 1;
			
		$invitations_received = $this->Invitation->find('all', array('conditions'=> array(
			'OR'=>array(
				array('Invitation.guest_id' => $this->Session->read('Auth.User.id')),
				array('Invitation.mobile' => $this->Session->read('Auth.User.mobile'))
			),
			'Invitation.sent_mobile' => 0,
			'Event.to >= '=> date('Y-m-d H:i:s'),
			'Invitation.relation_id <> '=>1,
			'Invitation.status' => 1
		)));
		
		$invitations_sent = $this->Invitation->find('all', array('conditions'=> array(
			'Invitation.organizer_id'=>$this->Session->read('Auth.User.id'),
			'Invitation.sent_mobile' => 0,
			'OR'=>array(
				'Invitation.requested' => 0,
				'AND'=>array('Invitation.requested' => 1, 'Invitation.status'=>1)
			),
			'Invitation.relation_id <>' => 1,
			'Event.to >= '=> date('Y-m-d H:i:s'),
			'Invitation.status' => 1
		)));
		
		$invitations_request = $this->Invitation->find('all', array('conditions'=> array(
			'OR'=>array(
				array('Invitation.organizer_id'=>$this->Session->read('Auth.User.id')),
				array('Invitation.guest_id'=>$this->Session->read('Auth.User.id'))
			),
			'Invitation.sent_mobile' => 0,
			'Invitation.requested' => 1,
			'Invitation.status' => 0,
			'Event.to >= '=> date('Y-m-d H:i:s')
		)));
		
		$this->set('invitations_received', $invitations_received);
		$this->set('invitations_sent', $invitations_sent);
		$this->set('invitations_request', $invitations_request);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Invitation->exists($id)) {
			throw new NotFoundException(__('Invitación Invalida'));
		}
		$options = array('conditions' => array('Invitation.' . $this->Invitation->primaryKey => $id));
		$this->set('invitation', $this->Invitation->find('first', $options));
	}
	
	public function update_repeat(){
		$this->layout = false;
		$this->autoRender = false;

		$this->loadModel('Manual');
		
		$invitations = $this->Invitation->find('all', array(
			'conditions'=>array(
				'Invitation.status'=>1,
				'Invitation.repeat'=>1, 
				'Invitation.repeat_from <= '=>date('Y-m-d'), 
				'Invitation.repeat_to >= '=>date('Y-m-d')
			)
		));
		//pr($invitations);
		if(!empty($invitations)){
			foreach($invitations as $key=>$invitation){
				$this->Invitation->Event->id = $invitation['Event']['id'];
				$event = $this->Invitation->Event->read(null, $invitation['Event']['id']);

				$manual = $this->Manual->findByInvitationId($invitation['Invitation']['id']);
				//echo $invitation['Invitation']['id'];
				//pr($manual); exit;
				if(!empty($manual)){
					$this->Manual->id = $manual['Manual']['id'];
					$this->Manual->read(null, $this->Manual->id);
				}

				$diferencia = 99;
				$next_day = $next_day_aux = date('Y-m-d');
				foreach($event['Day'] as $key=>$days){
					$days_start_time = $days['DaysEvent']['from'];
					$days_end_time =  $days['DaysEvent']['to'];
				
					$day_name = getDayName($days['code']);
					
					$datetime1 = new DateTime(date('Y-m-d'));
					$next_day_aux = date('Y-m-d', strtotime("next $day_name", strtotime(date('Y-m-d'))));
					$datetime2 = new DateTime($next_day_aux);
					$interval = $datetime1->diff($datetime2);
					
					if(date('w') == $days['code']){
						$diferencia = 0;
						$next_day = date('Y-m-d');
					} else {
						if($interval->format('%a') < $diferencia){
							$diferencia = $interval->format('%a');
							$next_day = date('Y-m-d', strtotime("next $day_name", strtotime(date('Y-m-d'))));
						}
					}
				}
				
				$this->Invitation->Event->saveField('from', $next_day.' '.$days_start_time);
				$this->Invitation->Event->saveField('to', $next_day.' '.$days_end_time);
				
				if(!empty($manual)){
					$this->Manual->saveField('start_date', $next_day.' '.$days_start_time);
					$this->Manual->saveField('end_date', $next_day.' '.$days_end_time);
					$this->Manual->saveField('sent_gate', '0');
				}

				$this->Invitation->read(null, $invitation['Invitation']['id']);
				$this->Invitation->saveCode($invitation['Invitation']['id']);
				mail("fajardo.esteban@gmail.com","Actualizacion de invitaciones repetitivas", "ID invitacion: ".$invitation['Invitation']['id']." dia ".$next_day." ".$days_start_time." - ".$days_end_time);
			}
		}
	}
	
	public function QrGenerator(){
		
		if($this->request->data){
			$opts = array( 'http'=>array( 'user_agent' => 'PHPSoapClient'));
			$context = stream_context_create($opts);
			$client = new SoapClient('http://dev.dribble.cl/~efajardo/safecard/soap/WsMobile/wsdl',
									 array('stream_context' => $context,
										   'cache_wsdl' => WSDL_CACHE_NONE));
			
			$result = $client->soap_AddInvitations(array(
				'subject'=> 'testing Benavides',
				'start_date' => $this->request->data['Event']['dfrom'],
				'start_time' => $this->request->data['Event']['hfrom'],
				'end_date' => $this->request->data['Event']['dto'],
				'end_time' => $this->request->data['Event']['hto'],
				'house_id' => $this->request->data['Event']['house_id'],
				'mobile_organizer' => $this->request->data['Event']['mobile_organizer'],
				'mobile_guests' => $this->request->data['Event']['mobile_guests'],
				'name_guests' => $this->request->data['Event']['name_guests'],
				'relation_id' => $this->request->data['Event']['relation_id'],
				'repeat' => 0,
				'days' => ''
			));
				
			$this->set('aes',$result->aes);
		}
	}
	
}
