<?php
App::uses('AppController', 'Controller');
/**
 * Notifications Controller
 *
 * @property Notification $Notification
 */
class NotificationsController extends AppController {
    
    public $paginate = array(
        'Notification'=>array(
            'order'=>array('created'=>'DESC'),
            'limit'=>10
        )
    );
/**
 * admin_index method
 *
 * @return void
 */
    public function admin_index() {
        $this->Notification->recursive = 0;
        $this->set('notifications', $this->paginate());
    }

}
