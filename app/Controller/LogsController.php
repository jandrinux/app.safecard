<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Users Controller
 *
 * @property User $User
 */
class LogsController extends AppController {
	
	public $paginate = array(
        'limit' => 2,
        'order' => array(
            'Log.created' => 'DESC'
        )
    );
}