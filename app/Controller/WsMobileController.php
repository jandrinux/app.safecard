<?php  
App::uses('AppController', 'Controller');
App::import('Vendor', 'AES');
App::import('Vendor','Twilio/Twilio');
App::uses('AuthComponent', 'Controller/Component');
class WsMobileController extends AppController{ 
	public $uses = array('Invitation', 'User', 'Owner', 'Contact', 'HousesUser', 'House','Relation','Condo','Student','Barrier','Manual');
	 
	public $components = array('Soap'); 
	
	function beforeFilter() {
		$this->Auth->allow();
		$this->Soap->__settings = array( 
            'wsdl' => "WSDLMobile", 
            'wsdlAction' => 'wsdl', 
            'prefix' => 'soap', 
            'action' => array('service'), 
        );  
		parent::beforeFilter();
	}
	
	public function soap_wsdl(){ 
		//will be handled by SoapComponent 
	} 
	public function soap_service(){ 
		//will be handled by SoapComponent 
	} 
	
	/*
	* Creación de usuario
	*/
	public function soap_AddUser($data){
		$obj = new stdClass(); 
		$obj->result = "NACK";
		
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->result = "NACK";
				return $obj;
			}
		} */	
		$this->User->create();
		$role = $this->User->Role->findByCode('PROP');
		$this->request->data['role_id'] = $role['Role']['id'];
		
		$country = $this->User->Country->findByIso2($data->iso2);
		
		if(!empty($country)){
			$this->request->data['country_id'] = $country["Country"]["id"];
			
			$this->request->data['mobile'] = $data->mobile;
			$this->request->data['name'] = $data->name;
			$this->request->data['father_surname'] = $data->father_surname;
			$this->request->data['password'] = sprintf("%04s", $data->pin);
			$this->request->data['device'] = $data->device;
			
			if ($this->User->save($this->request->data)) {
				//Merge entre propietarios cargados por Administradora y usuarios registrados
				//$this->loadModel('Owner');
				//$owners = $this->Owner->find('all', array('conditions'=>array('Owner.mobile'=>$this->User->field('mobile'))));
				//$user = $this->User->read(null,$this->User->id);
				
				/*if(!empty($owners)){
					$houses = array();
					$i = 0;
					foreach($owners as $key=>$owner){
						$this->Owner->House->HousesUser->create();
						$houses[$i]['HousesUser'] = array(
							'user_id'=>$this->User->id,
							'house_id'=>$owner['Owner']['house_id'],
							'owner'=>1,
							'active'=>1
						);
						$i++;
					}
					if($this->Owner->House->HousesUser->saveMany($houses)){
						$obj->result = "ACK";
					} else {
						$this->User->deleteAll(array('User.id'=>$this->User->id));
						$obj->result = "NACK";
					}
				} else {*/
					$obj->result = "ACK";
				//}
			} else {
				$obj->result = "NACK";
			}
		}
		
		return $obj;
	}
	
	/*
	* Detalle de usuario y/o contacto
	*/
	public function soap_DataContact($data){ 
		$obj = new stdClass();
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->name = $obj->father_surname = $obj->mobile = $obj->email = $obj->image = $obj->houses = $obj->pin = '';
				return $obj;
			} else {
				$contacto = $this->User->findByMobile($data->mobile);
			}
		} else {*/
			$contacto = $this->User->findByMobile($data->mobile); 
					//}		
		if(!empty($contacto)){
			
			//$this->Invitation->query("INSERT INTO coordlogs SET user_id = '".$contacto['User']['id']."', mobile = '".$data->mobile."', lat ='".$data->lat."', lng ='".$data->lng."', created = NOW()");
			$obj->name = $contacto['User']['name'];
			$obj->father_surname = $contacto['User']['father_surname'];
			$obj->mobile = $contacto['User']['mobile'];
			$obj->email = $contacto['User']['email']?$contacto['User']['email']:"NULL";
			$obj->image = $contacto['User']['image'];
			$obj->pin = $contacto['User']['password'];
			
			$houses = $this->HousesUser->find('all', array(
				'conditions'=>array(
					'HousesUser.user_id'=>$contacto['User']['id'],
					'HousesUser.active' => 1
			)));
			
			$houses_list = array();
			foreach($houses as $key=>$house){
				$recinto = '';
				if(is_null($house['HousesUser']['house_id'])){
					$house_ = $this->Student->read(null, $house['HousesUser']['student_id']);
					$model = "Student";
					$field_name = "full_name";
					$recinto = "School";
					
					$invitation = $this->Invitation->find('first', array('conditions'=>array(
						'Invitation.organizer_id' => $contacto['User']['id'],
						'Invitation.guest_id' => $contacto['User']['id'],
						'Invitation.student_id' => $house_[$model]['id'],
						'OR'=>array(
							array('Invitation.relation_id' => 1),
							array('Invitation.relation_id'=>2)
						),
						'Invitation.status' => 1
					)));
					
				} else {
					$house_ = $this->House->read(null,$house['HousesUser']['house_id']);
					$model = "House";
					$field_name = "name";
					$recinto = "Condo";
					
					$invitation = $this->Invitation->find('first', array('conditions'=>array(
						'Invitation.organizer_id' => $contacto['User']['id'],
						'Invitation.guest_id' => $contacto['User']['id'],
						'Invitation.house_id' => $house_[$model]['id'],
						'OR'=>array(
							array('Invitation.relation_id' => 1),
							array('Invitation.relation_id'=>2)
						),
						'Invitation.status' => 1
					)));
				}
				
				$aes = $invitation['Invitation']['aes']?$invitation['Invitation']['aes']:"NULL";
				
				$houses_list['houses'][$key] = array(
					"id"=>$house_[$model]["id"], 
					"recinto"=>$recinto, 
					"condo_id"=>$house_[$recinto]["id"],
					"house_name"=>$house_[$model][$field_name], 
					"condo_name"=>$house_[$recinto]["name"], 
					"aes"=>$aes
				);
			}
			
			$obj->houses = json_encode($houses_list);
		} else {
			$obj->name = $obj->father_surname = $obj->mobile = $obj->email = $obj->image = $obj->houses = $obj->pin = '';
		}
		
		return $obj;
	}
	
	/*
	* Invitaciones recibidas
	*/
	public function soap_InvitationsReceived($data){
		$obj = new stdClass();
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->invitations_received = "NACK";
				return $obj;
			}
		}*/
		$invitations_received = array();
		$invitations = array();
		
		$guest = $this->User->findByMobile($data->mobile_guest);
		
		if($data->mobile_guest != ""){		
			$invitations = $this->Invitation->find("all",array(
			'conditions'=>array(
				'OR'=>array(
					array('Invitation.guest_id' => ($guest['User']['id']>0)?$guest['User']['id']:'NULL'),
					array('Invitation.mobile' => $data->mobile_guest)
				),
				'Invitation.sent_mobile' => 0,
				'Event.to >= '=> date('Y-m-d H:i:s'),
				'NOT'=>array(
					'Invitation.relation_id' => array(1,2)
				)
			)));
		}
		
		if(!empty($invitations)){
			foreach($invitations as $key=>$invitation){	
				
				if(!is_null($invitation['Invitation']['house_id'])){
					$house_name = $invitation['House']['name'];
				} else {
					$house_name = $invitation['Student']['full_name'];
				}
			
				$invitations_received['invitations'][$key] = array(
						'id' => $invitation['Invitation']['id'],
						'organizer_name' => $invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname'],
						'organizer_mobile' => $invitation['Organizer']['mobile'],
						'start_date' => $invitation['Event']['from'],
						'end_date' => $invitation['Event']['to'],
						'condo_name' => $invitation['Condo']['name'],
						'condo_address'=>$invitation['Condo']['address'],
						'house_name' => $house_name, //$invitation['House']['name'],
						'code' => $invitation['Invitation']['code'],
						'aes' => $invitation['Invitation']['aes'],
						'status' => $invitation['Invitation']['status'],
						'lat' => $invitation['Condo']['lat'],
						'lon' => $invitation['Condo']['long'],
						'created' => $invitation['Invitation']['created']
				);
			}
		}
		
		$obj->invitations_received = json_encode($invitations_received);
		return $obj;
	}
	
	
	/*
	* Invitaciones enviadas
	*/
	public function soap_InvitationsSent($data){
		$obj = new stdClass();
		$invitations_sent = array();
		
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->invitations_sent = "NACK";
				return $obj;
			}
		}*/
		$organizer = $this->User->findByMobile($data->mobile_organizer);
		if(!empty($organizer)){
		
			$invitations = $this->Invitation->find("all",array(
			'conditions'=>array(
				'OR'=>array(
					'Invitation.requested' => 0,
					'AND'=>array(
						'Invitation.requested' => 1, 
						'Invitation.status'=>1
					)
				),
				'Invitation.organizer_id'=>$organizer['User']['id'],
				'Invitation.sent_mobile' => 0,
				'Event.to >= '=> date('Y-m-d H:i:s'),
				'NOT'=>array(
					'Invitation.relation_id' => array(1,2)
				)
			)));
			
			if(!empty($invitations)){
				foreach($invitations as $key=>$invitation){	
					$guest_name = '';
					$guest_mobile = $invitation['Invitation']['mobile']?$invitation['Invitation']['mobile']:'';
					if(!empty($invitation['Guest']['mobile'])){
						$guest_name = $invitation['Guest']['name'].' '.$invitation['Guest']['father_surname'];
						$guest_mobile = $invitation['Guest']['mobile'];
					} else {
						$guest_name = $invitation['Contact']['name'];
						$guest_mobile = $invitation['Contact']['mobile'];
					}
					
					if(!is_null($invitation['Invitation']['house_id'])){
						$house_name = $invitation['House']['name'];
					} else {
						$house_name = $invitation['Student']['full_name'];
					}
					
					$invitations_sent['invitations'][$key] = array(
							'id' => $invitation['Invitation']['id'],
							'guest_name' => $guest_name,
							'guest_mobile' => $guest_mobile,
							'start_date' => $invitation['Event']['from'],
							'end_date' => $invitation['Event']['to'],
							'condo_name' => $invitation['Condo']['name'],
							'condo_address'=>$invitation['Condo']['address'],
							'house_name' => $house_name, //$invitation['House']['name'],
							'code' => $invitation['Invitation']['code'],
							'aes' => $invitation['Invitation']['aes'],
							'status' => $invitation['Invitation']['status'],
							'lat' => $invitation['Condo']['lat'],
							'lon' => $invitation['Condo']['long'],
							'created' => $invitation['Invitation']['created']
					);
				}
			}
		}
		
		$obj->invitations_sent = json_encode($invitations_sent);
		return $obj;
	}
	
	/*
	* Invitaciones solicitadas
	*/
	public function soap_InvitationsRequested($data){
		$obj = new stdClass();
		$invitations_requested = array();
		
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->invitations_requested = "NACK";
				return $obj;
			}
		}*/
		$organizer = $this->User->findByMobile($data->mobile_organizer);
		
		if(!empty($organizer)){		
			$invitations = $this->Invitation->find("all",array('conditions'=>array(
				'Invitation.organizer_id'=>$organizer['User']['id'],
				'Invitation.sent_mobile' => 0,
				'Invitation.requested' => 1,
				'Invitation.status' => 0,
				'Event.to >= '=> date('Y-m-d H:i:s')
			)));
			
			if(!empty($invitations)){
				foreach($invitations as $key=>$invitation){	
					
					if(!is_null($invitation['Invitation']['house_id'])){
						$house_name = $invitation['House']['name'];
					} else {
						$house_name = $invitation['Student']['full_name'];
					}
					
					$invitations_requested['invitations'][$key] = array(
						'id' => $invitation['Invitation']['id'],
						'guest_name' => $invitation['Guest']['name'].' '.$invitation['Guest']['father_surname'],
						'guest_mobile' => $invitation['Guest']['mobile'],
						'start_date' => $invitation['Event']['from'],
						'end_date' => $invitation['Event']['to'],
						'condo_name' => $invitation['Condo']['name'],
						'condo_address'=>$invitation['Condo']['address'],
						'house_name' => $house_name, //$invitation['House']['name'],
						'code' => $invitation['Invitation']['code'],
						'aes' => $invitation['Invitation']['aes'],
						'status' => $invitation['Invitation']['status'],
						'lat' => $invitation['Condo']['lat'],
						'lon' => $invitation['Condo']['long'],
						'created' => $invitation['Invitation']['created']
					);
				}
			}
		}
		$obj->invitations_requested = json_encode($invitations_requested);
		return $obj;
	}
	
	/*
	* Creación de invitación
	*/
	public function soap_AddInvitations($data){
		$obj = new stdClass();
		
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->result = "NACK";
				return $obj;
			}
		}*/
		$this->loadModel('Day');
		$this->loadModel('DaysEvent');
		$event['Event']['subject'] = $data->subject;
		
		$condo = $this->Condo->findById($data->condo_id);
		//type | 0: condominio | 1: colegio
		if($condo['Condo']['type']==0){
			$prop = "house_id";
			$house = $this->House->findById($data->house_id);
		} else if($condo['Condo']['type']==1) {
			$prop = "student_id";
			$house = $this->Student->findById($data->house_id);
		}	
		$days_tmp = array();
		if($data->repeat){
			$days = explode(',', $data->days);	
			$days_tmp = explode(',', $data->days);
			sort($days_tmp);
			sort($days); //días ya ordenados (estos son los días seleccionados por el usuario) 
			
			$day_name = "";
			$flag = false;
			$flag1 = 1;
			// Inicio código PRG 
            $first_date = $data->start_date;
			$last_date = $data->end_date;
			$days_total = array();
			
			if(count($days) == 1)
			{	
				for($j=0;strtotime($first_date) <= strtotime($last_date); $j++ ){
			
					$first_date_day = date('w',strtotime($first_date));
					$indice = in_array($first_date_day,$days);
					
					if($indice){
						unset($days[$indice]);
						break;
					}
					$first_date = date('Y-m-d',strtotime($first_date."+ 1 day"));
				}
			}
			
			for($i=0; strtotime($first_date) <= strtotime($last_date); $i++ ){ 
				$first_date_day = date('w',strtotime($first_date));
				
				$indice = array_search($first_date_day, $days);
				if(in_array($first_date_day,$days)){
					unset($days[$indice]);
				}
				$first_date = date('Y-m-d',strtotime($first_date."+ 1 day"));
				
    			if(count($days)== null){
					$flag = true;
				}
			
				if($flag) {
					$flag1 = 0;
					break;
				}
			}
			if($flag1){
				$obj->aes = null;
				$obj->result = "NACK";
				return $obj;
			} else {
				$actual_day = date('w');
				$actual_day_temp = date('Y-m-d'); 
				$day_name = "";
				$flag = false;
				
				if(in_array($actual_day, $days_tmp)){
					
						/*if($actual_day_temp > $last_date){
							$obj->aes = null;
							$obj->result = "NACK";
							return $obj;
						}
						
						else{*/
						//pr($first_date);
						//exit;
						if($day_name == ""){
							$day_name = getDayName($actual_day[0]);
						}
							//pr($first_date);
							//pr($actual_day_temp);
							//exit;
							$first_date = $actual_day_temp;
							//pr($first_date);
							//pr($day_name);
							//exit;
							$event['Event']['from'] = date('Y-m-d H:i:s', strtotime("$day_name".$data->start_date." ".$data->start_time));
							$event['Event']['to'] = date('Y-m-d H:i:s', strtotime("$day_name".$data->start_date." ".$data->end_time));
							
						//}
				} else {
				 	
					foreach($days_tmp as $key=>$day){
						if($day > $actual_day){
							$day_name = getDayName($day);
							$flag = true;
						}
						if($flag) break;
					}
				
					if($day_name == ""){
						$day_name = getDayName($days_tmp[0]);
					}
					$event['Event']['from'] = date('Y-m-d H:i:s', strtotime("$day_name".$data->start_time, strtotime($data->start_date)));
					$event['Event']['to'] = date('Y-m-d H:i:s', strtotime("$day_name".$data->end_time, strtotime($data->start_date)));
					
				}
				//pr($event);
				//exit;
			
			
			//Fin modificación código PRG
			}
		} else {
			$event['Event']['from'] = date('Y-m-d H:i:s', strtotime($data->start_date.' '.$data->start_time));
			$event['Event']['to'] = date('Y-m-d H:i:s', strtotime($data->end_date.' '.$data->end_time));
		}
		
		if($condo['Condo']['type']==0){
			$event['Event']['house_id'] = $data->house_id;
			$event['Event']['student_id'] = NULL;
		} else if($condo['Condo']['type']==1){
			$event['Event']['student_id'] = $data->house_id;
			$event['Event']['house_id'] = NULL;
		}
		
		$organizer = $this->User->findByMobile($data->mobile_organizer);
		$event['Event']['user_id'] = $organizer['User']['id'];
		if($this->Invitation->Event->save($event)){
			
			//Guardar días en que evento se repetirá semanalmente
			if($data->repeat){
				foreach($days_tmp as $key=>$day){
					$dia = $this->Day->findByCode($day);
					$data_day_event[$key]['DaysEvent']['event_id'] = $this->Invitation->Event->id;
					$data_day_event[$key]['DaysEvent']['day_id'] = $dia['Day']['id'];
					$data_day_event[$key]['DaysEvent']['from'] = $data->start_time;
					$data_day_event[$key]['DaysEvent']['to'] = $data->end_time;
				}
				
				$this->DaysEvent->saveMany($data_day_event);
			}
			
			//$condo = $this->Invitation->House->getCondominioByHouse($event['Event']['house_id']);
			// $house = $this->Invitation->House->findById($data->house_id);
			$mobile_guests = explode(",", $data->mobile_guests);
			$name_guests = explode(",", $data->name_guests);
			
			foreach($mobile_guests as $key=>$mobile_guest){
			
				$mobile_guest = str_replace(' ','',$mobile_guest);
				
				/* Se registra contacto para disponibilidad en web */
				$contacto = $this->Contact->find('first', array('conditions'=>array(
					'Contact.mobile' => $mobile_guest,
					'Contact.organizer_id' => $this->Invitation->Event->field('user_id')
				)));
				
				if(empty($contacto)){
					$contacto = array(
						'organizer_id' => $this->Invitation->Event->field('user_id'),
						'name' => $name_guests[$key],
						'mobile' => $mobile_guest
					);
					$this->Contact->create();
					$this->Contact->save($contacto);
					$invitation[$key]['Invitation']['contact_id'] = $this->Contact->id;
				} else {
					$invitation[$key]['Invitation']['contact_id'] = $contacto['Contact']['id'];
				}
				
				$invitado = $this->User->find('first', array('conditions'=>array('mobile'=>$mobile_guest, 'OS'=>array('AND','IOS'))));
				
				$invitation[$key]['Invitation']['accessed'] = date('y').str_pad(1,9,"0",STR_PAD_LEFT);
				$invitation[$key]['Invitation']['organizer_id'] = $this->Invitation->Event->field('user_id');
				$manual[$key]['Manual']['name_organizer'] = $organizer['User']['name'].' '.$organizer['User']['father_surname'];
				$manual[$key]['Manual']['mobile_organizer'] = $organizer['User']['mobile'];
				$manual[$key]['Manual']['password_organizer'] = $organizer['User']['password'];
				$manual[$key]['Manual']['start_date'] = $this->Invitation->Event->field('from');
				$manual[$key]['Manual']['end_date'] = $this->Invitation->Event->field('to');
				$manual[$key]['Manual']['type'] = 'INV';
				
				if($condo['Condo']['type']==0){
					$invitation[$key]['Invitation']['house_id'] = $data->house_id;
					$invitation[$key]['Invitation']['student_id'] = NULL;
					$manual[$key]['Manual']['house_id'] = $data->house_id;
					$manual[$key]['Manual']['house_name'] = $house['House']['name'];
					$manual[$key]['Manual']['student_id'] = NULL;
					$manual[$key]['Manual']['student_name'] = '';
					$manual[$key]['Manual']['student_school_id'] = NULL;
				} else if($condo['Condo']['type']==1){
					$invitation[$key]['Invitation']['student_id'] = $data->house_id;
					$invitation[$key]['Invitation']['house_id'] = NULL;
					$manual[$key]['Manual']['house_id'] = NULL;
					$manual[$key]['Manual']['house_name'] = '';
					$manual[$key]['Manual']['student_id'] = $data->house_id;
					$manual[$key]['Manual']['student_name'] = $house['Student']['full_name'];
					$manual[$key]['Manual']['student_school_id'] = $house['Student']['stdschool_id'];
				}
				
				$invitation[$key]['Invitation']['condo_id'] = $data->condo_id;//$house['Condo']['id'];
				$manual[$key]['Manual']['condo_id'] = $data->condo_id;
				$invitation[$key]['Invitation']['event_id'] = $this->Invitation->Event->id;
				$invitation[$key]['Invitation']['mobile'] = $mobile_guest;
				$invitation[$key]['Invitation']['relation_id'] = $data->relation_id;
				$invitation[$key]['Invitation']['repeat'] = $data->repeat ? $data->repeat:0;
				$manual[$key]['Manual']['repeat'] = $data->repeat ? $data->repeat:0;
				
				if($data->repeat){
					$invitation[$key]['Invitation']['repeat_from'] = date('Y-m-d', strtotime($data->start_date));
					$invitation[$key]['Invitation']['repeat_to'] = date('Y-m-d', strtotime($data->end_date));
				}
				
				if(!empty($invitado)){
					$invitation[$key]['Invitation']['guest_id'] = $invitado['User']['id'];	
					$manual[$key]['Manual']['name_guest'] = $invitado['User']['name'].' '.$invitado['User']['father_surname'];
					$manual[$key]['Manual']['mobile_guest'] = $invitado['User']['mobile'];
					$manual[$key]['Manual']['password_guest'] = $invitado['User']['password'];
					$manual[$key]['Manual']['manual'] = 0;
					$invitation[$key]['Invitation']['manual'] = 0;	
				} else {
					//Envio SMS con clave de 4 dígitos
					//$pin = substr($mobile_guest,-4).rand(1000,9999);
					$pin = rand(1000,9999);
					$invitation[$key]['Invitation']['manual'] = 1;	
					$manual[$key]['Manual']['manual'] = 1;	
					//$invitation[$key]['Invitation']['pin'] = AuthComponent::password(substr($pin,-4));
					$invitation[$key]['Invitation']['pin'] = AuthComponent::password($pin);
	
					//[Nombre Propietario] te invitó a [casa-condominio]. Ingresa descargando Safecard en Google Play y App Store o digita XXXXYYYY en el acceso
					if($condo['Condo']['type'] == 0) {
						$text = stripAccents($organizer['User']['name'].' te invito a la casa "'.$house['House']['name'].'" del condominio '.$condo['Condo']['name'].'. Ingresa al acceso con la app http://bit.ly/1I3fsUp o digita tu celular mas '.$pin);
					} else if($condo['Condo']['type'] == 1) {
						$text = stripAccents($organizer['User']['name'].' te autorizo a retirar a '.$house['Student']['full_name'].'. Ingresa al acceso con la app http://bit.ly/1I3fsUp o digita tu celular mas '.$pin);
					}
					$manual[$key]['Manual']['name_guest'] = $name_guests[$key];
					$manual[$key]['Manual']['mobile_guest'] = $mobile_guest;
					$manual[$key]['Manual']['password_guest'] = AuthComponent::password($pin);
					$this->send_sms($mobile_guest, $text);
				}
			}
			if($this->Invitation->saveMany($invitation)){
				//Se insertan datos al manual
				//$this->Manual->saveMany($manual);
				foreach($this->Invitation->inserted_ids as $key=>$invitation_id){
					$manual[$key]['Manual']['invitation_id'] = $invitation_id; 
					$this->Manual->save($manual[$key]['Manual']);
					$this->Invitation->saveCode($invitation_id);					
					$this->sendNotification($invitation_id,'create');
					$inv = $this->Invitation->read(null, $invitation_id);
				}
				$obj->aes = $inv['Invitation']['aes'];
				$obj->result = "ACK";		
			}
		} 
		
		return $obj;
	}
	public function soap_SmsConfirm($data){ 
		$obj = new stdClass();
		$obj->result = "NACK";
		$mesage = str_ireplace('[CODE]',$data->msg,Configure::read('SMS_CONFIRM'));
		if($this->send_sms($data->mobile, $mesage)) 
			$obj->result = "ACK";
		//$obj->result = $this->send_sms($data->mobile, $mesage);
		
		return $obj;
	}
	public function send_sms($to, $msg){
		
		//$this->infobip_func($to, $msg);
		//$this->twilio_func($to, $msg);
		if(!$this->smartel_func($to, $msg))
			$this->sistek_func($to, $msg);
		
		return 1;
	}
	public function sistek_func($to, $msg){ 
		$url = 'http://sms.sistek.cl/sms.asmx/Sms';
		$fields = array(
			'id' => Configure::read('IdSMS'),
			'text' => $msg,
			'to_number' => str_replace('+','',$to),
			'username' => Configure::read('UsernameSMS'),
			'password' => Configure::read('PassSMS'),
			'type'=>'SMS'
		);
		
		$fields_string = http_build_query($fields);
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$result = simplexml_load_string($result);
		
		curl_close($ch);
		$this->Invitation->query("INSERT INTO smslogs SET mobile = '".$to."', texto ='".$msg."', proveedor='sistek', result='".$result->STATUS.': '.$result->STATUSD."', created = NOW()");
		if($result->STATUS === '000')
			return 1;
		else
			return 0;
	}
	
	public function twilio_func($to, $msg){
		$account_sid = Configure::read('Twilio.SID'); 
		$auth_token = Configure::read('Twilio.TOKEN'); 
		$client = new Services_Twilio($account_sid, $auth_token); 
		 
		$result = $client->account->messages->create(array( 
			'To' => $to, 
			'From' => Configure::read('Twilio.FROM'), 
			'Body' => $msg,
		));
		
		$this->Invitation->query("INSERT INTO smslogs SET mobile = '".$to."', texto ='".$msg."', proveedor='twilio', result='".$result->status.': '.$result->error_code."', created = NOW()");
		if($result->status != 'undelivered' AND $result->status != 'failed')
			return 1;
		else
			return 0;
		//ERRORES
		//https://www.twilio.com/docs/api/rest/message#error-values
	}
	public function smartel_func($to, $msg){
		$JSON='
		{
			"authentication": {
			 	"username": "'.Configure::read('Smartel.Username').'",
			 	"password": "'.Configure::read('Smartel.Password').'"
			},
			"messages": [{
			 	"sender": "'.str_replace('+','',$to).'",
			 	"text": "'.$msg.'",
			 	"datacoding": "longSMS",
			 	"recipients": [{
			 		"gsm": "'.str_replace('+','',$to).'"
			 	}]
			}]
		}';
		$url = 'http://193.105.74.159/api/v3/sendsms/json';
		$fields = array('JSON' => $JSON);
		
		$fields_string = http_build_query($fields); 
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$result1 = json_decode($result);
		
		curl_close($ch); 
		$this->Invitation->query("INSERT INTO smslogs SET mobile = '".$to."', texto ='".$msg."', proveedor='smartel', result='".$result."', created = NOW()");
		
		if($result1->results[0]->status == 0)
			return 1;
		else
			return 0;
	}
	public function infobip_func($to, $msg){
		$url = Configure::read('InfoBip.URL'); 
		$url = str_ireplace("[mobile]",str_replace('+','',$to),$url);
		$url = str_ireplace("[msg]",$msg,$url);
		//$result = file_get_contents($url);
   		
   		$ch = curl_init();  
    	curl_setopt($ch,CURLOPT_URL,$url);
    	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    	$result=curl_exec($ch);
    	curl_close($ch);
		$result = simplexml_load_string($result);
		$this->Invitation->query("INSERT INTO smslogs SET mobile = '".$to."', texto ='".$msg."', proveedor='infobip', result='".$result->status."', created = NOW()");
		if($result->status != '0')
			return 1;
		else
			return 0;
		//DOCUMENTACION
		/*
		<results>
			<result>
				<status>0</status>
				<messageid>115042119290882277</messageid>
				<destination>56988802556</destination>
			</result>
		</results>
		*/
		//http://www.infobip.com/themes/site_themes/infobip/documentation/Infobip_HTTP_API_and_SMPP_specification.pdf
	}
	/*
	* Solicitar Invitacion
	*/
	public function soap_RequestInvitation($data){
		$obj = new stdClass();
		
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->result = "NACK";
				return $obj;
			}
		}*/
		$event['Event']['subject'] = $data->subject;
		$event['Event']['from'] = date('Y-m-d H:i:s', strtotime($data->start_date.' '.$data->start_time));
		$event['Event']['to'] = date('Y-m-d H:i:s', strtotime($data->end_date.' '.$data->end_time));
		$event['Event']['house_id'] = $data->house_id;
		
		$organizer = $this->User->findByMobile($data->mobile_organizer);
		$event['Event']['user_id'] = $organizer['User']['id'];
		 
		$obj->result = "NACK";
		
		//pr($event);exit;
		if($this->Invitation->Event->save($event)){
			$house = $this->Invitation->House->findById($data->house_id);
			// $name_guests = explode(",", $data->name_guest);
			
			$guest = $this->User->findByMobile($data->mobile_guest);
			
			$invitation['Invitation']['organizer_id'] = $this->Invitation->Event->field('user_id');
			$invitation['Invitation']['house_id'] = $data->house_id;
			$invitation['Invitation']['condo_id'] = $house['Condo']['id'];
			$invitation['Invitation']['event_id'] = $this->Invitation->Event->id;
			$invitation['Invitation']['guest_id'] = $guest['User']['id'];
			//$invitation['Invitation']['relation_id'] = $data->relation_id;
			$invitation['Invitation']['requested'] = 1;
			$invitation['Invitation']['status'] = 0;
				
			if($this->Invitation->save($invitation)){
				$this->Invitation->saveInvalidCode($this->Invitation->id);				
				$this->sendNotification($this->Invitation->id,'request');
				$manuals['Manual']['condo_id'] = $house['Condo']['id'];
				$manuals['Manual']['name_guest'] = $guest['User']['name'].' '.$guest['User']['father_surname'];
				$manuals['Manual']['mobile_guest'] = $guest['User']['mobile'];
				$manuals['Manual']['password_guest'] = $guest['User']['password'];
				$manuals['Manual']['name_organizer'] = $organizer['User']['name'].' '.$organizer['User']['father_surname'];
				$manuals['Manual']['mobile_organizer'] = $organizer['User']['mobile'];
				$manuals['Manual']['password_organizer'] = $organizer['User']['password'];
				$manuals['Manual']['start_date'] = $event['Event']['from'];
				$manuals['Manual']['end_date'] = $event['Event']['to'];
				$manuals['Manual']['invitation_id'] = $this->Invitation->id;
				$manuals['Manual']['house_id'] = $data->house_id;
				$manuals['Manual']['house_name'] = $house['House']['name'];
				$manuals['Manual']['deleted'] = 1;
				$manuals['Manual']['repeat'] = 0;
				$manuals['Manual']['type'] = 'INV';
				$manuals['Manual']['manual'] = 0;
				$manuals['Manual']['sent_gate'] = 0;
				$this->Manual->create();
				$this->Manual->save($manuals);
				$obj->result = "ACK";
			}
		}
		
		return $obj;
	}
	
	public function soap_UpdatePin($data){
		$obj = new stdClass();
		$obj->out = "NACK";
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->out = "NACK";
				return $obj;
			}
		}*/
		$old_pin = sprintf("%04s", $data->old_pin);
		$old_pin = AuthComponent::password($old_pin);
		
		$user = $this->User->find('first', array('conditions'=>array('password'=>$old_pin, 'mobile'=>$data->mobile)));
		
		if(!empty($user)){
			$this->User->id = $user['User']['id'];
			$this->User->read(null,$this->User->id);
			$new_pin = sprintf("%04s", $data->new_pin);//$data->new_pin);
			if($this->User->saveField('password', $new_pin)){
				//Reinicio contador de administradores y conserjes
				/*$this->User->CondosUser->updateAll(
					array('CondosUser.sent_gate' => 0),
    				array('CondosUser.user_id' => $user['User']['id'])
    			);*/
    			//Reinicio contador de propietarios - residentes
    			/*$this->User->Invitation->updateAll(
    				array('Invitation.sent_gate' => 0),
    				array(
    					'Invitation.relation_id IN' => array(1,2),
    					'Invitation.status' => 1,
    					'Invitation.organizer_id' => $user['User']['id'],
    					'Invitation.guest_id' => $user['User']['id']
    				)
    			);*/
				$this->Manual->recursive = '-1';
				$this->Manual->updateAll(
					array(
						'Manual.sent_gate' => 0,
						'Manual.password_guest' => "'".$this->User->field('password')."'",
						'Manual.password_organizer' => "'".$this->User->field('password')."'",
						'Manual.modified' => "'".date('Y-m-d H:i:s')."'"
					),
					array(
						'Manual.type' => 'ACC',
						'Manual.mobile_organizer' => $this->User->field('mobile')
					)
				);
				$this->Manual->updateAll(
					array(
						'Manual.sent_gate' => 0,
						'Manual.password_guest' => "'".$this->User->field('password')."'",
						'Manual.modified' => "'".date('Y-m-d H:i:s')."'"
					),
					array(
						'Manual.type' => 'INV',
						'Manual.manual' => 0,
						'Manual.mobile_guest' => $this->User->field('mobile')
					)
				);
				$obj->out = "ACK";
			}
		}
		
		return $obj;
	}
	
	public function soap_RevokeInvitation($data){
		$obj = new stdClass();
		$obj->result = "NACK";
		
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->result = "NACK";
				return $obj;
			}
		}*/
		$this->Invitation->id = $data->invitation_id;
		$invitation = $this->Invitation->read(null, $this->Invitation->id);
		if ($this->Invitation->exists($data->invitation_id)) {
			if($this->Invitation->saveField('status', -1)){
				$this->Manual->recursive = '-1';
				$manual = $this->Manual->findByInvitationId($data->invitation_id);
				$this->Manual->id = $manual['Manual']['id'];
				$this->Manual->saveField('deleted','1');
				$this->Manual->saveField('sent_gate','0');
				$this->Manual->saveField('modified',date('Y-m-d H:i:s'));
				$this->Invitation->saveInvalidCode($data->invitation_id);
				$obj->result = "ACK";
			}
		} else {
			$obj->result  = "NACK";
		}
		
		return $obj;
	}
	
	public function soap_AcceptInvitation($data){
		$obj = new stdClass();
		$obj->result = "NACK";
		
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->result = "NACK";
				return $obj;
			}
		}*/
		if($data->invitation_id){
			$invitation = $this->Invitation->read(null, $data->invitation_id);
			if ($this->Invitation->exists($data->invitation_id)) {
				if($this->Invitation->saveField('status', 1)){
					$this->Invitation->saveCode($data->invitation_id);
					$this->sendNotification($this->Invitation->id,'accept');
					$this->Manual->recursive = '-1';
					$manual = $this->Manual->findByInvitationId($data->invitation_id);
					$this->Manual->id = $manual['Manual']['id'];
					$this->Manual->saveField('deleted','0');
					$this->Manual->saveField('modified',date('Y-m-d H:i:s'));
					$obj->result = "ACK";
				}
			} else {
				$obj->result  = "NACK";
			}
		}
		
		return $obj;
	}
	
	public function soap_Access($data){	
		$obj = new stdClass();
		$obj->access_code = "NACK";
		
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->result = "NACK";
				return $obj;
			}
		}*/
		$user = $this->User->findByMobile($data->mobile);
		
		if(!empty($user)){
			$invitation = $this->Invitation->find('first', array('conditions'=>array(
				'Invitation.organizer_id' => $user['User']['id'],
				'Invitation.guest_id' => $user['User']['id'],
				'Invitation.house_id' => $data->house_id,
				'OR'=>array(
					array('Invitation.relation_id' => 1),
					array('Invitation.relation_id'=>2)
				),
				'status' => 1
			)));
			
			$obj->access_code = $invitation['Invitation']['aes']?$invitation['Invitation']['aes']:"NULL";
		}
		
		return $obj;
	}
	
	public function soap_EditProfile($data){
		$obj = new stdClass();
		$obj->result = "NACK";
		
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->result = "NACK";
				return $obj;
			}
		}*/
		$user = $this->User->findByMobile($data->mobile);
		if(!empty($user)){
			$this->User->id = $user['User']['id'];
			$this->User->read(null, $this->User->id);
			
			$this->User->saveField('name',$data->name);
			$this->User->saveField('father_surname',$data->last_name);
			$this->User->saveField('email',$data->email);
			
			$obj->result = "ACK";
		}
		
		return $obj;
	}
	
	public function soap_RegisterDevice($data){
		$obj = new stdClass();
		$obj->result = "NACK";
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->result = "NACK";
				return $obj;
			}
		}*/
		$user = $this->User->findByMobile($data->mobile);
		if(!empty($user)){
			
			$this->User->read(null, $user['User']['id']);
			
			$this->User->saveField('OS',$data->os);
			$this->User->saveField('full_device',$data->full_device);
			
			$obj->result = "ACK";
		}
		
		return $obj;
	}
	
	
	public function soap_Logs($data){	
		$obj = new stdClass();
		$obj->logs = 'NACK';
		
		/*if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->result = "NACK";
				return $obj;
			}
		}*/
		$user = $this->User->findByMobile($data->mobile);
		if(!empty($user)){
			
			$houses = $this->HousesUser->find('all', array(
				'conditions'=>array(
					'HousesUser.user_id'=>$user['User']['id'],
					'HousesUser.active' => 1
				)
			));
			if(!empty($houses)){
				$houses_list = $students_list = array();
				foreach($houses as $key=>$house){
					if(!is_null($house['HousesUser']['house_id'])){
						$houses_list[] = $house["HousesUser"]["house_id"];	
					} else if (!is_null($house['HousesUser']['student_id'])){
						$students_list[] = $house['HousesUser']['student_id'];
					}
				}
				$or = array(
					'Invitation.house_id' => $houses_list,
					'Log.house_id' => $houses_list,
					'Invitation.student_id' => $students_list,
					'Log.student_id' => $students_list
				);
				if(empty($houses_list)){
					unset($or['Invitation.house_id']); 
					unset($or['Log.house_id']); 
				}
				if(empty($students_list)){
					unset($or['Invitation.student_id']); 
					unset($or['Log.student_id']); 
				}
				//pr($or);exit;
				$logs = $this->Invitation->Log->find('all', array(
					'conditions'=>array(
						'OR'=> $or,
						'Log.created >=' => date('Y-m-d 00:00:00', strtotime('-2 days')),
						'Log.sent_mobile' => 0
					)
				));
				//Nombre invitado, telefono invitado, datetime log, categoria de invitacion, name organizador, mobile organizador, tipo logs (entrada, salida, alerta)
				foreach($logs as $key=>$log){
					
					if(!is_null($log['Log']['invitation_id'])){
						//Acceso manual: invitacion
						$house_id = !is_null($log['Invitation']['house_id'])? $log['Invitation']['house_id']:$log['Invitation']['student_id'];
						if(!is_null($log['Invitation']['guest_id'])){
							$guest = $this->User->read(null,$log['Invitation']['guest_id']);
							$guest_name = $guest['User']['name'].' '.$guest['User']['father_surname'];
							$guest_mobile = $guest['User']['mobile'];
						} else {
							$guest = $this->Contact->read(null,$log['Invitation']['contact_id']);
							$guest_name = $guest['Contact']['name'];
							$guest_mobile = is_null($guest['Contact']['mobile']) ? "no informado":$guest['Contact']['mobile'];
						}
						
						$organizer = $this->User->read(null,$log['Invitation']['organizer_id']);
						$relation = $this->Relation->read(null,$log['Invitation']['relation_id']);
						$relation_name = $relation['Relation']['name'];
					} else {
						//manual guardia o alumno
						if(!is_null($log['Log']['house_id'])){
							$house_id = $log['Log']['house_id'];
							$guest_name = $log['Guard']['name'].' '.$log['Guard']['father_surname'];
							$guest_mobile = is_null($log['Guard']['mobile'])? "no informado":$log['Guard']['mobile'];
							$relation_name = $log['Service']['glosa'];
							$organizer = $this->User->read(null, $log['Log']['guard_id']);
						} else {
							$house_id = $log['Log']['student_id'];
							$guest_name = $log['Student']['full_name'];
							$guest_mobile = is_null($log['Student']['mobile']) ? "no informado":$log['Student']['mobile'];
							$relation_name = 'Alumno(a)';
							$organizer = $this->User->read(null, $user['User']['id']); 
						}
					}
					$log_tmp['logs'][$key] = array(
						'id'=>$log['Log']['id'],
						'house_id'=>$house_id,
						'guest_name' => $guest_name,
						'guest_mobile' => $guest_mobile,
						'datetime' => $log['Log']['created'],
						'relation' => $relation_name,
						'organizer_name' => $organizer['User']['name'].' '.$organizer['User']['father_surname'],
						'organizer_mobile' => $organizer['User']['mobile'],
						'type' => $log['Log']['type']
					);
					//$this->Invitation->Log->id = $log['Log']['id'];
					//$this->Invitation->Log->saveField('sent_mobile', 1);
				}
				$obj->logs = json_encode($log_tmp);
			}
		}
		
		return $obj;
	}
	public function soap_Barriers($data){
		$obj = new stdClass();
		$obj->out = "NACK";
		$obj->msg = "OK";
		$condo = $this->Condo->findById($data->condo_id);
		
		if(isset($data->lat) AND isset($data->lng)){
			if($data->lat AND $data->lng){
				$distancia = $this->distanceCalculation($condo['Condo']['lat'],$condo['Condo']['long'], $data->lat, $data->lng);
				if($distancia <= 0.9){
					
					$data_barriers = array(
						'aes' => $data->aes,
						'condo_id' => $data->condo_id,
						'type' => $data->tipo
					);
					$this->Barrier->create();
					if($this->Barrier->save($data_barriers)){
						$obj->out = "ACK";
					} 
				}else{
					$obj->msg = "No es posible realizar la apertura de barreras desde tu ubicación actual (a ".$distancia."km de la barrera)";
				}
				
			}else{
				$obj->msg = "Activa tu GPS para usar el control remoto";
			}
		}else{
			$data_barriers = array(
				'aes' => $data->aes,
				'condo_id' => $data->condo_id,
				'type' => $data->tipo
			);
			$this->Barrier->create();
			if($this->Barrier->save($data_barriers)){
				$obj->out = "ACK";
			}
		}
		
		return $obj;
	}
	public function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $decimals = 2) {
		// Cálculo de la distancia en grados
		$degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
		$distance = $degrees * 111.13384; // 1 grado = 111.13384 km, basándose en el diametro promedio de la Tierra (12.735 km)
		return round($distance, $decimals);
	}
	
	public function sendNotification($invitation_id, $type = 'create'){
		$this->Invitation->id = $invitation_id;
		$invitation = $this->Invitation->read(null, $this->id);
		
		switch($type){
			case 'create':
				$message = $invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname'].' te ha invitado.';
				$OS = $invitation['Guest']['OS'];
				$registrationID = $invitation['Guest']['full_device'];
				$type = 'INV';
				break;
			case 'request':
				$message = $invitation['Guest']['name'].' '.$invitation['Guest']['father_surname'].' te ha solicitado una invitación.';
				$OS = $invitation['Organizer']['OS'];
				$registrationID = $invitation['Organizer']['full_device'];
				$type = 'REQ';
				break;
			case 'accept':
				$message = $invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname'].' ha aceptado tu solicitud.';
				$OS = $invitation['Guest']['OS'];
				$registrationID = $invitation['Guest']['full_device'];
				$type = 'ACC';
				break;
		}
		$flag = false;
		// echo $invitation['Guest']['OS'];exit;
		switch($OS){
			case 'AND':
				// API access key from Google API's Console
				define( 'API_ACCESS_KEY', 'AIzaSyCuTFWabl9ExkEWKrd4oYDB6j3s_8JUVo4' );
				$registrationIds = array($registrationID);
				 
				// prep the bundle
				$msg = array(
					'title'			=> 'Safecard',
					'message' 		=> $message,
					'subtitle'		=> 'Revisa tu listado de invitaciones.',
					'tickerText'	=> $message,
					'type'			=> $type
				);
				 
				$fields = array(
					'registration_ids' 	=> $registrationIds,
					'data'				=> $msg
				);
				 
				$headers = array(
					'Authorization: key=' . API_ACCESS_KEY,
					'Content-Type: application/json'
				);
				
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$result = curl_exec($ch );
				
				curl_close( $ch );
				$flag = true;
				break;
				
			case 'IOS':
				$deviceToken = $registrationID;
				// Put your private key's passphrase here:
				$passphrase = Configure::read('APNS.pass');//'Intelli.,2014';
				////////////////////////////////////////////////////////////////////////////////
				$ctx = stream_context_create();
				stream_context_set_option($ctx, 'ssl', 'local_cert', Configure::read('PathAppController').'ck.pem');
				stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
				
				// Open a connection to the APNS server
				$fp = stream_socket_client(
					Configure::read('APNS.url'), $err,
					$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); 
				// Create the payload body
				$body['aps'] = array(
					'alert' => array(
								"action-loc-key" => "para abrir Safecard",
								"body" => $message
							),
					'sound' => 'default',
					'badge' => 1
				);
				// Encode the payload as JSON
				$payload = json_encode($body);
				// Build the binary notification
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
				// Send it to the server
				$result = fwrite($fp, $msg, strlen($msg));
				
				// Close the connection to the server
				fclose($fp);
				
				$flag = true;
				break;
			case 'WPH':
			
				break;
		}
		return $flag;
	}
} 
?>
