<?php  
App::uses('AppController', 'Controller');

// API access key from Google API's Console
// define( 'API_ACCESS_KEY', 'AIzaSyCuTFWabl9ExkEWKrd4oYDB6j3s_8JUVo4' );

class WsSchoolsController extends AppController{ 
	public $uses = array('Student','User','Invitation');
	 
	public $components = array('Soap'); 
	
	function beforeFilter() {
		$this->Auth->allow();
		$this->Soap->__settings = array( 
            'wsdl' => "WSSchools", 
            'wsdlAction' => 'wsdl', 
            'prefix' => 'soap', 
            'action' => array('service'), 
        );  
		parent::beforeFilter();
	}
	
	public function soap_wsdl(){ 
		//will be handled by SoapComponent 
	} 

	public function soap_service(){ 
		//will be handled by SoapComponent 
	} 
	 
	/*
	* Registros cargados de froma manual son enviados a portico para validación de manera local
	*/
	public function soap_Sync($data){ 
		$obj = new stdClass(); 
		$obj->out = 'NACK';
		
		$data_user = array(
			'name' 				=> $data->name,
			'father_surname' 	=> $data->father_surname,
			'mother_surname' 	=> $data->mother_surname,
			'mobile' 			=> $data->mobile,
			'password' 			=> 1234,
			'role_id'			=> 7
		);
		
		$school_user = $this->User->SchoolsUser->find('first', array('conditions'=>array('school_user_id'=>$data->IDS,'school_id'=>$data->school_id)));
		
		$this->User->id = empty($school_user) ? 0 : $school_user['SchoolsUser']['user_id'];
		
		if(!$this->User->exists()){
			$user_tmp = $this->User->findByMobile($data->mobile);
			if(empty($user_tmp)){
				$this->User->create();
				$this->User->save($data_user);				
				$user_id = $this->User->id;
			} else {
				$obj->out = 'NACK';
				return $obj;
			}
		} else {
			$this->User->saveField('mobile', $data->mobile);
			$user_id = $this->User->id;
		}
		
		if(empty($school_user)){
			$this->User->SchoolsUser->create();
			
			$this->User->SchoolsUser->save(
				array(
					'school_user_id' 	=> $data->IDS,
					'user_id' 			=> $user_id,
					'school_id' 		=> $data->school_id
				)
			);
		}
		
		//Estudiantes
		$childs = json_decode($data->childs, true);
		
		if(array_key_exists('Childs', $childs) && !empty($childs['Childs'])){
			foreach($childs['Childs'] as $key=>$child){
				
				$student = $this->Student->find('first', array(
					'conditions'=>array(
						'stdschool_id' 	=> $child['stdschool_id'],
						'school_id'		=> $data->school_id
					)
				));
				
				if(empty($student)){
					$this->Student->create();				
					$data_student = array(
						'stdschool_id' 		=> 	$child['stdschool_id'],
						'school_id' 		=> 	$data->school_id,
						'full_name' 		=> 	$child['full_name'],
						'student_mobile'	=> 	$child['mobile'],
						'status'			=>	1
					);
					$this->Student->save($data_student);
					$student_id = $this->Student->id;
				} else {
					$student_id = $student['Student']['id'];//$child['stdschool_id'];
				}
				
				$data_house = $this->User->HousesUser->find('first', array('conditions'=>array('user_id'=>$user_id,'student_id'=>$student_id)));
				if(empty($data_house)){
				
					$data_houses_user = array(
						'user_id'		=> $user_id,
						'student_id' 	=> $student_id,
						'owner'			=> 1,
						'active'		=> 1
					);
					$this->User->HousesUser->create();
					$this->User->HousesUser->save($data_houses_user);
					
					$this->User->Event->create();
						
					$data_event = array(
						'user_id' => $user_id,
						'house_id'=> NULL,
						'student_id'=> $student_id,
						'from'=> date('Y-m-d H:i:s'),
						'to' => '2038-01-19 00:00:00'
					);
					if(!$this->User->Event->save($data_event)){
						pr($data_event);
						debug($this->User->Event->validationErrors);
					}
					
					$this->Invitation->create();
					$data_invitation = array(
						'event_id' => $this->User->Event->id,
						'organizer_id' => $user_id,
						'guest_id' => $user_id,
						'relation_id' => 1,
						'condo_id' => $data->school_id,
						'house_id'=> NULL,
						'student_id'=> $student_id,
						'mobile' => $data->mobile
					);
					
					$this->Invitation->save($data_invitation);
					$this->Invitation->saveCode($this->Invitation->id);	
				}
				
				$obj->out = 'ACK';
			}
		}
		
		return $obj;
	}
	
	public function soap_Invitations($data){
		$obj = new stdClass();
		
		$obj->invitations = array();
		// $obj->invitations = "NACK";
		
		$invitations = $this->Invitation->find('all', array('conditions'=>array('Invitation.condo_id'=>$data->school_id)));
		$data_invitation = array();
		
		if(!empty($invitations)){
		
			foreach($invitations as $key=>$invitation){
				$data_invitation['Invitation'][$key] = array(
					'id'					=>	$invitation['Invitation']['id'],
					'stdschool_id'			=>	$invitation['Student']['stdschool_id'],
					'school_id'				=>	$invitation['Invitation']['condo_id'],
					'accessed'				=>	$invitation['Invitation']['accessed'],
					'parent_mobile'			=>	$invitation['Organizer']['mobile'],
					'responsible_mobile'	=>	$invitation['Guest']['mobile'],
					'responsible_name'		=>	$invitation['Guest']['name'].' '.$invitation['Guest']['father_surname'],
					'student_mobile'		=>	$invitation['Student']['student_mobile'],
					'relation_id'			=>	$invitation['Invitation']['relation_id'],
					'pin'					=>	$invitation['Invitation']['pin'],
					'code'					=>	$invitation['Invitation']['code'],
					'aes'					=>	$invitation['Invitation']['aes']
				);
			}
		}
		
		$obj->invitations = json_encode($data_invitation);
		
		return $obj;
	}

	public function soap_ParentMobile($data){
		$obj = new stdClass();
		$obj->result = 'NACK';

		if($data->version != ''){
			$aes = new AES($data->version, Configure::read('Security.versionWS'), 128);
			$decrypt = $aes->decrypt();
			if($decrypt != Configure::read('Security.seedVersionWS').date('YmdH')){
				$obj->result = "NACK";
				return $obj;
			}
		} 
		
		$user = $this->User->findByMobile($data->old_mobile);
		if(!empty($user)){

			$school_user = $this->User->CondosUser->find('first', array(
				'conditions'=>array(
					'CondosUser.condo_id'=>$data->school_id, 
					'CondosUser.user_id'=>$user['User']['id']
				)
			));

			$houses_user = $this->User->HousesUser->find('all', array(
				'conditions' => array(
					'HousesUser.user_id' => $user['User']['id'],
					'HousesUser.student_id <>' => NULL,
					'HousesUser.active' => 1
				)
			));

			if(!empty($school_user)){
				//$this->User->id = $user['User']['id'];

				$user = $this->User->read(null, $user['User']['id']);
				$old_user_id = $user['User']['id'];
				if($this->User->field('mobile') != $data->new_mobile){

					$user_new = $this->User->findByMobile($data->new_mobile);
					if(empty($user_new)){
						//Si mobile nuevo no existe entonces lo creo con los datos del mobile antiguo
						$this->User->create();
						$data_user = array(
							'name' => $user['User']['name'],
							'father_surname' => $user['User']['father_surname'],
							'mother_surname' => $user['User']['mother_surname'],
							'mobile' => $data->new_mobile,
							'role_id' => 7,
							'email' => $user['User']['email'],
							'country_id' => $user['User']['country_id'],
							'last_login' => $user['User']['last_login'],
							'password' => '1234'
						);
						$user_id = '';
						if($this->User->save($data_user)){
							$this->User->UsersRole->create();
							$user_id = $this->User->id;
							$this->User->UsersRole->save(
								array(
									'user_id' => $user_id,
									'role_id' => 7
								)
							);
						}
					} else {
						$user_id = '';
						//Si mobile existe entonces recupero sus mismos datos y no genero nuevo usuario
						$this->User->id = $user_new['User']['id'];
						$user = $this->User->read(null,$this->User->id);
						$user_id = $this->User->id;
					}	

					if($user_id != ''){
						if(!empty($houses_user)){
							foreach($houses_user as $key=>$house_user){

								//Antes de crear un nuevo permiso verifico si existia previamente.
								$hu = $this->User->HousesUser->find('first', array('conditions'=>array(
									'HousesUser.user_id' => $user_id,
									'HousesUser.student_id' => $house_user['HousesUser']['student_id']
								)));

								if(!empty($hu)){
									$this->User->HousesUser->updateAll(
										array( 'HousesUser.active' => 1 ),
										array( 
											'HousesUser.student_id' => $house_user['HousesUser']['student_id'],
											'HousesUser.user_id' => $user_id
										)
									);

									$this->User->Invitation->updateAll(
										array('Invitation.status' => '1'),
										array(
											'Invitation.organizer_id'	=>$user_id,
											'Invitation.student_id'		=>$house_user['HousesUser']['student_id']
										)
									);
								}

								//Niego acceso a ID de usuario antiguo
								$this->User->HousesUser->updateAll(
									array( 'HousesUser.active' => 0 ),
									array( 
										'HousesUser.student_id' => $house_user['HousesUser']['student_id'],
										'HousesUser.user_id' => $old_user_id
									)
								);

								$this->User->Invitation->updateAll(
									array('Invitation.status' => '-1'),
									array(
										'Invitation.organizer_id'	=>$old_user_id,
										'Invitation.student_id'		=>$house_user['HousesUser']['student_id']
									)
								);

								//Actualizo ID referencia de usuario safecard con ID de usuario colegio
								$this->User->SchoolsUser->updateAll(
									array('user_id' => $user_id),
									array(
										'school_user_id' => $data->parent_id,
										'school_id' => $data->school_id
									)
								);

								if(empty($hu)){
									//Otorgo acceso a nuevo ID de usuario
									$this->User->HousesUser->create();
									$data_houses_user['HousesUser'] = array(
										'user_id' => $user_id, 
										'house_id' => NULL,
										'student_id'=> $house_user['HousesUser']['student_id'],
										'owner'=>1,
										'active'=>1
									);
									$this->User->HousesUser->save($data_houses_user);
									
									//Creación de invitación para propietario
									$this->User->Event->create();
									
									$data_event = array(
										'user_id' => $user_id,
										'house_id'=> NULL,
										'student_id'=> $house_user['HousesUser']['student_id'],
										'from'=> date('Y-m-d H:i:s'),
										'to' => '2038-01-19 00:00:00'
									);
									if(!$this->User->Event->save($data_event)){
										pr($data_event);
										debug($this->User->Event->validationErrors);
									}
									
									$this->User->Invitation->create();
									$data_invitation = array(
										'event_id' => $this->User->Event->id,
										'organizer_id' => $user_id,
										'guest_id' => $user_id,
										'relation_id' => 1,
										'condo_id' => $data->school_id,
										'house_id'=> NULL,
										'student_id'=> $house_user['HousesUser']['student_id'],
										'mobile' => $data->new_mobile
									);
									
									$this->User->Invitation->save($data_invitation);
									$this->User->Invitation->saveCode($this->User->Invitation->id);
								}
							}
							$obj->result = "ACK";
						}
					}
				}
			}	
		}

		return $obj;
	}
} 
?>