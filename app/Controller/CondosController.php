<?php
App::uses('AppController', 'Controller');
/**
 * Condos Controller
 *
 * @property Condo $Condo
 */
class CondosController extends AppController {

	public $helpers = array('GoogleMap'); 
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Condo->recursive = 0;
		
		$filtro = array();
		if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){
			$condos_id = ClassRegistry::init('CondosUser')->getCondominios();
			$filtro = array('Condo.id'=>$condos_id);
		}
		
		$this->set('condos', $this->paginate($filtro));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Condo->exists($id)) {
			throw new NotFoundException(__('Recinto Inválido'));
		}
		
		if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){
			$condos_id = ClassRegistry::init('CondosUser')->getCondominios();
			if(!in_array($id,$condos_id)){
				throw new NotFoundException(__('Condominio Inválido'));
			}
		}
		
		$options = array('conditions' => array('Condo.' . $this->Condo->primaryKey => $id));
		$this->set('condo', $this->Condo->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Condo->create();
			if ($this->Condo->save($this->request->data)) {
				$this->Condo->Comuna->id = $this->Condo->field('comuna_id');
				$comuna = $this->Condo->Comuna->read(null, $this->Condo->Comuna->id);
				$Address = urlencode($this->Condo->field('address').', '.$comuna['Comuna']['glosa']);
				$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$Address."&sensor=true";
				$xml = simplexml_load_file($request_url) or die("url not loading");
				$status = $xml->status;
				if ($status=="OK") {
					$Lat = $xml->result->geometry->location->lat;
					$Lon = $xml->result->geometry->location->lng;
					$LatLng = "$Lat,$Lon";

					$this->Condo->saveField('lat', $Lat);
					$this->Condo->saveField('long', $Lon);
				}
				
				foreach($this->request->data['CondosRelation'] as $relation_id=>$t){
					$data[] = array(
						'condo_id' => $this->Condo->id,
						'relation_id' => $relation_id,
						'alert_time' =>  $t['alert_time']
					);
				}
				$this->Condo->CondosRelation->deleteAll(array('condo_id'=>$this->Condo->id));
				$this->Condo->CondosRelation->saveMany($data);
				
				$this->Session->setFlash(__('Recinto registrado exitosamente'),'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Recinto no registrado. Inténtelo nuevamente.'));
			}
		}
		$comunas = $this->Condo->Comuna->find('list', array('order'=>'glosa ASC'));
		$this->loadModel('Relation');
		$condos_relations = $this->Relation->find('all');
		$this->set(compact('comunas','condos_relations'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Condo->exists($id)) {
				throw new NotFoundException(__('Recinto Inválido'));
		}
		
		if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){
			$condos_id = ClassRegistry::init('CondosUser')->getCondominios();
			if(!in_array($id,$condos_id)){
				throw new NotFoundException(__('Recinto Inválido'));
			}
		}
		
		if ($this->request->is('post') || $this->request->is('put')) {
			// pr($this->request->data);exit;
			if ($this->Condo->save($this->request->data)) {
				
				$this->Condo->Comuna->id = $this->Condo->field('comuna_id');
				$comuna = $this->Condo->Comuna->read(null, $this->Condo->Comuna->id);
				$Address = urlencode($this->Condo->field('address').', '.$comuna['Comuna']['glosa']);
				$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$Address."&sensor=true";
				$xml = simplexml_load_file($request_url) or die("url not loading");
				$status = $xml->status;
				if ($status=="OK") {
						$Lat = $xml->result->geometry->location->lat;
						$Lon = $xml->result->geometry->location->lng;
						$LatLng = "$Lat,$Lon";

						$this->Condo->saveField('lat', $Lat);
						$this->Condo->saveField('long', $Lon);
				}
				
				foreach($this->request->data['CondosRelation'] as $relation_id=>$t){
					$data[] = array(
						'condo_id' => $this->Condo->id,
						'relation_id' => $relation_id,
						'alert_time' =>  $t['alert_time']
					);
				}
				$this->Condo->CondosRelation->deleteAll(array('condo_id'=>$this->Condo->id));
				$this->Condo->CondosRelation->saveMany($data);
				
				$this->Session->setFlash(__('Recinto editado exitosamente'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El recinto no pudo ser guardado. Inténtalo nuevamente.'));
			}
		} else {
			$options = array('conditions' => array('Condo.' . $this->Condo->primaryKey => $id));
			$this->request->data = $this->Condo->find('first', $options);
		}
		$comunas = $this->Condo->Comuna->find('list', array('order'=>'glosa ASC'));
		$this->loadModel('Relation');
		$condos_relations = $this->Relation->find('all');
		$this->set(compact('comunas','condos_relations'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Condo->id = $id;
		if (!$this->Condo->exists()) {
			throw new NotFoundException(__('Recinto Inválido'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Condo->delete()) {
			$this->Session->setFlash(__('Recinto eliminado'), 'flash_success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Recinto no pudo ser eliminado. Intentalo nuevamente'), 'flash_error');
		$this->redirect(array('action' => 'index'));
	}
}
