<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'AES');
/**
 * Events Controller
 *
 * @property Event $Event
 */
class EventsController extends AppController {

	/**
	 * admin_index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Event->recursive = 0;
		
		$user = $this->Event->User->findById($this->Session->read('Auth.User.id'));
		$house_ids = array();
		foreach($user['House'] as $house){
			$house_ids[] = $house['id'];
		}
		
		$this->set('events', $this->paginate(array('house_id'=>$house_ids,'private'=>0)));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Evento Invalido'));
		}
		$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
		$this->set('event', $this->Event->find('first', $options));
		
		$this->set('invitations',$this->Event->Invitation->find('all', array('conditions'=>array('event_id'=>$id))));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Event->create();
			
			$this->request->data['Event']['user_id'] = $this->Session->read('Auth.User.id');
			/* if($this->request->data['Event']['per_day']){
				$this->request->data['Event']['hfrom'] = '00:00:00';
				$this->request->data['Event']['hto'] = '23:59:59';
			} */
			// pr($this->request->data);exit;
			if ($this->Event->save($this->request->data)) {
				
				if($this->request->data['Event']['per_day']){
					$j = 0;
					foreach($this->request->data['DaysEvent']['day_id'] as $key=>$day){
						$this->Event->DaysEvent->Day->id = $day;
						$this->Event->DaysEvent->Day->read(null, $this->Event->DaysEvent->Day->id);
						
						$data[$j]['DaysEvent']['event_id'] = $this->Event->id;
						$data[$j]['DaysEvent']['day_id'] = $day;
						$data[$j]['DaysEvent']['code'] = $this->Event->DaysEvent->Day->field('code');
						$data[$j]['DaysEvent']['hfrom'] = $this->request->data['DaysEvent']['hfrom'];
						$data[$j]['DaysEvent']['hto'] = $this->request->data['DaysEvent']['hto'];
						$j++;
					}
					
					/*$this->Event->saveField('from', date('Y-m-d H:i:s', strtotime($this->request->data['Event']['dfrom'].' 00:00:00'))); 
					$this->Event->saveField('to',date('Y-m-d H:i:s', strtotime($this->request->data['Event']['dto'].' 23:59:59')));*/
					
					if($this->Event->DaysEvent->saveMany($data)){
						$this->Session->setFlash(__('Evento creado exitosamente. Ahora realiza tus invitaciones.'),'flash_success');
						$this->redirect(array('action' => 'invite', $this->Event->id));
					} else {
						$this->Event->deleteAll(array('Event.id'=>$this->Event->id));
						$this->Session->setFlash(__('Ocurrió un error al intentar guardar. Inténtelo nuevamente'),'flash_error');
					}
				} else {
					$this->Session->setFlash(__('Evento creado exitosamente. Ahora realiza tus invitaciones.'),'flash_success');
					$this->redirect(array('action' => 'invite', $this->Event->id));
				}
			} else {
				debug($this->Event->invalidFields());
				$this->Session->setFlash(__('El evento no pudo ser creado. Revise su formulario e intentalo nuevamente'),'flash_error');
			}
		}
		
		$this->Event->House->bindModel(array('hasOne' => array('HousesUser')), false);
		$houses_user = $this->Event->House->find('all',array('conditions'=>array('HousesUser.user_id' => $this->Session->read('Auth.User.id'))));
		
		$houses = array();
		foreach($houses_user as $house){
			$houses[$house['House']['id']] = $house['Condo']['name'].' - '.$house['House']['name'];
		}
		
		$days = $this->Event->Day->find('list');
		
		$this->set(compact('houses','days'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Invalid event'));
		}
		
		$event = $this->Event->read(null, $id);
		if($event['Event']['user_id'] != $this->Session->read('Auth.User.id')){
			$this->Session->setFlash(__('Este evento fue creado por uno de sus contactos y no tiene permiso para realizar modificaciones o invitaciones.'), 'flash_warning');
			$this->redirect($this->referer());
		} 
		
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash(__('Evento editado exitosamente'),'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Evento no pudo ser editar. Intentelo nuevamente'),'flash_error');
			}
		} else {
			$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
			$this->request->data = $this->Event->find('first', $options);
		}
		
		$this->Event->House->bindModel(array('hasOne' => array('HousesUser')), false);
		$houses_user = $this->Event->House->find('all',array('conditions'=>array('HousesUser.user_id' => $this->Session->read('Auth.User.id'))));
		
		$houses = array();
		foreach($houses_user as $house){
			$houses[$house['House']['id']] = $house['Condo']['name'].' - '.$house['House']['name'];
		}
		$this->set(compact('houses'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Evento invalido'));
		}
		$this->request->onlyAllow('post', 'delete');
		
		$event = $this->Event->read(null, $id);
		if($event['Event']['user_id'] != $this->Session->read('Auth.User.id')){
			$this->Session->setFlash(__('Este evento fue creado por uno de sus contactos y no tiene permiso para realizar modificaciones o invitaciones.'), 'flash_warning');
			$this->redirect($this->referer());
		} 
		
		if ($this->Event->delete()) {
			$this->Session->setFlash(__('Evento eliminado'),'flash_success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Evento no pudo ser eliminado. Intentelo nuevamente'),'flash_error');
		$this->redirect(array('action' => 'index'));
	}
        
	public function invite($event_id = null){
		$this->Event->id = $event_id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Evento inválido'));
		}
		
		$event = $this->Event->read(null, $event_id);
		if($event['Event']['user_id'] != $this->Session->read('Auth.User.id')){
			$this->Session->setFlash(__('Este evento fue creado por uno de sus contactos y no tiene permiso para realizar modificaciones o invitaciones.'), 'flash_warning');
			$this->redirect($this->referer());
		} 
		
		if($this->request->is('post')){
			$i = 0;
			foreach($this->request->data['Event']['contact_id'] as $contact_id){
				$this->Event->User->Contact->id = $contact_id;
				$contact = $this->Event->User->Contact->read(null,$contact_id);
				$guest = $this->Event->User->find('first', array('conditions'=>array('mobile'=>$contact['Contact']['mobile'])));
				
				$data[$i]['Invitation']['contact_id'] = $contact_id;
				$data[$i]['Invitation']['organizer_id'] = $this->Session->read('Auth.User.id');
				
				$data[$i]['Invitation']['guest_id'] = empty($guest)? NULL:$guest['User']['id'];
				$data[$i]['Invitation']['event_id'] = $event_id;
				$i++;
			}
			
			if($this->Event->Invitation->saveMany($data)){
				foreach($this->Event->Invitation->inserted_ids as $key=>$invitation_id){
					$this->Event->Invitation->id = $invitation_id;
					$this->Event->Invitation->read(null,$invitation_id);
					$code = $this->Event->Invitation->getCode($invitation_id);
					
					$condominio = $this->Event->House->getCondominioByHouse($this->Event->field('house_id'));
					
					$aes = new AES($code, Configure::read('Security.passQR'), 128);
					$enc = $aes->encrypt();
					
					$this->Event->Invitation->saveField('condo_id',$condominio['Condo']['id']);
					$this->Event->Invitation->saveField('house_id',$condominio['House']['id']);
					
					$this->Event->Invitation->saveField('aes',"QrX0".$enc."#");
					$this->Event->Invitation->saveField('code',$code);
				}
				$this->Session->setFlash(__('Invitación generada exitosamente. Tu contacto recibirá en su smartphone su código de invitación'), 'flash_success');
			} else {
				$this->Session->setFlash(__('Invitación no generada. Intentalo nuevamente'),'flash_error');
			}
			
		}
		
		$guests = $this->Event->Invitation->find('all',array('conditions'=>array('event_id'=>$event_id)));
		$contact_ids = array();
		foreach($guests as $guest){
			$contact_ids[] = $guest['Invitation']['contact_id'];
		}
		
		$contacts = $this->Event->User->Contact->find('list', array('conditions'=>array(
			'organizer_id'=>$this->Session->read('Auth.User.id'),
			'id NOT'=>$contact_ids,
		)));
		
		$this->set(compact('contacts','event'));
	}
}
