<?php
App::uses('AppController', 'Controller');
/**
 * Contacts Controller
 *
 * @property Contact $Contact
 */
class ContactsController extends AppController {

    // public $components = array('RequestHandler');
    
	public $paginate = array(
		'Contact'=>array(
			'limit' => 20,
			'order' => 'Contact.name ASC'
		)
	);
	
    function beforeFilter() {
		parent::beforeFilter();
    }
/**
 * index method
 *
 * @return void
 */
    public function index() {
		// echo $this->Contact->User->rutValidate('16.241.833-5')? "verdad":"NO";
		$this->Contact->recursive = 0;
		$this->set('contacts', $this->paginate('Contact',array('organizer_id'=>$this->Session->read('Auth.User.id'))));
		$this->set('user_contacts', $this->Contact->find('list', array('conditions'=>array('organizer_id'=>$this->Session->read('Auth.User.id')))));
		
		$this->set('relations',$this->Contact->Relation->find('list', array('order'=>'name ASC')));
    }

    public function add(){
    	$this->Session->setFlash(__("Agregar un nuevo contacto no lo incorpora a tu smartphone"),"flash_info");
    	if($this->request->is('post') || $this->request->is('put')){
    		$this->Contact->create();

    		$this->request->data['Contact']['organizer_id'] = $this->Session->read("Auth.User.id");
    		
    		if($this->Contact->save($this->request->data)){
    			$this->Session->setFlash(__("Contacto guardado."),'flash_success');
    			$this->redirect(array('action' => 'index'));
    		} else {
    			$this->Session->setFlash(__("Ocurrió un error. Inténtelo nuevamente"),"flash_error");
    		}
    	}

    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {
        if (!$this->Contact->exists($id)) {
                throw new NotFoundException(__('Invalid contact'));
        }

        $contacto = $this->Contact->find('first', array('conditions'=>array(
        	'Contact.id'=>$id,
        	'Contact.organizer_id'=>$this->Session->read('Auth.User.id')
        	)
        ));
		
        if(empty($contacto)){
        	$this->Session->setFlash(__('Contacto inválido'),'flash_error');
        	$this->redirect(array('action'=>'index'));
        }

		$options = array('conditions' => array('Contact.' . $this->Contact->primaryKey => $id));
		$contact = $this->Contact->find('first', $options);
        $this->set('contact', $contact);
		
		$user = $this->Contact->User->findByMobile($contact['Contact']['mobile']);
		$this->set('user',$user);
		
		if($this->request->is('post')){	
			
			$i = 0;
			if(!empty($this->request->data['Contact']['house_id'])){
				foreach($this->request->data['Contact']['house_id'] as $house_id){	
					$data_house[$i] = array(
						'house_id' => $house_id,
						'user_id' => $user['User']['id'],
						'owner' => 0,
						'active' => 1,
						'responsible_id' => $this->Session->read('Auth.User.id')
					);
					
					$data_event[$i] = array(
						'user_id' => $user['User']['id'],
						'house_id' => $house_id,
						'from'=> date('Y-m-d H:i:s'),
						'to' => '2038-01-19 00:00:00'
					);
					
					$i++;
				}
				
				$this->loadModel('Event');
				//Se eliminan todas las casas como residente
				$this->Contact->User->HousesUser->deleteAll(array('HousesUser.responsible_id'=>$this->Session->read('Auth.User.id'), 'HousesUser.user_id'=>$user['User']['id']), false);
				//Se eliminan eventos e invitaciones como residente
				$invitations = $this->Event->Invitation->find('all', array('conditions'=>array('Invitation.organizer_id'=>$user['User']['id'], 'Invitation.relation_id'=>2)));
				foreach($invitations as $key=>$inv){
					$this->Event->delete($inv['Invitation']['event_id'], true);
				}
				
				if($this->Contact->User->HousesUser->saveMany($data_house)){
					
					//Creación de invitación para propietario
					if($this->Event->saveMany($data_event)){
						foreach($this->Event->inserted_ids as $key=>$event_id){
							$event = $this->Event->read(null, $event_id);
							$data_invitation = array(
								'event_id' => $event_id,
								'organizer_id' => $user['User']['id'],
								'guest_id' => $user['User']['id'],
								'relation_id' => 2,
								'condo_id' => $event['House']['condo_id'],
								'house_id' => $event['House']['id'],
								'mobile' => $user['User']['mobile']
							);
							$this->Event->Invitation->create();
							$this->Event->Invitation->save($data_invitation);
							$this->Event->Invitation->saveCode($this->Event->Invitation->id);
						}
					}
					
					$this->Session->setFlash(__('Acceso(s) guardados exitosamente.'),'flash_success');
				} else {	
					$this->Session->setFlash(__('No se pudo registrar el acceso. Inténtalo nuevamente.'),'flash_error');
				}
			} else {
				$this->Contact->User->HousesUser->deleteAll(array('HousesUser.responsible_id'=>$this->Session->read('Auth.User.id'), 'HousesUser.user_id'=>$user['User']['id']), false);
				$this->Session->setFlash(__($contact['Contact']['name'].' ha quedado sin acceso a sus propiedades.'),'flash_warning');
			}
		}
		
		if(!empty($user)){
			$houses_user = $this->Contact->User->HousesUser->find('all', 
				array(
					'conditions'=>array(
						'HousesUser.user_id'=>$user['User']['id'],'responsible_id <>'=>'NULL'
					)
				)
			);
			$house_ids = array();
			foreach($houses_user as $houseUser){
				$house_ids[] = $houseUser['HousesUser']['house_id'];
			}
			$this->set('house_ids', $house_ids);
		} else {
			$this->Session->setFlash(__($contact['Contact']['name'].' no se encuentra registrado(a) en Safecard.'),'flash_warning');
		}
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {
        $this->Contact->id = $id;
        if (!$this->Contact->exists()) {
                throw new NotFoundException(__('Contacto inválido'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Contact->delete()) {
			$this->Session->setFlash(__('Contacto eliminado'),'flash_success');
			$this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Contacto no eliminado'),'flash_error');
        $this->redirect(array('action' => 'index'));
    }   

    public function invite(){

    	if($this->request->is('post') || $this->request->is('put')){
    		$mobile_guests = $name_guests = '';
    		if(!empty($this->request->data['Invitation']['contact_id'])){

				foreach($this->request->data['Invitation']['contact_id'] as $contact_id){
					$contacto = $this->Contact->read(null, $contact_id);
					$mobile_guests .= $contacto['Contact']['mobile'].',';
					$name_guests .= $contacto['Contact']['name'].',';
				}
				
				$mobile_guests = substr($mobile_guests,0,strlen($mobile_guests)-1);
				$name_guests = substr($name_guests,0,strlen($name_guests)-1);

				try{

					$opts = array( 'http'=>array( 'user_agent' => 'PHPSoapClient'));
					$context = stream_context_create($opts);
					$client = new SoapClient(Configure::read("Ws.url"),
											 array('stream_context' => $context,
												   'cache_wsdl' => WSDL_CACHE_NONE));
					
					$days_select = '';
					if($this->request->data['Event']['per_day']){
						foreach($this->request->data['DaysEvent']['day_id'] as $day_id){
							$days_select .= $day_id.',';
						}
					}
					$days_select = substr($days_select, 0, strlen($days_select)-1);
					
					$this->Contact->Invitation->House->id = $this->request->data['Event']['house_id'];

					$result = $client->soap_AddInvitations(array(
						'subject'=> $this->request->data['Event']['subject'],
						'start_date' => $this->request->data['Event']['dfrom'],
						'start_time' => $this->request->data['Event']['hfrom'],
						'end_date' => $this->request->data['Event']['dto'],
						'end_time' => $this->request->data['Event']['hto'],
						'house_id' => $this->request->data['Event']['house_id'],
						'condo_id' => $this->Contact->Invitation->House->field('condo_id'),
						'mobile_organizer' => $this->Session->read('Auth.User.mobile'),
						'mobile_guests' => $mobile_guests,
						'name_guests' => $name_guests,
						'relation_id' => $this->request->data['Event']['relation_id'],
						'repeat' => $this->request->data['Event']['per_day'],
						'days' => $days_select
					));
					
					if($result->result == 'ACK'){
						//echo json_encode(array('exito'=>1,'message'=>'Invitación generada exitosamente. Tu contacto recibirá en su smartphone su código de invitación'));
						$this->Session->setFlash(__("Invitacion(es) generadas exitosamente. Tu contacto recibirá en su smartphone su código de invitación o bien un SMS con un pin de acceso."),"flash_success");
					} else {
						$this->Session->setFlash(__("No fue posible crear la(s) invitacion(es). Inténtelo nuevamente. Revise que todos los datos estén completos"),"flash_error");
						//echo json_encode(array('exito'=>0,'message'=>''));
					}
				} catch(Exception $e) {+
					$this->Session->setFlash($e->getMessage(),'flash_error');
					//echo json_encode(array('exito'=>0,'message'=>$e->getMessage()));
				}

			} else {
				$this->Session->setFlash(__("Debes seleccionar al menos uno de tus contactos"),"flash_error");
			}
    	}

    	$user_contacts = $this->Contact->find("list", array('conditions'=>array('organizer_id'=>$this->Session->read("Auth.User.id"))));
    	
    	$this->loadModel('Relation');
    	$relations_invite = $this->Relation->find('list', array('conditions'=>array('id'=>array('3','4','5'))));
    	
    	$this->set(compact('user_contacts','relations_invite'));
    }

    public function admin_index(){
		$this->index();
    	$this->render('index');
    }

    public function admin_invite(){
    	$this->invite();
    	$this->render('invite');
    }

    public function admin_delete(){
    	$this->delete();
    }

    public function admin_add(){
    	$this->add();
    	$this->render("add");
    }
}
