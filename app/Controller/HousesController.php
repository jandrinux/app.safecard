<?php
App::uses('AppController', 'Controller');
/**
 * Houses Controller
 *
 * @property House $House
 */
class HousesController extends AppController {
    
    public function beforeFilter() {
		parent::beforeFilter();
	}

    public $paginate = array(
        /* 'User'=>array(
            'limit' => 20,
            'contain' => array('UsersRole','CondosUser'),
            'group' => array('User.id')
        ),
        'Contact'=>array(
            'limit' => 20, 
        ),*/
        'House'=>array(
            'limit'=>20,
            'order' => array('House.name' => 'ASC')
        )
    );
	
    public function index() {
        $this->House->recursive = 1;
        
        //Houses
        $this->House->bindModel(array('hasOne' => array('HousesUser')), false);
        $houses = $this->House->find('all', array('conditions'=>array('HousesUser.house_id NOT'=>NULL,'HousesUser.active'=>1, 'HousesUser.user_id' => $this->Session->read('Auth.User.id'))));
        
        $this->loadModel('Event');
        foreach($houses as $key=>$house){
            $houses[$key]['House']['total_eventos'] = $this->Event->totalEventosByHouse($house['House']['id']);
        }
        
        $this->set('houses', $houses);

        //Students
        //Students
        $this->loadModel('Student');
        $this->Student->bindModel(array('hasOne' => array('HousesUser')), false);
        $students = $this->Student->find('all',array('conditions'=>array('HousesUser.student_id NOT'=>NULL,'HousesUser.active'=>1,'HousesUser.user_id' => $this->Session->read('Auth.User.id'))));
        
        $this->loadModel('Event');
        foreach($students as $key=>$student){
            $students[$key]['Student']['total_eventos'] = 0;//$this->Event->totalEventosByHouse($house['House']['id']);
        }   

        $this->set('students', $students);
    }

/**
 * admin_index method
 *
 * @return void
 */
    public function admin_index() {
		
        $this->House->recursive = 1;
		$filtro = array();

        $condos = $this->House->Condo->find('list', array('conditions'=>array('id'=>$this->viewVars['condos_ids'],'type'=>0)));
        $houses = $condo = array();
        //pr($this->params->querycondo_id);
        if($this->request->is('post')){

            //Reinicio paginador en caso que uno de los filtros haya cambiado
            if(
                @($this->request->data['House']['condo_id']!=$this->Session->read('filter_condo_id')) //OR
            ){
                $this->request->params['named']['page'] = 1;
            }

            $filtro = array();
            
            $this->Session->write('filter_condo_id','');
            
            if(isset($this->request->data['House']['condo_id']) && $this->request->data['House']['condo_id'] != ''){
                $filtro[] = array(
                    'House.condo_id'=>$this->request->data['House']['condo_id'],
                );

                $this->Session->write('filter_condo_id', $this->request->data['House']['condo_id']);
                $condo = $this->House->Condo->read(null, $this->request->data['House']['condo_id']);
            } else {
                $filtro[] = array(
                    'House.condo_id' => $this->viewVars['condos_ids'],
                );
            }
        } else {
            if($this->Session->read('filter_condo_id')!=''){
                $filtro[] = array(
                    'House.condo_id'=>$this->Session->read('filter_condo_id')
                );
            } else {
                $filtro[] = array(
                    'House.condo_id'=>$this->viewVars['condos_ids']
                );
            }
        }    
        $houses = $this->paginate('House',$filtro);
        $this->set('houses', $houses);
        $this->set('condos', $condos);
    }

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_view($id = null) {
        if (!$this->House->exists($id)) {
            throw new NotFoundException(__('Vivienda Invalida'));
        }
		$house = $this->House->read(null,$id);
		if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){			
			$condos_id = ClassRegistry::init('CondosUser')->getCondominios();
			if(!in_array($house['House']['condo_id'],$condos_id)){
				throw new NotFoundException(__('Vivienda Invalida'));
			}
		}
		
        $options = array('conditions' => array('House.' . $this->House->primaryKey => $id));
        $this->set('house', $this->House->find('first', $options));
    }

/**
 * admin_add method
 *
 * @return void
 */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->House->create();
            if ($this->House->save($this->request->data)) {
                
                $this->Session->setFlash(__('Vivienda creada exitosamente'),'flash_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Vivienda no pudo ser guardada. Intentelo nuevamente'),'flash_error');
            }
        }
		
		$filtro = array();
		if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){			
			$condos_id = ClassRegistry::init('CondosUser')->getCondominios();
			$filtro = array('Condo.id'=>$condos_id);
		}
		
        $condos = $this->House->Condo->find('list', array('conditions'=>$filtro));
        $this->set(compact('condos'));
    }

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_edit($id = null) {
        if (!$this->House->exists($id)) {
            throw new NotFoundException(__('Invalid house'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->House->save($this->request->data)) {
                    $this->Session->setFlash(__('Vivienda editada exitosamente'), 'flash_success');
                    $this->redirect(array('action' => 'index'));
            } else {
                    $this->Session->setFlash(__('La vivienda no pudo ser editada, intentalo nuevamente'), 'flash_error');
            }
        } else {
            $options = array('conditions' => array('House.' . $this->House->primaryKey => $id));
            $this->request->data = $this->House->find('first', $options);
        }
		
		$filtro = array();
		if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){			
			$condos_id = ClassRegistry::init('CondosUser')->getCondominios();
			$filtro = array('Condo.id'=>$condos_id);
		}
		
        $condos = $this->House->Condo->find('list', array('conditions'=>$filtro));
        $this->set(compact('condos'));
    }

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_delete($id = null) {
        $this->House->id = $id;
        if (!$this->House->exists()) {
            throw new NotFoundException(__('Vivienda invalida'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->House->delete()) {
            $this->Session->setFlash(__('Vivienda eliminada exitosamente'),'flash_success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Vivienda no pudo ser eliminada'), 'flash_error');
        $this->redirect(array('action' => 'index'));
    }
    
    public function admin_getHouseByCondo($condo_id = null){
        $this->autoRender = false;
        $this->layout = false;
        if($this->request->is('ajax')){
            $houses = array();
            if($this->House->Condo->exists($condo_id)){
				$condo = $this->House->Condo->read(null, $condo_id);
				
				if(!$condo['Condo']['type']){
					$houses = $this->House->find('list', array(
                        'conditions'=>array(
                            'condo_id'=>$condo_id
                        ), 
                        'order'=>array('name' => 'ASC')
                    ));
				} else {
					$this->loadModel('Student');
                    $this->Student->recursive = 0;
					$houses = $this->Student->find('list', array(
                        'conditions'=>array(
                            'Student.school_id'=>$condo_id
                        ),
                        'order'=>array('Student.full_name' => 'ASC')
                    ));
				}
            }
            asort($houses);
            //$houses = array($houses);
            echo json_encode($houses);
        }
    }
}
