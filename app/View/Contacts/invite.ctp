<?php
    //echo $this->Html->script('jquery.Rut');
	$admin = false;
	if(in_array($this->Session->read("Auth.User.Role.code"),Configure::read("AppAdmin"))){
		$admin = true;
	}
    $this->Html->addCrumb('<i class="entypo-users"></i>Contactos', array('action'=>'index', 'admin'=>$admin), array('escape'=>false));
    $this->Html->addCrumb('Nueva invitación');
?>

<h2><?php echo __('Invitar'); ?></h2>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-body">
			<?php echo $this->Form->create('Event', array('class'=>'form-horizontal','role'=>'form')); ?>
			
			<div class="form-group">
			<label class="col-sm-3 control-label"><?php echo __('Asunto') ?></label>
			<?php echo $this->Form->input('subject', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
			</div>
			
			<div class="form-group">
			<label class="col-sm-3 control-label"><?php echo __('Casa-Depto') ?></label>
			<?php echo $this->Form->input('house_id', array('label'=>false, 'type'=>'select', 'options'=>array(''=>'Selecciona tu vivienda')+$user_houses,'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
			</div>
			
			<?php //echo $this->Form->input('Invitation.contact_id', array('type'=>'select','multiple'=>'multiple')); ?>
			<div class="form-group">
			<label class="col-sm-3 control-label"><?php echo __('Invitados') ?></label>
			<?php echo $this->Form->input('Invitation.contact_id', array('options'=>$user_contacts, 'label'=>false, 'class'=>'select2','multiple'=>'multiple','div'=>array('class'=>'col-sm-6'))); ?>
			</div>
				
			<div class="form-group">
			<label class="col-sm-3 control-label"><?php echo __('Desde') ?></label>
			<?php echo $this->Form->input('dfrom', array(
				'label'=>false,
				'type'=>'text', 
				'data-format'=>'dd-mm-yyyy',
				'data-start-date'=>'0d',
				'value'=>isset($this->request->data['Event']['dfrom']) ? date('d-m-Y', strtotime($this->request->data['Event']['dfrom'])):date('d-m-Y'),
				'class'=>'form-control datepicker', 
				'div'=>array('class'=>'col-sm-3'))); ?>
			
			<?php echo $this->Form->input('hfrom', array(
				'label'=>false,
				'data-template'=>'dropdown',
				'data-show-seconds'=>false,
				'data-show-meridian'=>false,
				'data-minute-step'=>15,
				'type'=>'text', 
				'class'=>'form-control timepicker', 
				'div'=>array('class'=>'col-sm-3'))); ?>
			
			</div>
			
			<div class="form-group">
			<label class="col-sm-3 control-label"><?php echo __('Hasta') ?></label>
			
			<?php echo $this->Form->input('dto', array(
				'label'=>false,
				'type'=>'text', 
				'data-format'=>'dd-mm-yyyy',
				'data-start-date'=>'0d',
				'value'=>isset($this->request->data['Event']['dto']) ? date('d-m-Y', strtotime($this->request->data['Event']['dto'])):date('d-m-Y'),
				'class'=>'form-control datepicker', 
				'div'=>array('class'=>'col-sm-3'))); ?>
			
			<?php echo $this->Form->input('hto', array(
				'label'=>false,
				'data-template'=>'dropdown',
				'data-show-seconds'=>false,
				'data-show-meridian'=>false,
				'type'=>'text', 
				'class'=>'form-control timepicker', 
				'div'=>array('class'=>'col-sm-3'))); ?>
			  
			</div>
			
			<div class="form-group">
			<label class="col-sm-3 control-label"><?php echo __('Tipo de invitado') ?></label>
			<?php echo $this->Form->input('relation_id', array('label'=>false, 'type'=>'select', 'options'=>array(''=>'Selecciona')+$relations_invite,'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
			</div>
			
			<div class="form-group">
			<label class="col-sm-3 control-label"><?=__('Repetir semanalmente')?></label>
			<?php echo $this->Form->input('per_day', array('type'=>'checkbox', 'label'=>false, 'div'=>array('class'=>'col-sm-6')));?>
			</div>
			
			<div class="form-group per_day">
			<label class="col-sm-3 control-label"><?=__('Días') ?></label>
			<?php echo $this->Form->input('DaysEvent.day_id', array('label'=>false, 'options'=>$days, 'class'=>'select2','multiple'=>'multiple','div'=>array('class'=>'col-sm-6'))); ?>
			</div>
			
			<div class="form-group">
			<label class="col-sm-3 control-label"></label>
			<?php 
			echo $this->Html->link(__('Cancelar'),  array('action'=>'index','admin'=>$admin), array('class'=>'btn btn-default', 'div'=>array('class'=>'form-group'))).'&nbsp;';
			echo $this->Form->button(__('Enviar invitaciones'), array('class'=>'btn btn-orange'));
			?>
			</div>
				
			<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>