<script type="text/javascript">
$(function(){
   
   /*$(".btn").on('click', function(event){
		var checked = $(".chk_contacts:checked").length
		if(checked == 0){
			alert("Debe seleccionar al menos uno de sus contactos");
			return false;
		} else {
			var contacts_id = new Array();
			$.each($(".chk_contacts:checked"), function(index, value){
				contacts_id.push($(this).val());
				// var opt = $(this).val();
				//$("#InvitationContactId option[value="+ opt +"]").attr("selected",true);
			})
			$("#InvitationContactId").select2("val",contacts_id);
			$('#invitacion-modal').modal('show');//,{backdrop: 'static'});
		}
		
		//$('#invitacion-modal').css('max-height', jQuery(window).height());
   })
   
   $("#EventIndexForm").on('submit', function(event){

		$("#InvitationContactId").attr('disabled', false);
		$.ajax({
			url: "<?php echo $this->Html->url(array('controller'=>'events','action'=>'add_ajax')); ?>",
			type: 'POST',
			data: $("#EventIndexForm").serializeArray(),
			dataType: 'JSON',
			success: function(data){
				alert(data.message);
				if(data.exito == 1){
					$("#invitacion-modal").modal("hide");
				}
			}
		});
		event.preventDefault();
   })*/
   
})
</script>
<h1 data-toggle="tooltip" data-placement="left" title="" data-original-title="Listado de contactos que has invitado al menos una vez"><?php echo __('Contactos'); ?></h1>

<!--<button class="btn btn-white" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Contactos a los cuales has invitado al menos una vez." data-original-title="Listado de contactos">I'm a Popover</button>-->

<nav class="navbar navbar-inverse" role="navigation">
	<?php echo $this->Html->link("Invitar", array('action'=>'invite'), array('class'=>'navbar-brand')); ?> 
	<?php echo $this->Html->link("Agregar", array('action'=>'add'), array('class'=>'navbar-brand')); ?>
</nav>
<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>

<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
    <thead>
        <tr>
			<!--<th>&nbsp;</th>-->
            <th><?php echo $this->Paginator->sort('User.name','Nombre contacto'); ?></th>
            <th><?php echo $this->Paginator->sort('mobile','Celular'); ?></th>
            <th class="actions"><?php echo __('Actividad'); ?></th>
        </tr>
    </thead>
<?php foreach ($contacts as $contact): ?>
<tr>
	<!--<td><?php echo $this->Form->checkbox('contact_id', array('class'=>'chk_contacts','value'=>$contact['Contact']['id'],'label'=>false,'type'=>'checkbox')); ?></td>-->
    <td>
        <?php echo $contact['Contact']['name'].' '.$contact['Contact']['father_surname']; ?>
    </td>
    <td><?php echo h($contact['Contact']['mobile']); ?>&nbsp;</td>
    <td class="actions">
        <div class="label label-success"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $contact['Contact']['id'])); ?></div>
        <div class="label label-warning" data-toggle="tooltip" data-placement="top" data-original-title="Esta acción no elimina este contacto de tu smartphone."><?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $contact['Contact']['id']), null, __('Seguro desea eliminar el contacto %s?', $contact['Contact']['name'])); ?></div>
    </td>
</tr>
<?php endforeach; ?>
</table>

<div class="paging">
<?php
    echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
?>
</div>