<?php
    $this->Html->addCrumb('Contactos', array('action'=>'index'));
    $this->Html->addCrumb('Detalle contacto');
?>
<div class="row">
	<div class="col-md-6">
		<h2><?php echo __('Detalle contacto'); ?></h2>
		<table class="table table-hover">
			<tr>
				<td><?php echo __('Contacto'); ?></td>
				<td>
						<?php echo $contact['Contact']['name'].' '.$contact['Contact']['father_surname'].' '.$contact['Contact']['mother_surname']; ?>
						&nbsp;
				</td>
			</tr>
			<tr>
				<td><?php echo __('Celular'); ?></td>
				<td>
						<?php echo h($contact['Contact']['mobile']); ?>
						&nbsp;
				</td>
			</tr>
			<tr>
				<td><?php echo __('Registrado'); ?></td>
				<td>
						<?php echo get_time_difference($contact['Contact']['created']); ?>
						&nbsp;
				</td>
			</tr>
		</table>
	</div>
	<?php if(!empty($user)):?>
	<div class="col-md-6">
	<h2>Dar acceso permanente</h2>
	<?php 
		echo $this->Form->create('Contact', array('class'=>'form-horizontal','role'=>'form'));
		echo "<div class='form-group'>";
		echo $this->Form->input('house_id', array('label'=>'Mis Propiedades', 'options'=>$user_houses,'selected'=>$house_ids,'class'=>'select2 form-control','multiple'=>'multiple','div'=>array('class'=>'col-md-6'))); 
		echo "</div>";
		echo $this->Form->button(__('Generar'), array('class'=>'btn btn-orange'));
		echo $this->Form->end(); 
	?>
	</div>
	<?php endif; ?>
</div>

