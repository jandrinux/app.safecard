<?php
    //echo $this->Html->script('jquery.Rut');
	$admin = false;
	if(in_array($this->Session->read("Auth.User.Role.code"),Configure::read("AppAdmin"))){
		$admin = true;
	}
    $this->Html->addCrumb('<i class="entypo-users"></i>Contactos', array('action'=>'index', 'admin'=>$admin), array('escape'=>false));
    $this->Html->addCrumb('Nueva invitación');
?>

<h2><?php echo __("Nuevo contacto");?></h2>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-body">
			
			<?php echo $this->Form->create("Contact", array('class'=>'form form-horizontal')); ?>
			<div class="form-group">
				<label class="col-sm-3 control-label"><?=__('Nombre') ?></label>
				<?php echo $this->Form->input("name", array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label"><?=__('Ap. Paterno') ?></label>
				<?php echo $this->Form->input("father_surname", array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label"><?=__('Ap. Materno') ?></label>
				<?php echo $this->Form->input("mother_surname", array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label"><?=__('Celular') ?></label>
				<?php echo $this->Form->input("mobile", array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label"></label>
			<?php 
			echo $this->Html->link(__('Cancelar'),  array('action'=>'index','admin'=>$admin), array('class'=>'btn btn-default', 'div'=>array('class'=>'form-group'))).'&nbsp;';
			echo $this->Form->button(__('Crear contacto'), array('class'=>'btn btn-orange'));
			?>
			</div>
				
			<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>