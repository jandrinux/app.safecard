<style>
.frm-block{
	margin-bottom:5px;
}
.frm-label{
	font-weight:bold;
	color:#555555;
}
</style>
<h1><?php echo __('Invitaciones'); ?></h1>



<div class="panel panel-primary">
    <div class="panel-body">

    <?php echo $this->Form->create('Invitation', array('class'=>'form-horizontal', 'role'=>'form')); ?>
    <div class="row frm-block">
	    <div class="col-sm-3 frm-label"><?=__('Numero') ?></div>
	    <div class="col-sm-9">
			<?php echo $this->Form->input('mobile', array(
			'label'=>false,
			'type'=>'text', 
			'class'=>'form-control'));  ?>
		</div>
	</div>

    <div class="row frm-block">
	    <div class="col-sm-3 frm-label"><?=__('Tipo') ?></div>
	    <div class="col-sm-9">
			<select name="data[Invitation][tipo_inv]"  class="form-control" <?php if(!isset($post)){echo 'disabled';}?>>
				<option value="">Todas</option>
				<option value="env" <?php echo (isset($tipo_inv) and $tipo_inv=='env') ? 'selected' : ''?>>Enviadas</option>
				<option value="rec" <?php echo (isset($tipo_inv) and $tipo_inv=='rec') ? 'selected' : ''?>>Recibidas</option>
			</select>
		</div>
	</div>
	<br>
	<div class="row">
   		<div class="col-sm-9 col-sm-offset-3">
   		<?php echo $this->Form->button(__('Buscar'),  array('class'=>'btn btn-orange'));?>
    	</div>
    </div>

    <?php echo $this->Form->end(); ?>

    </div>
</div>




<?php //echo pr($dias);?>



<p class="count" ><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>
<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
	<thead>
		<tr>
			<!--<th>ID</th>-->
			<?php if(isset($post) and isset($mobile)) : ?>
			<th>Tipo</th>
			<?php endif; ?>
			<th>De</th>
			<th>Para</th>
			<th>Repetitivo</th>
			<th>Fecha / Hora</th>
			
		</tr>
	</thead>
	<?php foreach($invitations as $inv) : ?>
	<tr>
		<!--<td><?php echo $inv['Invitation']['id'];?>&nbsp;</td>-->
		<?php if(isset($post) and isset($mobile)) : ?>
		<td><?php echo $inv['Organizer']['mobile']==$mobile ? 'Enviado' : 'Recibido'; ?>&nbsp;</td>
		<?php endif; ?>
		<td>
			<?php if($inv['Organizer']['id']) : ?>
				<?php echo $inv['Organizer']['name']; ?> 
				<?php echo $inv['Organizer']['father_surname']; ?>
				<br>
				<i style="font-size:10px;color:gray;"><?php echo $inv['Organizer']['mobile']; ?></i>&nbsp;
			<?php endif;?>


		</td>
		<td>
			<?php if($inv['Guest']['id']) : ?>
				<?php echo $inv['Guest']['name']; ?> 
				<?php echo $inv['Guest']['father_surname']; ?>
				<br>
				<i style="font-size:10px;color:gray;"><?php echo $inv['Guest']['mobile']; ?></i>&nbsp;
			<?php endif;?>
			<?php if(!$inv['Guest']['id']) : ?>
				<?php echo $inv['Invitation']['mobile']; ?> 
			<?php endif;?>
		</td>
		<td>
			<?php echo $inv['Invitation']['repeat']==1 ? 'SI' : 'NO' ?>&nbsp;
		</td>
		<td style="font-size:11px;">
			<?php echo $inv['Invitation']['repeat']==1 ? 'FECHA: '.$inv['Invitation']['repeat_from'].' - '.$inv['Invitation']['repeat_to'].'<br>HORA: '.substr($inv['Event']['from'],10,-3).' - '.substr($inv['Event']['to'],10,-3).'<br>DÍAS: '.$dias[$inv['Invitation']['event_id']] : 'DESDE: '.substr($inv['Event']['from'],0,-3).'<br>HASTA: '.substr($inv['Event']['to'],0,-3); ?>&nbsp;
		</td>
	</tr>
	<?php endforeach; ?>
</table>
<div class="paging">
<?php
	echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
	echo $this->Paginator->numbers(array('separator' => ''));
	echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
?>
</div>