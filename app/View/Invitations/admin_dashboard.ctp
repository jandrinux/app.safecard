<style>
.frm-block{
	margin-bottom:5px;
}
.frm-label{
	font-weight:bold;
	text-align:left;
	color:#555555;
}
</style>
<script>
// Area Chart
$(document).ready(function(){
	// Goals
	var day_data = [
		<?php foreach($dias as $dia): ?>
		{"elapsed": "<?php echo $dia['Day']['glosa']?>", "value": <?php echo $dia['Day']['total_visitas']; ?>},
		<?php endforeach; ?>
	];
	
	Morris.Line({
		element: 'week_access',
		data: day_data,
		xkey: 'elapsed',
		ykeys: ['value'],
		labels: ['Total visitas'],
		parseTime: false,
		lineColors: ['#7595ac']
	});
	
	// Donut PROPIETARIOS
	var paramsPROP = {
	    element: 'details24hoursPROP',
	    data: [],
	    colors: ['#f26c4f', '#00a651', '#00bff3', '#0072bc', '#cccccc', '#e70c22', '#1c2b49']
	};

	data_prop = <?php 
		$total_visitas_diarias = 0;
		foreach($relations_details as $rd): 
			$total_visitas_diarias += $rd['Relation']['total_visitas']; 
	?>
			paramsPROP.data.push( {"label": "<?php echo $rd['Relation']['glosa']?>", "value": <?php echo $rd['Relation']['total_visitas']; ?>} );
	<?php endforeach; ?>

	//data_prop = data_prop.substr(0,data_prop.length()-1);
	
	var total_visitas_diarias = <?php echo $total_visitas_diarias; ?>;
	if(total_visitas_diarias > 0){
		Morris.Donut(paramsPROP);
	} else {
		$("#details24hoursPROP").html("<div class='alert alert-info'>No existe registro de ingresos las últimas 24 horas</div>");
	}

	// Donut SERVICIOS GUARDIA
	var paramsGRD = {
	    element: 'details24hoursGRD',
	    data: [],
	    colors: ['#f26c4f', '#00a651', '#00bff3', '#0072bc', '#cccccc', '#e70c22', '#1c2b49']
	};

	data_grd = <?php 
		foreach($services_details as $sd): 
			$total_visitas_diarias += $sd['Service']['total_visitas']; 
	?>
		paramsGRD.data.push( {"label": "<?php echo $sd['Service']['glosa']?>", "value": <?php echo $sd['Service']['total_visitas']; ?>} );
	<?php endforeach; ?>
	
	var total_visitas_diarias = <?php echo $total_visitas_diarias; ?>;

	if(total_visitas_diarias > 0){ 
		Morris.Donut(paramsGRD);	
	} else {
		$("#details24hoursGRD").html("<div class='alert alert-info'>No existe registro de ingresos las últimas 24 horas</div>");
	}

	$(".pie-large").sparkline([total_visitas_diarias], {
		type: 'pie',
		width: '200px ',
		height: '200px',
		sliceColors: ['#f82']
		
	});
	
	setInterval(function(){
		Logs();
	}, 3000);
	
	function Logs(){
		// alert($("#lastID").val());
		$.ajax({
			url: "<?php echo $this->Html->url(array('action'=>'get_last_logs', 'admin'=>false), true); ?>"+"/"+$("#lastID").val(),
			type: 'GET',
			dataType: "json",
			success: function(data){
				$.each(data, function(index, log){
					style = '';
					if(log.type_id == 2){
						style = 'style="background:#feffa3;"';
					}
					$("#table-logs tbody").prepend(
						'<tr '+style+'>'+
						'<td><center><img src="<?php echo Router::url('/', true); ?>/img/'+log.type+'" width="32px"/></center></td>'+
						"<td>"+log.guest_name+"<br>"+log.guest_mobile+"</td>"+
						"<td>"+log.condo_name+"</td>"+
						"<td>"+log.house_name+"</td>"+
						"<td>"+log.organizer_name+"<br>"+log.organizer_mobile+"</td>"+ 
						"<td>"+log.relation+"</td>"+
						"<td>"+log.created+"</td>"+
						"</tr>"
					);
					$("#lastID").val(log.id);
					$("#table-logs tbody tr:first-child").hide();
					$("#table-logs tbody tr:first-child").show(400);
				})
			}
		});
	}

	$("#LogCondoId").on('change', function(){
        $.ajax({
            url: '<?php echo $this->Html->url(array('controller'=>'houses', 'action'=>'getHouseByCondo'), true); ?>/'+$(this).val(),
            dataType: 'json',
            type: 'GET',
            beforeSend: function(){
                if($("#LogCondoId").val() > 0){
                    $("#LogHouseId").prop('disabled', true);
                }
            },
            success: function(data){
                $("#LogHouseId").prop('disabled', false);
                $("#LogHouseId").html('');
                $('#LogHouseId').append($('<option>', {
                        value: '',
                        text : 'Todos'
                    }));
                $.each(data, function( index, value ) {
                    $('#LogHouseId').append($('<option>', {
                        value: index,
                        text : value
                    }));
                })
            }   
        })
    })
	
});			
</script>
<h1><?php echo __('Ingresos'); ?></h1>
<center>



<div class="panel panel-primary">
    <div class="panel-body">

    <?php echo $this->Form->create('Log', array('class'=>'form-horizontal', 'role'=>'form')); ?>
    <div class="row frm-block">
	    <div class="col-sm-2 frm-label"><?=__('Condominio') ?></div>
	    <div class="col-sm-10">
			<?php echo $this->Form->input('Log.condo_id', array(
                'label'=>false,
                'type'=>'select', 
                'class'=>'form-control', 
				'options' => array(''=>'Todos')+$condos_usuario,
				'selected' => isset($_SESSION['filter_condo_id'])? $_SESSION['filter_condo_id']:key($condos_usuario)));?>
		</div>
	</div>

    <div class="row frm-block">
	    <div class="col-sm-2 frm-label"><?=__('Propiedad') ?></div>
	    <div class="col-sm-10">
			<?php echo $this->Form->input('Log.house_id', array(
                'label'=>false,
                'type'=>'select', 
                'class'=>'form-control', 
				'selected' => isset($_SESSION['filter_house_id'])? $_SESSION['filter_house_id']:key($houses)));
			?>
		</div>
	</div>
    <div class="row frm-block">
	    <div class="col-sm-2 frm-label"><?=__('Desde / Hasta') ?></div>
	    <div class="col-sm-5">
			<?php echo $this->Form->input('Log.start_date', array(
                'label'=>false,
				'value'=>isset($_SESSION['filter_start_date'])? date('d-m-Y',strtotime($_SESSION['filter_start_date'])):date('d-m-Y',strtotime('-7 days')),
                'type'=>'text', 
                'data-format'=>'dd-mm-yyyy',
                'class'=>'form-control datepicker')); 
			?>
		</div>
	    <div class="col-sm-5">
			<?php echo $this->Form->input('Log.end_date', array(
                'label'=>false,
				'value'=>isset($_SESSION['filter_end_date'])? date('d-m-Y', strtotime($_SESSION['filter_end_date'])):date('d-m-Y'),
                'type'=>'text', 
                'data-format'=>'dd-mm-yyyy',
                'class'=>'form-control datepicker')); ?>
		</div>
	</div>
	<div class="row frm-block">
   		<div class="col-sm-10 col-sm-offset-2" align="left">
   		<?php echo $this->Form->button(__('Buscar'),  array('class'=>'btn btn-orange'));?>
    	</div>
    </div>

    <?php echo $this->Form->end(); ?>

    </div>
</div>











<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>
<?php echo $this->Form->input('last_id', array('type'=>'hidden','id'=>"lastID",'value'=>$last_id)); ?>
<table id="table-logs" class="table table-bordered responsive">
	<thead>
		<tr>
			<td>&nbsp;</td>
			<td><?php echo __('Invitado / Quien retira');?></td>
			<!--<td><?php echo __('ID');?></td>-->
			<td><?php echo __('Recinto');?></td>
			<td><?php echo __('Casa - Alumno');?></td>
			<td><?php echo __('Autorizado por');?></td>
			<td><?php echo __('Tipo - Servicio');?></td>
			<td><?php echo __('Horario');?></td>
		</tr>
	</thead>
	<?php foreach($logs as $log): ?>
	<tr <?php echo ($log['Log']['type']==2) ? 'style="background:#feffa3;"':''; ?>>
		<td>

		<center>
		<?php 
			switch($log['Log']['type']){
				case 0: 
					echo $this->Html->image('ic_arrow_out.png', array('width'=>'32px','title'=>'Salida'));
					break;
				case 1:
					echo $this->Html->image('ic_arrow_in.png', array('width'=>'32px','title'=>'Entrada'));
					break;
				case 2:
					echo $this->Html->image('ic_warning.png', array('width'=>'32px','title'=>'Alerta'));
					break;
			} 
		?>
		</center>
		</td>
		
		<td><?php echo $log['Log']['guest_name'].'<br/>'.$log['Log']['guest_mobile']; ?></td>
		<!--<td><?php echo $log['Log']['id']; ?></td>-->
		<td><?php echo $log['Condo']['name']; ?></td>
		<td><?php echo $log['Log']['house_name']; ?></td>
		<td><?php echo $log['Log']['organizer_name'].'<br/>'.$log['Log']['organizer_mobile']; ?></td>
		<td><?php echo $log['Log']['relation']; ?></td>
		<td><?php echo date('d M, H:i', strtotime($log['Log']['created'])); ?></td>
	</tr>
	<?php endforeach; ?>
</table>
<div class="paging">
<?php
    echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
?>
</div>
</center>
<br/>
<table class="table table-bordered responsive">
	<tbody>
		<tr>
			<td>
				<strong>Acceso Semanal</strong>
				<br />
				<div id="week_access" style="height: 300px"></div>
			</td>
			<!--<td width="20%">
			</td>-->
		</tr>
	</tbody>
</table>
<!--
<table class="table table-bordered responsive">
	<tbody>
		<tr>
			<td width="50%" style="text-align: center;">
				<? echo $this->Html->image('gmap.png'); ?>
			</td>
			<td style="text-align: center;">
				<? echo $this->Html->image('heatmap.png'); ?>
			</td>
		</tr>
	</tbody>
</table>
--> 

<table class="table table-bordered responsive">
	<tbody>
		<tr>
			<!--<td width="30%" style="text-align: center;">
				<h3><?php echo __('Visitas condominio'); ?></h3>
				<div class="text-center">
					<span class="pie-large"></span>
				</div>
				<p><strong><?php echo $total_visitas_diarias; ?> visitas diarias.</strong><br>
				Últimas 24 horas.</p>
			</td>-->
			<td width="50%" style="text-align: center;">
				<h3><?php echo __('Detalle 24 Horas - Invitaciones'); ?></h3>
				<div id="details24hoursPROP" style="height: 250px"></div>
			</td>
			<td style="text-align: center;">
				<h3><?php echo __('Detalle 24 Horas - Acceso Guardia'); ?></h3>
				<div id="details24hoursGRD" style="height: 250px"></div>
			</td>
		</tr>
	</tbody>
</table>