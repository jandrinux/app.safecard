<?php echo $this->Html->script('main-gsap'); ?>
<?php //echo $this->Html->script('jquery-ui-1.10.3.minimal.min'); ?>
<?php echo $this->Html->script('bootstrap'); ?>
<?php echo $this->Html->script('joinable'); ?>
<?php echo $this->Html->script('resizeable'); ?>
<?php echo $this->Html->script('neon-api'); ?>
<?php echo $this->Html->script('jquery-jvectormap-1.2.2.min'); ?>
<?php echo $this->Html->script('jquery-jvectormap-europe-merc-en'); ?>
<?php echo $this->Html->script('jquery.sparkline.min'); ?>
		
<?php echo $this->Html->script('bootstrap-datepicker'); ?>
<?php echo $this->Html->script('bootstrap-timepicker'); ?>
<?php echo $this->Html->script('daterangepicker'); ?>
		
<?php //echo $this->Html->script('/vendor/d3.v3'); ?>
<?php //echo $this->Html->script('rickshaw/rickshaw.min'); ?>
<?php echo $this->Html->script('raphael-min'); ?>
<?php echo $this->Html->script('morris.min'); ?>
<?php echo $this->Html->script('toastr'); ?>
<?php echo $this->Html->script('neon-chat'); ?>
<?php echo $this->Html->script('jquery.peity.min'); ?>
<?php //echo $this->Html->script('neon-charts'); ?>
<?php echo $this->Html->script('fileinput'); ?>
<?php echo $this->Html->script('jquery.form.min'); ?>
<?php echo $this->Html->script('neon-custom'); ?>
<?php echo $this->Html->script('neon-demo'); ?>
		
<?php echo $this->Html->css('../js/select2-bootstrap'); ?>
<?php echo $this->Html->css('../js/select2'); ?>
<?php echo $this->Html->script('select2.min'); ?>
<h1>Test</h1>
<?php
	echo $this->Form->create('Event', array('class'=>'form-horizontal','role'=>'form'));
	
	echo $this->Form->input('house_id', array('value'=>4, 'type'=>'hidden'));
	echo $this->Form->input('mobile_organizer', array('value'=>"+56982443979", 'type'=>'hidden'));
	echo $this->Form->input('mobile_guests', array('value'=>"+56982280394", 'type'=>'hidden'));
	echo $this->Form->input('name_guests', array('value'=>"Juan Pablo Herrera", 'type'=>'hidden'));
	echo $this->Form->input('relation_id', array('value'=>"3", 'type'=>'hidden'));
?>
	<div class="form-group">
	<?
	echo $this->Form->input('dfrom', array(
		'label'=>"Fecha inicio",
		'type'=>'text', 
		'data-format'=>'dd-mm-yyyy',
		'value'=>isset($this->request->data['Event']['dfrom']) ? date('d-m-Y', strtotime($this->request->data['Event']['dfrom'])):date('d-m-Y'),
		'class'=>'form-control datepicker', 
		'div'=>array('class'=>'col-sm-3'))); 
	echo $this->Form->input('hfrom', array(
		'label'=>"Hora entrada",
		'data-template'=>'dropdown',
		'data-show-seconds'=>false,
		'data-show-meridian'=>false,
		'data-minute-step'=>15,
		'type'=>'text', 
		'class'=>'form-control timepicker', 
		'div'=>array('class'=>'col-sm-3')));
	?>
	</div>
	<div class="form-group">
	<?
	echo $this->Form->input('dto', array(
		'label'=>"Fecha termino",
		'type'=>'text', 
		'data-format'=>'dd-mm-yyyy',
		'value'=>isset($this->request->data['Event']['dto']) ? date('d-m-Y', strtotime($this->request->data['Event']['dto'])):date('d-m-Y'),
		'class'=>'form-control datepicker', 
		'div'=>array('class'=>'col-sm-3')));				
	
	echo $this->Form->input('hto', array(
		'label'=>"Hora salida",
		'value'=>date('h:i A', strtotime('+1 hours')),
		'data-template'=>'dropdown',
		'data-show-seconds'=>false,
		'data-show-meridian'=>false,
		'data-minute-step'=>15,
		'type'=>'text', 
		'class'=>'form-control timepicker', 
		'div'=>array('class'=>'col-sm-3')));
	?>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label"></label>
	<?php 
	echo $this->Form->button(__('Generate'), array('class'=>'btn btn-default'));
	?>
	</div>
		
	<?php echo $this->Form->end(); ?>
	
	<?php 
		if(isset($aes) && $aes != ''){
			echo $this->Qrcode->text($aes, array('size'=>'300x300'));
		}
	?>
	