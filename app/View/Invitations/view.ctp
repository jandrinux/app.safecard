<?php
    $this->Html->addCrumb('Invitaciones', array('action'=>'index'));
    $this->Html->addCrumb('Detalle');
?>
<h2><?php echo __('Invitación'); ?></h2>

<div class="col-sm-6">
	<table class="table table-hover responsive">
		<tr>
			<td><?php echo __('Evento'); ?></td>
			<td>
				<?php echo $invitation['Event']['subject']? $invitation['Event']['subject']:'no informado'; ?>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td><?php echo __('Organizador'); ?></td>
			<td>
				<?php echo $invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname']; ?>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td><?php echo __('Condomiio - Casa/Depto'); ?></td>
			<td>
				<?php echo $invitation['Condo']['name'].' - '.$invitation['House']['name']; ?>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td><?php echo __('Fecha invitación'); ?></td>
			<td>
				<?php echo date('d M, [H:i]',strtotime($invitation['Event']['from'])).' al '.date('d M, [H:i]',strtotime($invitation['Event']['to'])); ?>
				&nbsp;
			</td>
		</tr>
	</table>
</div>

<div class="row">
	<div class="col-md-6">
		<center><?php echo $this->Qrcode->text($invitation['Invitation']['aes'], array('size'=>'300x300')); ?></center>
	</div>
</div>
