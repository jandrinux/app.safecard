
<h1><?php echo __('Invitaciones'); ?></h1>

<nav class="navbar navbar-inverse" role="navigation">
<div class="navbar-header">
</div>
</nav>

<ul class="nav nav-tabs bordered">
	<li class="active">
		<a href="#received" data-toggle="tab">
			<span class="visible-xs"><i class="entypo-home"></i></span>
			<span class="hidden-xs"><?php echo __('Recibidos'); ?></span>
		</a>
	</li>
	<li>
		<a href="#requested" data-toggle="tab">
			<span class="visible-xs"><i class="entypo-home"></i></span>
			<span class="hidden-xs"><?php echo __('Solicitudes'); ?></span>
		</a>
	</li>
	<li>
		<a href="#sent" data-toggle="tab">
			<span class="visible-xs"><i class="entypo-home"></i></span>
			<span class="hidden-xs"><?php echo __('Enviados'); ?></span>
		</a>
		
	</li>
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="received">

		<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
			<thead>
			<tr>
				<th><?php echo __('Organizador'); ?></th>
				<th><?php echo __('Condominio - Casa/Depto'); ?></th>
				<th><?php echo __('Fecha Invitación'); ?></th>
				<th><?php echo __('Creado'); ?></th>
				<th class="actions"><?php echo __('Actividad'); ?></th>
			</tr>
			</thead>
			<?php foreach ($invitations_received as $invitation):?>
			<tr>
				<td>
					<?php echo $invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname']; ?>
				</td>
				<td>
					<?php echo $invitation['Condo']['name'].' - '.$invitation['House']['name']; ?>
				</td>
				<td><?php echo h(date('d M, [H:i]',strtotime($invitation['Event']['from'])).' al '.date('d M, [H:i]',strtotime($invitation['Event']['to']))); ?>&nbsp;</td>
				<td><?php echo h(date('d M, Y',strtotime($invitation['Invitation']['created']))); ?>&nbsp;</td>
				<td class="actions">
					<div class="label label-success"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $invitation['Invitation']['id'])); ?></div>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
	</div>
	
	<div class="tab-pane" id="requested">
		<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
			<thead>
			<tr>
				<th><?php echo __('Solicitud'); ?></th>
				<th><?php echo __('Condominio - Casa/Depto'); ?></th>
				<th><?php echo __('Creado'); ?></th>
				<th class="actions"><?php echo __('Actividad'); ?></th>
			</tr>
			</thead>
			<?php foreach ($invitations_request as $invitation):?>
			<tr>
				<td>
					<?php 
						if($invitation['Guest']['id']==$this->Session->read('Auth.User.id')){
							echo 'Enviada a '.$invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname']; 
						} else {
							echo 'Solicitada por '.$invitation['Guest']['name'].' '.$invitation['Guest']['father_surname']; 
						}
					?>
				</td>
				<td>
					<?php echo $invitation['Condo']['name'].' - '.$invitation['House']['name']; ?>
				</td>
				<td><?php echo h(date('d M, Y',strtotime($invitation['Invitation']['created']))); ?>&nbsp;</td>
				<td class="actions">
					<div class="label label-success"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $invitation['Invitation']['id'])); ?></div>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
	</div>
	
	<div class="tab-pane" id="sent">
		<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
			<thead>
			<tr>
				<th><?php echo __('Invitado'); ?></th>
				<th><?php echo __('Condominio - Casa/Depto'); ?></th>
				<th><?php echo __('Fecha invitación'); ?></th>
				<th><?php echo __('Creado'); ?></th>
				<th class="actions"><?php echo __('Actividad'); ?></th>
			</tr>
			</thead>
			<?php foreach ($invitations_sent as $invitation):?>
			<tr>
				<td>
					<?php echo $invitation['Guest']['name'].' '.$invitation['Guest']['father_surname']; ?>
				</td>
				<td>
					<?php echo $invitation['Condo']['name'].' - '.$invitation['House']['name']; ?>
				</td>
				<td><?php echo h(date('d M, [H:i]',strtotime($invitation['Event']['from'])).' al '.date('d M, [H:i]',strtotime($invitation['Event']['to']))); ?>&nbsp;</td>
				<td><?php echo h(date('d M, Y',strtotime($invitation['Invitation']['created']))); ?>&nbsp;</td>
				<td class="actions">
					<div class="label label-success"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $invitation['Invitation']['id'])); ?></div>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
	</div>
</div>
