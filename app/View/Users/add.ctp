<?php echo $this->Html->script('jquery.Rut'); ?>
<script type="text/javascript">
$(function(){
	$("#UserRut").Rut({
		on_error: function(){
			alert("Rut invalido");
			$("#UserRut").focus();
		}
	})
})
</script>
<style>
label{ color: #fff; }
</style>
<div class="row">
	<h2 class="raleway"><?php echo __('Registro'); ?></h2>
	<?php 
		echo $this->Form->create("User", array('class'=>'form form-horizontal'));
	?>
	<div class="form-group">
		<label class="col-sm-3 control-label"></label>
		<?php echo $this->Form->input('rut', array('placeholder'=>'Rut','label'=>false, 'class'=>'form-control','div'=>array('class'=>'col-sm-6'))); ?>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label"></label>
		<?php echo $this->Form->input('name', array('placeholder'=>'Nombre','label'=>false, 'class'=>'form-control','div'=>array('class'=>'col-sm-6'))); ?>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label"></label>
		<?php echo $this->Form->input('father_surname', array('placeholder'=>'Apellido','label'=>false, 'class'=>'form-control','div'=>array('class'=>'col-sm-6'))); ?>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label"></label>
			<?php echo $this->Form->input('country_id', array('label'=>false,'type'=>'select','options'=>$countries,'selected'=>$country_selected, 'class'=>'form-control','div'=>array('class'=>'col-sm-2')));?>
			<?php echo $this->Form->input('mobile', array('label'=>false, 'maxlength'=>8,'placeholder'=>__('Celular'), 'class'=>'form-control','div'=>array('class'=>'col-sm-4')));?>
		
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label"></label>
		<?php echo $this->Form->input('password', array('placeholder'=>'Password (4 dígitos)','label'=>false, 'maxlength'=>4,'class'=>'form-control','div'=>array('class'=>'col-sm-2'))); ?>
		<?php echo $this->Form->input('cpassword', array('placeholder'=>'Confirma Password', 'type'=>'password','maxlength'=>4,'label'=>false, 'class'=>'form-control','div'=>array('class'=>'col-sm-2'))); ?>
	</div>
	
	<?php echo $this->Form->end(__('REGISTRARSE')); ?>
	<!--<div class="login-bottom-links">
	<?php echo $this->Html->link(__('Ya poseo cuenta Safecard'), array('controller'=>'users', 'action'=>'index'), array('class'=>'link')); ?>
	</div>-->
</div>
