<?php
    //echo $this->Html->script('jquery.Rut');
    $this->Html->addCrumb('<i class="entypo-users"></i>Propietarios', array('controller'=>'users','action'=>'owners', 'admin'=>true), array('escape'=>false));
    $this->Html->addCrumb('Nuevo propietario');
?>

<script type="text/javascript">
$(document).ready(function(){
    /* $("#OwnerRut").Rut({
        on_error: function(){ 
            alert('Rut invalido'); 
            $("#OwnerRut").focus();
        }
    }); */
    
    $("#UserCondoId").on('change', function(){
        $.ajax({
            url: '<?php echo $this->Html->url(array('controller'=>'houses', 'action'=>'getHouseByCondo'), true); ?>/'+$(this).val(),
            dataType: 'json',
            type: 'GET',
            beforeSend: function(){
                if($("#UserCondoId").val() > 0){
                    $("#UserHouseId").prop('disabled', true);
                    //$("#UserHouseId option").filter("[value='']").html('Cargando ...');
                }
            },
            success: function(data){
                $("#UserHouseId").prop('disabled', false);
                //$("#UserHouseId option").filter("[value='']").html('Seleccione');
                //$("#UserHouseId option").filter("[value!='']").remove();
                $("#UserHouseId").html('');
                $('#UserHouseId').append($('<option>', {
                        value: '',
                        text : 'Seleccione'
                }));

                var dataOrder;
                if (typeof Object.keys === 'function') {
                    dataOrder = Object.keys(data);
                } else {
                    for (var key in data) {
                        dataOrder.push(key);
                    }
                }
                dataOrder.sort(function(a, b){
                    return data[a].localeCompare(data[b]);
                });

                var results = document.getElementById('results'), listItem;
                for (var i = 0, l = dataOrder.length; i < l; i++){
                    $('#UserHouseId').append($('<option>', {
                        value: dataOrder[i],
                        text : data[dataOrder[i]]
                    }));
                }
            }   
        })
    })
    /*$("#UserCountryId").on('change', function(){
        $.ajax({
            url: '<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'getCallingCodeByCountry','admin'=>false), true); ?>/'+$(this).val(),
            dataType: 'json',
            type: 'GET',
             
            success: function(data){
                $("#UserMobile").attr("value",data.callingCode);
            }   
        })
    })*/
});
</script>

<h3><?php echo __('Agregar Propietario'); ?></h3>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">

            <?php	echo $this->Form->create('User', array('class'=>'form-horizontal', 'role'=>'form')); ?>
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Condominio') ?></label>
				<?php echo $this->Form->input('condo_id', array('label'=>false, 'options'=>array(''=>'Seleccione')+$condos,'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Casa-Depto') ?></label>
				<?php echo $this->Form->input('house_id', array('label'=>false, 'options'=>array(''=>'Seleccione')+$houses,'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Celular') ?></label>
				<?php echo $this->Form->input('country_id', array('label'=>false, 'class'=>'form-control', 'selected'=>$country_selected, 'div'=>array('class'=>'col-sm-2'))); ?>
				<?php echo $this->Form->input('mobile', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-4'))); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Nombre') ?></label>
                <?php echo $this->Form->input('name', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
			
			<div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Apellido') ?></label>
                <?php echo $this->Form->input('father_surname', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
			
            <div class="form-group">
            <label class="col-sm-3 control-label">¿Es propietario?</label>
				<?php
					echo $this->Form->input('owner', array('label'=>false,'type'=>'checkbox', 'class'=>'checkbox', 'div'=>array('class'=>'col-sm-6')));
				?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <?php
				echo $this->Html->link(__('Cancelar'),  array('action'=>'index'), array('class'=>'btn btn-default', 'div'=>array('class'=>'form-group'))).'&nbsp;';
				echo $this->Form->button(__('Guardar'),  array('class'=>'btn btn-orange', 'div'=>array('class'=>'form-group')));

            ?>
            </div>

            <?php echo $this->Form->end(); ?>

            </div>
        </div>
    </div>
</div>