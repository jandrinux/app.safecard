<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Editar Usuario'); ?></legend>
	<?php
		echo $this->Form->input('id', array('type'=>'hidden'));
		echo $this->Form->input('username', array('label'=>'Nombre de usuario'));
		echo $this->Form->input('name', array('label'=>'Nombre'));
		echo $this->Form->input('father_surname', array('label'=>'Apellido paterno'));
		echo $this->Form->input('mother_surname', array('label'=>'Apellido materno'));
		echo $this->Form->input('email');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Editar')); ?>
</div>
