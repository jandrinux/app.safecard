<script type="text/javascript">
var RecaptchaOptions = {
   theme : 'white',
   lang : 'es'
};

$(function(){
	$("input[name='via']").on('click', function(){
		if($(this).val() == 'sms'){
			$("#viaSMS").show();
			$("#viaEmail").hide();
		} else if($(this).val() == 'email'){
			$("#viaSMS").hide();
			$("#viaEmail").show();
		}
	})
	
	
});

</script>

<h3 class="col-sm-offset-3 col-sm-5 raleway">Recuperar PIN</h3>

<!--<div class="col-sm-offset-3 col-sm-5">
	<div class="radio">
		<label>
		<input type="radio" name="via" value="sms" checked />Via SMS
		</label>
	</div>
	<div class="radio">
		<label>
		<input type="radio" name="via" value="email"/>Via Email
		</label>
	</div>
</div>-->

<div id="viaSMS" class="clear">
	<?php 
		echo $this->Form->create('User', array('class'=>'form-horizontal','role'=>'form')); 
		echo $this->Form->input('via', array('value'=>'viaSms', 'type'=>'hidden'));
	?>
	<div class="form-group">
		<label class="col-sm-3 control-label"></label>
		<?php echo $this->Form->input('country_id', array('label'=>false,'type'=>'select','options'=>$countries,'selected'=>$country_selected, 'class'=>'form-control','div'=>array('class'=>'col-sm-2')));?>
		<?php echo $this->Form->input('mobile', array('label'=>false, 'placeholder'=>'Ingresa tu celular (9 dígitos)', 'maxlength'=>9, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-3'))); ?>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label"></label>
		<div class="col-sm-5">
		<?php
			$publickey = Configure::read('Captcha.PublicKey'); // you got this from the signup page
			echo recaptcha_get_html($publickey);
		?>
		</div>
	</div>
	
<?php echo $this->Form->end(__('Recuperar')); ?>
</div>

<div id="viaEmail" class="clear" style="display: none;">
<?php 
	echo $this->Form->create('User', array('class'=>'form-horizontal','role'=>'form')); 
	echo $this->Form->input('via', array('value'=>'viaEmail','type'=>'hidden'));
?>
	<div class="form-group">
		<label class="col-sm-3 control-label"></label>
		<?php echo $this->Form->input('email', array('label'=>false, 'placeholder'=>'Ingresa tu Email', 'class'=>'form-control', 'div'=>array('class'=>'col-sm-5'))); ?>
	</div>
<?php echo $this->Form->end(__('Recuperar')); ?>
</div>
