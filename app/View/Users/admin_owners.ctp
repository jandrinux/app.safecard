<script>
$(document).ready(function(){
	$("#UserCondoId").on('change', function(){
        $.ajax({
            url: '<?php echo $this->Html->url(array('controller'=>'houses', 'action'=>'getHouseByCondo'), true); ?>/'+$(this).val(),
            dataType: 'json',
            type: 'GET',
            beforeSend: function(){
                if($("#UserCondoId").val() > 0){
                    $("#UserHouseId").prop('disabled', true);
                    //$("#UserHouseId option").filter("[value='']").html('Todos');
                }
            },
            success: function(data){
                $("#UserHouseId").prop('disabled', false);
                //$("#UserHouseId option").filter("[value='']").html('Todos');
                //$("#UserHouseId option").filter("[value!='']").remove();
                $("#UserHouseId").html('');
                $('#UserHouseId').append($('<option>', {
                        value: '',
                        text : 'Todos'
                    }));

                var dataOrder;
                if (typeof Object.keys === 'function') {
                    dataOrder = Object.keys(data);
                } else {
                    for (var key in data) {
                        dataOrder.push(key);
                    }
                }
                dataOrder.sort(function(a, b){
                    return data[a].localeCompare(data[b]);
                });

                var results = document.getElementById('results'), listItem;
                for (var i = 0, l = dataOrder.length; i < l; i++){
                    $('#UserHouseId').append($('<option>', {
                        value: dataOrder[i],
                        text : data[dataOrder[i]]
                    }));
                }
            }   
        })
    })
});
</script>
	<h1><?php echo __('Propietarios - Apoderados'); ?></h1>
	
	<nav class="navbar navbar-inverse" role="navigation">
	<div class="navbar-header">
		<?php echo $this->Html->link('Agregar',array('action'=>'addowner'), array('class'=>'navbar-brand'))?>
		<?php echo $this->Html->link('Carga masiva',array('action'=>'bulk'), array('class'=>'navbar-brand'))?>
	</div>
	</nav>

<!-- filtros -->
<h3><?php echo __('Buscar propietario - apoderado'); ?></h3>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">

            <?php	echo $this->Form->create('User', array('class'=>'form-horizontal', 'role'=>'form')); ?>
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Condominio') ?></label>
				<?php echo $this->Form->input('condo_id', array(
					'label'=>false, 
					'options'=>array(''=>'Todos')+$condos_usuario,
					'class'=>'form-control', 
					'div'=>array('class'=>'col-sm-6'),
					'selected' => isset($this->request->data['User']['condo_id']) ? $this->request->data['User']['condo_id']:@$this->Session->read('filter_condo_id')
				)); ?>
            </div>
			
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Casa-Alumno') ?></label>
				<?php echo $this->Form->input('house_id', array(
					'label'=>false, 
					'class'=>'form-control', 
					'div'=>array('class'=>'col-sm-6'),
					'selected' => isset($this->request->data['User']['house_id']) ? $this->request->data['User']['house_id']:@$this->Session->read('filter_house_id')
				)); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Nombre') ?></label>
                <?php echo $this->Form->input('search_name', array(
                	'label'=>false, 
                	'class'=>'form-control', 
                	'div'=>array('class'=>'col-sm-6'),
                	'value' => isset($this->request->data['User']['search_name']) ? $this->request->data['User']['search_name']:@$this->Session->read('filter_search_name')
                )); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <?php
				echo $this->Form->button(__('Consultar'),  array('class'=>'btn btn-orange', 'div'=>array('class'=>'form-group')));
            ?>
            </div>

            <?php echo $this->Form->end(); ?>

            </div>
        </div>
    </div>
</div>
<!-- filtros -->

	<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>
	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-responsive">
	<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('name','Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('mobile','Celular'); ?></th>
            <th><?php echo __('Vinculo'); ?></th>
			<th><?php echo $this->Paginator->sort('house_id','Casa - Alumno'); ?></th>
			<th><?php echo __('Actividad'); ?></th>
		</tr>
	</thead>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['name'].' '.$user['User']['father_surname']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['mobile']); ?>&nbsp;</td>
        <td>
            <?php echo is_null($user['HousesUser']['responsible_id'])? 'Propietario':'Residente';?>
        </td>
		<td>
			<?php echo !is_null($user['House']['id']) ? $user['House']['name']:$user['Student']['full_name']; ?>
		</td>
		<td class="actions padding-lg">
            <?php //echo $user['HousesUser']['active']; ?>

            <?php if(is_null($user['HousesUser']['responsible_id'])){ ?>
                
                <?php if($user['HousesUser']['active'] == 1 && !is_null($user['HousesUser']['house_id'])){ ?>
                    <div class="label label-warning" data-toggle="tooltip" data-placement="top" data-original-title="Esta acción desvincula al propietario y sus residentes de la propiedad"><?php echo $this->Form->postLink(__('Bloquear'), array('action' => 'unlink_owner', $user['User']['id'],$user['House']['id'],'unlink_house'), null, __('Está seguro que desea desvincular la propiedad %s al propietario %s?', $user['House']['name'],$user['User']['name'].' '.$user['User']['father_surname'])); ?></div>

                <?php } else if($user['HousesUser']['active'] == 1 && !is_null($user['HousesUser']['student_id'])) { ?>
                    <div class="label label-warning" data-toggle="tooltip" data-placement="top" data-original-title="Esta acción desvincula al apoderado del alumno"><?php echo $this->Form->postLink(__('Bloquear'), array('action' => 'unlink_owner', $user['User']['id'],$user['Student']['id'],'unlink_student'), null, __('Está seguro que desea desvincular el alumno %s del apoderado %s?', $user['Student']['full_name'],$user['User']['name'].' '.$user['User']['father_surname'])); ?></div>
                <?php } ?>
                
                <?php if($user['HousesUser']['active'] == 0 && !is_null($user['HousesUser']['house_id'])){ ?>
                    <div class="label label-primary" data-toggle="tooltip" data-placement="top" data-original-title="Esta acción vincula al propietario y sus residentes a la propiedad"><?php echo $this->Form->postLink(__('Desbloquear'), array('action' => 'unlink_owner', $user['User']['id'],$user['House']['id'],'link_house'), null, __('Está seguro que desea vincular la propiedad %s al propietario %s?', $user['House']['name'],$user['User']['name'].' '.$user['User']['father_surname'])); ?>
                    </div>
                
                <?php } else if ($user['HousesUser']['active'] == 0 && !is_null($user['HousesUser']['student_id'])){ ?>
                    <div class="label label-primary" data-toggle="tooltip" data-placement="top" data-original-title="Esta acción vincula al apoderado con el alumno"><?php echo $this->Form->postLink(__('Desbloquear'), array('action' => 'unlink_owner', $user['User']['id'],$user['Student']['id'],'link_student'), null, __('Está seguro que desea vincular el alumno %s al apoderado %s?', $user['Student']['full_name'],$user['User']['name'].' '.$user['User']['father_surname'])); ?>
                    </div>
                <?php } ?>

            <?php } else { 

                if($user['HousesUser']['active'] == 0){
                    echo "<div class='label label-danger' data-toggle='tooltip' data-placement='top' data-original-title='Propietario bloqueado'>Inactivo</div>";
                } else {
                    echo "<div class='label label-success' data-toggle='tooltip' data-placement='top' data-original-title='Propietario activo'>Activo</div>";
                }
            } 
            ?>
            <?php if($this->Session->read('Auth.User.Role.code') == 'SADM'): ?>
                <div class="label label-info"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $user['User']['id'])); ?></div>
            <?php endif; ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
