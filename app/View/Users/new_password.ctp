<script type="text/javascript">
var RecaptchaOptions = {
   theme : 'white',
   lang : 'es'
};
</script>
<h3 class="col-sm-offset-3 col-sm-5 raleway">Cambiar Pin</h3>

<div id="viaSMS" class="clear">
	<?php 
		echo $this->Form->create('User', array('class'=>'form-horizontal','role'=>'form')); 
		echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$user_id));
		echo $this->Form->input('token', array('type'=>'hidden','value'=>$token));
	?>
	<div class="form-group">
		<label class="col-sm-3 control-label"></label>
		<?php echo $this->Form->input('password', array('label'=>false,'type'=>'password', 'placeholder'=>'Nuevo Pin', 'maxlength'=>4, 'class'=>'form-control','div'=>array('class'=>'col-sm-5'))); ?>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label"></label>
		<?php echo $this->Form->input('xpassword', array('label'=>false,'type'=>'password', 'placeholder'=>'Confirmar Pin', 'maxlength'=>4, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-5'))); ?>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label"></label>
		<div class="col-sm-5">
		<?php
			$publickey = Configure::read('Captcha.PublicKey'); // you got this from the signup page
			echo recaptcha_get_html($publickey);
		?>
		</div>
	</div>
	
<?php echo $this->Form->end(__('Modificar')); ?>
</div>