<?	
    /*$this->Html->addCrumb('<i class="entypo-home"></i>Viviendas', array('action'=>'index', 'admin'=>true), array('escape'=>false));
    $this->Html->addCrumb('Nueva vivienda');*/
?>
<h3><?php echo __('Envio SMS masivo'); ?></h3>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
            <?php echo $this->Form->create('User', array('class'=>'form-horizontal', 'role'=>'form')); ?>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo __('Condominio') ?></label>
            <?php echo $this->Form->input('condo_id', array('label'=>false, 'class'=>'form-control', 'options'=>array(''=>'Seleccione')+$condos, 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo __('Mensaje (SMS)'); ?></label>
            <?php echo $this->Form->input('text', array('label'=>false, 'class'=>'form-control','type'=>'textarea', 'div'=>array('class'=>'col-sm-6')));?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <? 
                //echo $this->Html->link(__('Cancelar'), array('action'=>'index'), array('class'=>'btn btn-default')).'&nbsp;';
                echo $this->Form->button(__('Enviar'), array('class'=>'btn btn-orange')); 
            ?>
            </div>
            <?	echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>