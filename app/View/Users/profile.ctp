<?php echo $this->Html->script('jquery.Rut'); ?>
<script type="text/javascript">
$(function(){
    $('#UserRut').Rut({
        on_error: function(){
            alert('Rut incorrecto');
            $('#UserRut').focus();
        }
    })
	
	var status = $('#status');
	var bar = $(".progress-bar");
	var span_text = $(".sr-only");
	
	$('#profileForm').ajaxForm({
		beforeSend: function() {
			$("#status").html('Cargando...');
		},
		uploadProgress: function(event, position, total, percentComplete) {
			var percentVal = percentComplete + '%';
			bar.width(percentVal);
			span_text.html(percentVal+'%');
			// percent.html(percentVal);
			// console.log(percentVal, position, total);
		},
		type: 'POST',
		dataType: 'json',
		success: function(data) {
			alert(data.message);
			window.location.reload();
		},
		complete: function(xhr) {
			//alert(xhr.responseText);
			//status.html(xhr.responseText);
		}
	});

    $('#passForm').ajaxForm({
        beforeSend: function() {
            $("#status").html('Cargando...');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            span_text.html(percentVal+'%');
        },
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            alert(data.message);
            window.location.reload();
        }
    });
})
</script>

<?php	
    $this->Html->addCrumb('<i class="entypo-users"></i>Mi Perfil', array('action'=>'view', 'admin'=>true), array('escape'=>false));
    $this->Html->addCrumb('Editar perfil');
?>
<h3>Editar perfil</h3>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
            <?php 
                echo $this->Form->create('User', array('url'=>array('action'=>'profile'),'class'=>'form-horizontal', 'id'=>'profileForm','role'=>'form', 'type'=>'file'));
                echo $this->Form->input('id', array('type'=>'hidden'));
            ?>
            
			<div class="form-group">
				<div class="progress">
					<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0", aria-valuemin="0", aria-valuemax="0">
						<span class="sr-only">0%</span>
					</div>
				</div>
			</div>
			
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Rut') ?></label>
                <?php echo $this->Form->input('rut', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Nombre') ?></label>
                <?php echo $this->Form->input('name', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Ap. Paterno') ?></label>
                <?php echo $this->Form->input('father_surname', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Ap. Materno') ?></label>
                <?php echo $this->Form->input('mother_surname', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Email') ?></label>
                <?php echo $this->Form->input('email', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
            
            <!--<div class="form-group">
                <label class="col-sm-3 control-label">¿Usuario activo?</label>
                <?php echo $this->Form->input('active', array('label'=>false,'class'=>'checkbox', 'div'=>array('class'=>'col-sm-6')));?>
            </div>-->
            
			<!--<div class="form-group">
                <label class="col-sm-3 control-label"></label>
				<?php echo $this->Form->input('image', array('label'=>false, 'type'=>'file', 'class'=>'file-input-wrapper btn form-control file2 inline btn btn-primary', 'data-label'=>'<i class="entypo-vcard"></i> Seleccionar imagen'));?>
			</div>-->
			
            <div class="form-group clear">
                <label class="col-sm-3 control-label"></label>
                <?php echo $this->Html->link(__('Cancelar'),array('action'=>'view'), array('class'=>'btn btn-default')).'&nbsp;';?>
                <?php echo $this->Form->button(__('Editar'), array('label'=>false, 'class'=>'btn btn-orange'));?>
            </div>
            
			<?php echo $this->Form->end();?> 
            </div>
        </div>
                <div class="panel panel-primary">
            <div class="panel-body">
            <?php 
                echo $this->Form->create('User', array('url'=>array('action'=>'editpin','admin'=>false),'class'=>'form-horizontal', 'id'=>'passForm','role'=>'form', 'type'=>'file'));
                echo $this->Form->input('id', array('type'=>'hidden'));
            ?>
            
            <div class="form-group">
                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0", aria-valuemin="0", aria-valuemax="0">
                        <span class="sr-only">0%</span>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Password actual') ?></label>
                <?php echo $this->Form->input('old_password', array('value'=>'','maxlength'=>4, 'label'=>false, 'type'=>'password','class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Nuevo password') ?></label>
                <?php echo $this->Form->input('new_password', array('value'=>'','maxlength'=>4, 'label'=>false, 'type'=>'password','class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Confirmar nuevo password') ?></label>
                <?php echo $this->Form->input('password', array('value'=>'','maxlength'=>4, 'label'=>false, 'type'=>'password','class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
            
            <div class="form-group clear">
                <label class="col-sm-3 control-label"></label>
                <?php echo $this->Html->link(__('Cancelar'),array('action'=>'view'), array('class'=>'btn btn-default')).'&nbsp;';?>
                <?php echo $this->Form->button(__('Modificar'), array('label'=>false, 'class'=>'btn btn-orange'));?>
            </div>
            
            <?php echo $this->Form->end();?> 
            </div>
        </div>
    </div>
</div>
