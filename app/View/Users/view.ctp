
<?php
    if($this->Session->read('Auth.User.id') != $user['User']['id']){
        $this->Html->addCrumb('<i class="entypo-users"></i>Usuarios', array('action'=>'index', 'admin'=>true), array('escape'=>false));
        $this->Html->addCrumb('Usuarios');
    }
?>

<?php if($this->Session->read('Auth.User.id') == $user['User']['id']): ?>
    <h3><?php echo __('Mi Perfil'); ?></h3>
    <nav class="navbar navbar-inverse" role="navigation">
    <div class="navbar-header">
        <?=$this->Html->link('Editar perfil',array('action'=>'profile'), array('class'=>'navbar-brand'))?>
    </div>
    </nav>
    
<?php else :?>
    <h3><?php echo __('Detalle'); ?></h3>
<?php endif; ?>
<? //echo $this->webroot; ?>
<div class="profile-env">
	<header class="row">
		<div class="col-sm-2">
		<a href="#" class="profile-picture">
			<?php //echo $this->Html->image('profile.png', array('class'=>'img-responsive img-circle')) ?>
			<?php echo $this->Html->image(
								(is_null($this->Session->read('Auth.User.image')) || $this->Session->read('Auth.User.image')=='')?'profile.png':'/files/profile/'.$this->Session->read('Auth.User.image'), 
								array('width'=>'44px', 'class'=>'img-circle'));  ?>
			</a>
		</div>
		<div class="col-sm-3">
			<ul class="profile-info-sections">
				<li>
					<div class="profile-name">
						<strong><a href="#"><?php echo h($user['User']['name'].' '.$user['User']['father_surname'].' '.$user['User']['mother_surname']); ?></a></strong>
						<!--<span><?php echo __('Cel:'); ?> <a href="#"><?php echo h($user['User']['mobile']); ?></a></span>-->
						<!--<span><?php echo __('Rut:'); ?> <?php echo h($user['User']['rut']); ?></span>-->
					</div>
				</li>
			</ul>
		</div>
		<div class="col-sm-6">
			<table class="table table-hover responsive"> 
				<tr>
					<td><?php echo __('Celular'); ?></td>
					<td>
						<?php echo h($user['User']['mobile']); ?>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td><?php echo __('Email'); ?></td>
					<td>
						<?php echo h($user['User']['email']); ?>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td><?php echo __('Ultimo ingreso'); ?></td>
					<td>
						<?php echo h(get_time_difference($user['User']['last_login'])); ?>
						&nbsp;
					</td>
				</tr>
			</table>
		</div>
	</header>
	<!--<section class="profile-info-tabs">
		<div class="row">
			<div class="col-sm-offset-2 col-sm-10">
				<ul class="user-details">
					<li>
						<a href="#"><i class="entypo-location"></i>&nbsp;<?php echo 'Santiago, Chile'; ?></a></li>
					<li>
						<a href="#"><i class="entypo-calendar"></i>&nbsp;Último ingreso:&nbsp;
							<?php echo get_time_difference($user['User']['last_login']); ?>
						</a></li>
				</ul>
				<ul class="nav nav-tabs">
					<li><?php echo $this->Html->link('Perfiles', array());?></li>
					<li><?php echo $this->Html->link('Propiedades', array());?></li>
				</ul>
			</div>
		</div>
	</section>-->
</div>


