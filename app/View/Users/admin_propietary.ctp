<h1><?php echo __('Propietarios'); ?></h1>
	
	<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>

	<div class="OptionsBar">
		<?=$this->Html->link('Agregar',array('action'=>'add'), array('class'=>'itembar fright'))?>
	</div>
	
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('name','Nombre'); ?></th>
		<th><?php echo $this->Paginator->sort('father_surname','Ap. Paterno'); ?></th>
		<th><?php echo $this->Paginator->sort('mother_surname','Ap. Materno'); ?></th>
		<th><?php echo $this->Paginator->sort('email'); ?></th>
		<th><?php echo $this->Paginator->sort('last_login','Ultimo ingreso'); ?></th>
		<th><?php echo $this->Paginator->sort('created','Creado'); ?></th>
		<th class="actions"><?php echo __('Actividades'); ?></th>
	</tr>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['father_surname']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['mother_surname']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<td><?php echo h(date('d-m-Y H:i', strtotime($user['User']['last_login']))); ?>&nbsp;</td>
		<td><?php echo h(date('d-m-Y', strtotime($user['User']['created']))); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $user['User']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>

