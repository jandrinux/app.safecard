<?php
    // echo $this->Html->script('jquery.Rut');
    $this->Html->addCrumb('<i class="entypo-users"></i>Usuarios', array('action'=>'index', 'admin'=>true), array('escape'=>false));
    $this->Html->addCrumb('Editar usuario');
?>
<script type="text/javascript">
$(document).ready(function(){
    /* $("#UserRut").Rut({
        on_error: function(){ 
            alert('Rut incorrecto'); 
            $("#UserRut").focus();
        }
    }); */
});
</script>
<h3><?php echo __('Editar Usuario'); ?></h3>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
            <?php 
                echo $this->Form->create('User', array('class'=>'form-horizontal', 'role'=>'form'));
                echo $this->Form->input('id', array('type'=>'hidden'));
            ?>
            
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Rol') ?></label>
                <?php echo $this->Form->input('UsersRole.role_id', array('selected'=>$roles_selected, 'label'=>false, 'class'=>'select2','multiple'=>'multiple','div'=>array('class'=>'col-sm-6'))); ?>
            </div>
			
			<div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Condominio') ?></label>
                <?php echo $this->Form->input('CondosUser.condo_id', array('selected'=>$condos_selected, 'label'=>false, 'class'=>'select2','multiple'=>'multiple','div'=>array('class'=>'col-sm-6'))); ?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Nombre') ?></label>
                <?php echo $this->Form->input('name', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Ap. Paterno') ?></label>
                <?php echo $this->Form->input('father_surname', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Ap. Materno') ?></label>
                <?php echo $this->Form->input('mother_surname', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Email') ?></label>
                <?php echo $this->Form->input('email', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
            
            <!--<div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Celular') ?></label>
                <?php echo $this->Form->input('mobile', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>-->
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Usuario activo</label>
                <?php echo $this->Form->input('active', array('label'=>false,'class'=>'checkbox', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Notificaciones de ingreso</label>
                <?php echo $this->Form->input('notifications_pref', array('label'=>false, 'type'=> 'checkbox','class'=>'checkbox', 'div'=>array('class'=>'col-sm-6')));?>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <?php echo $this->Html->link(__('Cancelar'),array('action'=>'index'), array('class'=>'btn btn-default')).'&nbsp;';?>
                <?php echo $this->Form->button(__('Editar'), array('label'=>false, 'class'=>'btn btn-orange'));?>
            </div> 
                
            <?php echo $this->Form->end();?> 
            </div>
        </div>
    </div>
</div>

