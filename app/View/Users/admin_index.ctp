<script type="text/javascript">
$(function(){
    $('.RolesUser').on('click', function(){
        var user_id = $(this).attr('user_id');
        var role_id = $(this).attr('value');
        
        if($(this).is(':checked')){
            var action = 'add';
        } else {
            var action = 'delete';
        }
        
        $.ajax({
            dataType: 'json',
            url: '<?php echo $this->Html->url(array('controller'=>'users','action'=>'changeRole'),true);?>/'+user_id+'/'+role_id+'/'+action,
            type: 'get'
        })
    })
})
</script>
	<h1><?php echo __('Personal del recinto'); ?></h1>
	
	<nav class="navbar navbar-inverse" role="navigation">
	<div class="collapse navbar-collapse">
		<?php echo $this->Html->link('Agregar',array('action'=>'add'), array('class'=>'navbar-brand')); ?>
		
		<?php 
			echo $this->Form->create('User', array('class'=>'navbar-form navbar-right')); 
			
			echo "<div class='form-group'>";
			echo $this->Form->input('condo_id', array(
                'label'=>false,
                'type'=>'select', 
                'class'=>'form-control', 
				'options' => array(''=>'Recintos') + $condos, 
                'div'=>array('class'=>'col-sm-3'))); 
			echo "</div>";	
			echo "<div class='form-group'>";
			echo $this->Form->input('role_id', array(
                'label'=>false,
                'type'=>'select', 
                'class'=>'form-control', 
				'options' => array(''=>'Roles') + $roles, 
                'div'=>array('class'=>'col-sm-3'))); 
			echo "</div>";	
			
			echo $this->Form->button(__('Buscar'), array('class'=>'btn btn-default'));
			echo $this->Form->end();
		?>
	</div>
	</nav>
	
	<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>
	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-hover responsive">
	<thead>
        <tr>
			<!--<th><?php echo $this->Paginator->sort('rut','Rut'); ?></th>-->
            <th><?php echo $this->Paginator->sort('name','Nombre'); ?></th>
            <th><?php echo $this->Paginator->sort('mobile','Celular'); ?></th>
			<th><?php echo $this->Paginator->sort('email','Email'); ?></th>
			<!--<th><?php echo $this->Paginator->sort('condos','Recinto'); ?></th>-->
            <th><?php echo $this->Paginator->sort('last_login','Ultimo ingreso'); ?></th>
            <!--<th><?php echo $this->Paginator->sort('created','Creado'); ?></th>-->
            <th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	</thead>
        <tbody>
	<?php foreach ($users as $user): ?>
	<tr>
		<!--<td><?php echo h($user['User']['rut']); ?>&nbsp;</td>-->
		<td><?php echo h($user['User']['name'].' '.$user['User']['father_surname']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['mobile']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<!--<td><?php echo h($user['User']['condos']); ?>&nbsp;</td>-->
		<td><?php echo h(date('d-m-Y H:i', strtotime($user['User']['last_login']))); ?>&nbsp;</td>
		<!--<td><?php echo h(date('d-m-Y', strtotime($user['User']['created']))); ?>&nbsp;</td>-->
		<td class="actions padding-lg">
			<div class="label label-success"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $user['User']['id'])); ?></div>
			<div class="label label-info"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $user['User']['id'])); ?></div>
			<div class="label label-warning"><?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $user['User']['id']), null, __('Esta seguro que desea eliminar el usuario %s?', $user['User']['name'].' '.$user['User']['father_surname'])); ?></div>
		</td> 
	</tr>
<?php endforeach; ?>
        </tbody>
	</table>
	
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>

