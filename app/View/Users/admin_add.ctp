<?php echo $this->Html->script('jquery.Rut');?>
<script type="text/javascript">
$(function(){
    $("#UserRut").Rut({
        on_error: function(){
            alert('Rut incorrecto');
            $("#UserRut").focus();
        }
    })


    $("#UserCountryId").on('change', function(){
        $.ajax({
            url: '<?php echo $this->Html->url(array('controller'=>'owners', 'action'=>'getCallingCodeByCountry'), true); ?>/'+$(this).val(),
            dataType: 'json',
            type: 'GET',
             
            success: function(data){
                $("#UserMobile").attr("value",data.callingCode);
            }   
        })
    })
})
</script>
<?php	
    $this->Html->addCrumb('<i class="entypo-users"></i>Usuarios', array('action'=>'index', 'admin'=>true), array('escape'=>false));
    $this->Html->addCrumb('Nuevo condominio');
?>
<h3><?=__('Nuevo Usuario');?></h3>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
            <?php echo $this->Form->create('User', array('class'=>'form-horizontal','role'=>'form')); ?>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Rol') ?></label>
            <?php echo $this->Form->input('UsersRole.role_id', array('label'=>false, 'class'=>'select2','multiple'=>'multiple','div'=>array('class'=>'col-sm-6'))); ?>
            </div>
			
			<div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Condominio') ?></label>
            <?php echo $this->Form->input('CondosUser.condo_id', array('label'=>false, 'options'=>$condos_usuario,'class'=>'select2','multiple'=>'multiple','div'=>array('class'=>'col-sm-6'))); ?>
            </div>
            
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Rut') ?></label>
               <?php echo $this->Form->input('rut', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
                
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Nombre') ?></label>
               <?php echo $this->Form->input('name', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Apellido paterno') ?></label>
            <?php echo $this->Form->input('father_surname', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Apellido materno') ?></label>
            <?php echo $this->Form->input('mother_surname', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Email') ?></label>
            <?php echo $this->Form->input('email', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
            
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Celular') ?></label>
                <?php echo $this->Form->input('country_id', array('label'=>false, 'class'=>'form-control', 'selected'=>$country_selected, 'div'=>array('class'=>'col-sm-2'))); ?>
                <?php echo $this->Form->input('mobile', array('label'=>false, 'value'=>$callingCode, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-4'))); ?>
            </div>
                
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Password') ?></label>
                <?php echo $this->Form->input('password', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Repita Password') ?></label>
                <?php echo $this->Form->input('cpassword', array('label'=>false, 'type'=>'password', 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <?php
                echo $this->Html->link(__('Cancelar'), array('action'=>'index'), array('class'=>'btn btn-default')).'&nbsp;';
                echo $this->Form->button(__('Enviar'), array('class'=>'btn btn-orange'));
            ?>
            </div>
            <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
