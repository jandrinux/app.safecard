<script type="text/javascript">
$(function(){
    
    $("#UserRoleId").on('change', function(){
        if($(this).val()!=''){
            var formData = $("#UserSetRoleForm").serializeArray();
            var url = '<?php echo $this->Html->url(array('action'=>'ajax_login','admin'=>false),true); ?>';
            
            $.ajax({
                url: url,
                data: formData,
                type: 'post',
                dataType: 'json',
                success: function(data){
                    location.href= "<?php echo $this->Html->url('/', true); ?>"+data.admin+data.controller+data.action;
                }
            })
            
        } else {
            alert("Seleccione el perfil con el que desea comenzar.");
        }
    })
});
</script>


<?php if($results['exito'] == 1): ?>
    <div> Bienvenido:<?php echo $usuario['User']['name'].' '.$usuario['User']['father_surname']; ?></div>
    <div> Seleccione el perfil con el que desea comenzar: </div>
<?php     
    echo $this->Form->create('User'); 
    echo $this->Form->input('password', array('value'=>$pass_original,'type'=>'hidden'));
    echo $this->Form->input('username', array('value'=>$usuario['User']['username'],'type'=>'hidden'));
    echo $this->Form->input('role_id', array('options'=>array(''=>'Seleccione Rol') + $roles,'type'=>'select','label'=>'Perfil','class'=>'large'));
    echo $this->Form->end();
else:
    echo $results['msg'];
endif;


