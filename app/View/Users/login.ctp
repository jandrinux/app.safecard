<style>

.page-container{ padding-left: 20px; }
.nopadding{padding:0px;}
/*.img-margin{ margin-bottom: 20px; }*/
strong {color: #303641;}
/*p { font-size: 18px; color: #000;}*/
</style>
<div class="row">
	<div class="col-md-7">
		
		
		<?php 
			echo $this->Form->create('User', array('role'=>'form','id'=>'form_login'));
		?>

		<div class="row">
			<div class="col-md-12">
			<h1 class="raleway"><strong>Sistema de Gestión</strong></h1>
			<h1 class="raleway"><strong>de Acceso Seguro</strong></h1>
			<h2>Accede a la app safecard<span>&#174;</span></h2>	
			</div>
		</div>
		
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-addon">
					<i class="entypo-mobile"></i>
				</div>
				<?php echo $this->Form->input('country_id', array('label'=>false,'type'=>'select','options'=>$countries,'selected'=>$country_selected, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-5 nopadding')));?>
				<?php echo $this->Form->input('mobile', array('style'=>array('font-size: 20px; text-align: center'),'label'=>false,'maxlength'=>9, 'placeholder'=>__('Móvil (9 dígitos)'), 'class'=>'form-control', 'div'=>array('class'=>'col-sm-7 nopadding')));?>		
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-addon">
					<i class="entypo-key"></i>
				</div>
				<?php echo $this->Form->input('password', array('style'=>array('font-size: 20px'), 'label'=>'', 'maxlength'=>4, 'placeholder'=>__('Password'), 'class'=>'form-control', 'div'=>array('class'=>'col-sm-12 nopadding')));?>
			</div>
		</div>
		<div class="form-group">
		<?php 
			echo $this->Form->end(__("INGRESAR"));
		?>
		</div>
		<div class="login-bottom-links">
			<p><?php echo $this->Html->link("¿Olvidó su password?, Recupéralo aquí", array('action'=>'pass_recovery'), array('class'=>'link')); ?></p>
		</div>
	</div>
	<div class="col-md-5">
		<?php echo $this->Html->image('smartphone.png', array('class'=>'img-responsive center-block','width'=>'160px'));?>
	</div>
</div>
