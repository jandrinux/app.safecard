<?php echo $this->Html->script("http://maps.google.com/maps/api/js?sensor=false", false); ?>

<?php	
	$this->Html->addCrumb('<i class="entypo-home"></i>Recintos', array('action'=>'index', 'admin'=>true), array('escape'=>false));
	$this->Html->addCrumb('Detalle recinto');
?>

<h3><?php echo h($condo['Condo']['name']); ?> [<?php echo $condo['Condo']['type']==1?'Colegio':'Condominio'; ?>]</h3>
<table class="table table-hover">
	<tbody>
		<tr>
			<td><? echo __('Dirección'); ?></td>
			<td><?php echo h($condo['Condo']['address']); ?></td>
		</tr>
		<tr>
			<td><? echo __('Comuna'); ?></td>
			<td><?php echo h($condo['Comuna']['glosa']); ?></td>
		</tr>
		<tr>
			<td><? echo __('Registrado'); ?></td>
			<td><?php echo h(date('d M, Y', strtotime($condo['Condo']['created']))); ?></td>
		</tr>
		<tr>
			<td><? echo __('Registrado'); ?></td>
			<td><?php echo h(date('d M, Y', strtotime($condo['Condo']['modified']))); ?></td>
		</tr>
	</tbody>
</table>

<?php
	$map_options = array(
		"id"         => "map_canvas",
		"width"      => "800px",
		"height"     => "400px",
		"localize"   => false,
		"zoom"       => 16,
		"latitude"	 => $condo['Condo']['lat'],
		"longitude"	 => $condo['Condo']['long'],
		"marker"     => true,
		"infoWindow" => true,
		"type"     => "ROADMAP",
		"text"	=> $condo['Condo']['name'],
	);
	echo $this->GoogleMap->map($map_options); 
?>
