<?php echo $this->Html->script("http://maps.google.com/maps/api/js?sensor=false", false); ?>
<script>
$(function(){
	
	$('#CondoComunaId, #CondoAddress').on('change', function(){
		var direccion = $("#CondoAddress").val();
		var comuna = $('#CondoComunaId option:selected').text();
		var address = direccion + ', ' + comuna;
		
		$.ajax({
			url: "http://maps.googleapis.com/maps/api/geocode/json?address="+address+"&sensor=false",
			type: "POST",
			success: function(res){
				lat = res.results[0].geometry.location.lat;
				lng = res.results[0].geometry.location.lng;
				$("#CondoLat").attr('value',lat);
				$("#CondoLong").attr('value',lng);
			}
		});
	})
	
})
</script>

<?	
	$this->Html->addCrumb('<i class="entypo-home"></i>Recintos', array('action'=>'index', 'admin'=>true), array('escape'=>false));
	$this->Html->addCrumb('Editar recinto');
?>

<h3><?php echo __('Editar Recinto');?></h3>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
		
			<div class="panel-body">
			<?php 
				echo $this->Form->create('Condo', array('class'=>'form-horizontal', 'role'=>'form')); 
				echo $this->Form->input('id');
			?>
			<h4><?php echo __('Información Recinto'); ?></h4>
			<div class="form-group">
				<label class="col-sm-3 control-label"><?=__('Nombre') ?></label>
				<?php echo $this->Form->input('name', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label"><?=__('Dirección') ?></label>
				<?php echo $this->Form->input('address', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label"><?=__('Comuna') ?></label>
				<?php echo $this->Form->input('comuna_id', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
			</div>

			<div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Tipo Recinto') ?></label>
            <?php echo $this->Form->input('type', array('type'=>'select','options'=>array('0'=>'Condominio','1'=>'Colegio'),'selected'=>$this->request->data['Condo']['type'] ,'label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
			
			<h4><?php echo __('Tiempos de alerta'); ?></h4>
			<?php foreach($condos_relations as $key=>$relation): ?>
			<div class="form-group">
				<label class="col-sm-3 control-label"><?php echo $relation['Relation']['name']; ?></label>
				<div class="input-spinner">
				<?php
					// echo $this->Form->input('relation_id', array('data-min'=>0, 'data-max'=>60, 'type'=>'hidden', 'label'=>false, 'value'=>$relation_id, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));
					echo $this->Form->button('-', array('class'=>'btn btn-default','data-step'=>-5));
					
					echo $this->Form->input('CondosRelation.'.$relation['Relation']['id'].'.alert_time', array(
						'data-min'=>0, 
						'data-max'=>60, 
						'label'=>false, 
						'class'=>'form-control size-1', 
						'div'=>false,
						'value'=>!empty($this->request->data['Relation'])? $this->request->data['Relation'][$key]['CondosRelation']['alert_time']:0,
						'type'=>'text'
					));
					
					echo $this->Form->button('+', array('class'=>'btn btn-default','data-step'=>5));
				?>
				</div>
			</div>
			<?php endforeach; ?>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">&nbsp;</label>
				<? 
					echo $this->Html->link(__('Cancelar'), array('action'=>'index'), array('class'=>'btn btn-default')).'&nbsp;';
					echo $this->Form->button(__('Editar'), array('class'=>'btn btn-orange', 'div'=>array('class'=>'form-group'))); 
				?>
			</div>
			<?	echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>