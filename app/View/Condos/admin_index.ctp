
<h1><?php echo __('Recintos'); ?></h1>

<?php if($this->Session->read('Auth.User.Role.code') == 'SADM'): ?>
<nav class="navbar navbar-inverse" role="navigation">
<div class="navbar-header">
	<?=$this->Html->link('Agregar',array('action'=>'add'), array('class'=>'navbar-brand'))?>
</div>
</nav>
<?php endif; ?>

<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>
<div class="col-md-12">
	<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th><?php echo $this->Paginator->sort('name','Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('address','Dirección'); ?></th>
			<th><?php echo $this->Paginator->sort('comuna_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created','Creado'); ?></th>
			<th class="actions"><?php echo __('Actividad'); ?></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($condos as $condo): ?>
	<tr>
		<td>
			<?php
			if($condo['Condo']['type'] == 1){
				echo "<i class=entypo-graduation-cap title=Colegio></i>";
			} else {
				echo "<i class=entypo-home title=Condominio></i>";
			}
			?>
		</td>
		<td><?php echo h($condo['Condo']['name']); ?>&nbsp;</td>
		<td><?php echo h($condo['Condo']['address']); ?>&nbsp;</td>
		<td>
			<?php echo h($condo['Comuna']['glosa']); ?>
		</td>
		<td><?php echo h($condo['Condo']['created']); ?>&nbsp;</td>
		<td class="actions">
			<div class="label label-success"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $condo['Condo']['id']), array('class'=>'blue')); ?></div>
			<div class="label label-info"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $condo['Condo']['id']), array('class'=>'green')); ?></div>
			<?php if($this->Session->read('Auth.User.Role.code') == 'SADM'): ?>
			<div class="label label-warning"><?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $condo['Condo']['id']), array('class'=>'orange'), __('Está seguro que desea eliminar el recinto %s?', $condo['Condo']['name'])); ?></div>
			<?php endif; ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
    
 
