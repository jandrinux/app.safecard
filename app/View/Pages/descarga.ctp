<script type='text/javascript'>var _merchantSettings=_merchantSettings || [];_merchantSettings.push(['AT', '926535612']);(function(){var autolink=document.createElement('script');autolink.type='text/javascript';autolink.async=true; autolink.src= ('https:' == document.location.protocol) ? 'https://autolinkmaker.itunes.apple.com/js/itunes_autolinkmaker.js' : 'http://autolinkmaker.itunes.apple.com/js/itunes_autolinkmaker.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(autolink, s);})();</script>
<style>

.page-container{ padding-left: 0px; }
.img-margin{ margin-bottom: 20px; }
strong {color: #303641;}
p { font-size: 18px; color: #000;}
</style>
<div class="row">
	<div class="col-md-12">
		<p class="text-center"><strong>Descarga safecard ahora</strong> y ten el control de tus accesos en la palma de tu mano</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
			<!--https://itunes.apple.com/us/app/safecard/id926535612?ls=1&mt=8-->
			<?php 
				echo $this->Html->link(
					$this->Html->image('Button_iOS.png', array('class'=>'img-margin img-responsive center-block')),
					"https://itunes.apple.com/us/app/safecard/id926535612?ls=1&mt=8",
	    			array('escape' => false)
				); 
			?>
		</div>
		<div class="col-md-6">
			<!--https://play.google.com/store/apps/details?id=com.safecard.android-->
			<?php 
				echo $this->Html->link(
					$this->Html->image('Button_Android.png', array('class'=>'img-margin img-responsive center-block')),
					'https://play.google.com/store/apps/details?id=com.safecard.android',
					array('escape'=>false)
				); ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php echo $this->Html->image('logo_base.png', array('class'=>'img-responsive center-block')); ?>
	</div>
</div>