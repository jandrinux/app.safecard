
<h3><?php echo __('Notificaciones'); ?></h3>

<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>

<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
<thead>
    <tr>
        <th><?php echo $this->Paginator->sort('name','Titulo'); ?></th>
        <th><?php echo $this->Paginator->sort('description','Descripción'); ?></th>
        <th><?php echo $this->Paginator->sort('created','Registro'); ?></th>
    </tr>
</thead>
<?php foreach ($notifications as $notification): ?>
<tr>
    <td><?php echo h($notification['Notification']['name']); ?>&nbsp;</td>
    <td><?php echo h($notification['Notification']['description']); ?>&nbsp;</td>
    <td><?php echo h(date('d M, Y',strtotime($notification['Notification']['created']))); ?>&nbsp;</td>
</tr>
<?php endforeach; ?>
</table>
        
<div class="paging">
<?php
        echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
?>
</div>
