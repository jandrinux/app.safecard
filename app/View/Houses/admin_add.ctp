<?	
    $this->Html->addCrumb('<i class="entypo-home"></i>Viviendas', array('action'=>'index', 'admin'=>true), array('escape'=>false));
    $this->Html->addCrumb('Nueva vivienda');
?>
<h3><?php echo __('Nueva Vivienda'); ?></h3>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
            <?php echo $this->Form->create('House', array('class'=>'form-horizontal', 'role'=>'form')); ?>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Condominio') ?></label>
            <? echo $this->Form->input('condo_id', array('label'=>false, 'class'=>'form-control', 'options'=>array(''=>'Seleccione')+$condos, 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Casa-Depto') ?></label>
            <? echo $this->Form->input('name', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <? 
                echo $this->Html->link(__('Cancelar'), array('action'=>'index'), array('class'=>'btn btn-default')).'&nbsp;';
                echo $this->Form->button(__('Guardar'), array('class'=>'btn btn-orange')); 
            ?>
            </div>
            <?	echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>