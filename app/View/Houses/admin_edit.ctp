<?php	
    $this->Html->addCrumb('<i class="entypo-home"></i>'.__('Viviendas'), array('action'=>'index', 'admin'=>true), array('escape'=>false));
    $this->Html->addCrumb('Editar vivienda');
?>
<h3><? echo __('Editar propiedad');?></h3>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
            <?php 
                    echo $this->Form->create('House', array('class'=>'form-horizontal','role'=>'form'));
                    echo $this->Form->input('id');
            ?>
            <div class="form-group">
                    <label class="col-sm-3 control-label"><?=__('Condominio') ?></label>
                    <? echo $this->Form->input('condo_id', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
            <div class="form-group">
                    <label class="col-sm-3 control-label"><?=__('Nombre') ?></label>
                    <? echo $this->Form->input('name', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
            <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <?
                            echo $this->Html->link(__('Cancelar'), array('action'=>'index'), array('class'=>'btn btn-default')).'&nbsp;';
                            echo $this->Form->button(__('Editar'), array('class'=>'btn btn-orange', 'div'=>array('class'=>'form-group'))); 
                    ?>
            </div>
            <?	echo $this->Form->end(); ?>
        </div>
    </div>
</div>