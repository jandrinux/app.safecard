 
<h2><?php echo __('Propiedades').' ('.count($houses).')'; ?></h2>

<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
    <thead>
        <tr>
            <th><?php echo __('Condominio'); ?></th>
            <th><?php echo __('Casa - Depto'); ?></th>
            <th><?php echo __('created','Creado'); ?></th>
            <th><?php echo __('#Eventos');?>
        </tr>
    </thead>
    <?php
        if(!empty($houses)) {
            foreach ($houses as $house): ?> 
    <tr>
        <td>
            <?php echo $house['Condo']['name']; ?>
        </td>
        <td><?php echo h($house['House']['name']); ?>&nbsp;</td>
        <td><?php echo h(date('d M, Y', strtotime($house['House']['created']))); ?>&nbsp;</td>
        <td>
            <div class="label label-success"><?php echo $house['House']['total_eventos']?> </div>
        </td>
        <!--<td class="actions">
            <div class="label label-success"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $house['House']['id']), array('class'=>'blue')); ?></div>
            <div class="label label-info"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $house['House']['id']), array('class'=>'green')); ?></div>
            <div class="label label-warning"><?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $house['House']['id']), array('class'=>'orange'), __('Seguro desea eliminar la vivienda %s?', $house['House']['name'])); ?></div>
        </td>-->
    </tr>
    <?php 
            endforeach;
        } 
    ?>
</table>

<hr/>

<h2><?php echo __('Alumnos').' ('.count($students).')'; ?></h2>

<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
    <thead>
        <tr>
            <th><?php echo __('Colegio'); ?></th>
            <th><?php echo __('Alumno'); ?></th>
            <th><?php echo __('Creado'); ?></th>
            <th><?php echo __('#Eventos');?>
            <!--<th class="actions"><?php echo __('Actividad'); ?></th>-->
        </tr>
    </thead>
    <?php
        if(!empty($students)) {
            foreach ($students as $student): ?> 
    <tr>
        <td>
            <?php echo $student['School']['name']; ?>
        </td>
        <td><?php echo h($student['Student']['full_name']); ?>&nbsp;</td>
        <td><?php echo h(date('d M, Y', strtotime($student['Student']['created']))); ?>&nbsp;</td>
        <td>
            <div class="label label-success"><?php echo $student['Student']['total_eventos']?> </div>
        </td>
    </tr>
    <?php 
            endforeach;
        } 
    ?>
</table>
