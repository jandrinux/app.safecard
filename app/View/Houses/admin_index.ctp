
<h1><?php echo __('Propiedades'); ?></h1>

<nav class="navbar navbar-inverse" role="navigation">
<div class="navbar-header">
	<?=$this->Html->link('Agregar',array('action'=>'add'), array('class'=>'navbar-brand'))?>
</div>
</nav>

<!-- filtros -->
<h3><?php echo __('Buscar Por Recinto'); ?></h3>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">

            <?php	echo $this->Form->create('House', array('class'=>'form-horizontal', 'role'=>'form')); ?>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Recinto') ?></label>
				<?php echo $this->Form->input('condo_id', array(
					'label'=>false, 
					'options'=>array(''=>'Todos')+$condos, // cambiar por variable del controlador
					'class'=>'form-control', 
					'div'=>array('class'=>'col-sm-6'),
					'selected' => isset($this->request->data['House']['condo_id']) ? $this->request->data['House']['condo_id']:@$this->Session->read('filter_condo_id')
				)); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <?php
				echo $this->Form->button(__('Consultar'),  array('class'=>'btn btn-orange', 'div'=>array('class'=>'form-group')));
            ?>
            </div>

            <?php echo $this->Form->end(); ?>

            </div>
        </div>
    </div>
</div>
<!-- filtros -->

<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>
<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
	<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('condo_id','Condominio - Colegio'); ?></th>
			<th><?php echo $this->Paginator->sort('name','Casa - Alumno'); ?></th>
			<th><?php echo $this->Paginator->sort('created','Creado'); ?></th>
			<th class="actions"><?php echo __('Actividad'); ?></th>
		</tr>
	</thead>
	<?php foreach ($houses as $house): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($house['Condo']['name'], array('controller' => 'condos', 'action' => 'view', $house['Condo']['id'])); ?>
		</td>
		<td><?php echo h($house['House']['name']); ?>&nbsp;</td>
		<td><?php echo h($house['House']['created']); ?>&nbsp;</td>
		<td class="actions">
			<div class="label label-success"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $house['House']['id']), array('class'=>'blue')); ?></div>
			<div class="label label-info"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $house['House']['id']), array('class'=>'green')); ?></div>
			<div class="label label-warning"><?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $house['House']['id']), array('class'=>'orange'), __('Seguro desea eliminar la vivienda %s?', $house['House']['name'])); ?></div>
		</td>
	</tr>
<?php endforeach; ?>
</table>
	
<div class="paging">
<?php
	echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
	echo $this->Paginator->numbers(array('separator' => ''));
	echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
?>
</div>
