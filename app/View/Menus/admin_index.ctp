
<h1><?php echo __('Menus'); ?></h1>



<nav class="navbar navbar-inverse" role="navigation">
<div class="navbar-header">
    <?=$this->Html->link('Agregar',array('action'=>'add'), array('class'=>'navbar-brand'))?>
</div>
</nav>

<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>

<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
<tr>
    <thead>
    <th><?php echo $this->Paginator->sort('id','#'); ?></th>
    <th><?php echo $this->Paginator->sort('parent_id','Menu padre'); ?></th>
    <th><?php echo $this->Paginator->sort('admin'); ?></th>
    <th><?php echo $this->Paginator->sort('name','Nombre'); ?></th>
    <th><?php echo $this->Paginator->sort('controller','Controlador'); ?></th>
    <th><?php echo $this->Paginator->sort('action','Accion'); ?></th>
    <th><?php echo $this->Paginator->sort('order','Orden'); ?></th>
    <th class="actions"><?php echo __('Actividad'); ?></th>
    </thead>
</tr>
<?php foreach ($menus as $menu): ?>
<tr>
	<td><?php echo h($menu['Menu']['id']); ?>&nbsp;</td>
	<td>
		<?php echo $menu['ParentMenu']['id']>0?$this->Html->link($menu['ParentMenu']['name'], array('controller' => 'menus', 'action' => 'view', $menu['ParentMenu']['id'])):'--'; ?>
	</td>
	<td><?php echo h($menu['Menu']['admin']); ?>&nbsp;</td>
	<td><?php echo h($menu['Menu']['name']); ?>&nbsp;</td>
	<td><?php echo h($menu['Menu']['controller']); ?>&nbsp;</td>
	<td><?php echo h($menu['Menu']['action']); ?>&nbsp;</td>
	<td><?php echo h($menu['Menu']['order']); ?>&nbsp;</td>
	<td class="actions">
            <div class="label label-success"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $menu['Menu']['id'])); ?></div>
            <div class="label label-info"><?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $menu['Menu']['id']), null, __('Esta seguro que desea eliminar # %s?', $menu['Menu']['id'])); ?></div>
	</td>
</tr>
<?php endforeach; ?>
</table>

<div class="paging">
<?php
	echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
	echo $this->Paginator->numbers(array('separator' => ''));
	echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
?>
</div>