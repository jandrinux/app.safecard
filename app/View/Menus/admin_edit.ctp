<h3>Editar Menu</h3>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
            <?php 
                echo $this->Form->create('Menu', array('class'=>'form-horizontal','role'=>'form'));
                echo $this->Form->input('id');
            ?>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Titular') ?></label> 
                <?php echo $this->Form->input('parent_id', array('class'=>'form-control', 'label'=>false, 'options'=>array(''=>'Seleccione')+$parentMenus, 'div'=>array('class'=>'col-sm-6')));?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Nombre') ?></label>
                <?php echo $this->Form->input('name', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
              
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Controlador') ?></label>    
                <?php echo $this->Form->input('controller', array('label'=>false,'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Accion') ?></label>
                <?php echo $this->Form->input('action', array('label'=>false,'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Orden') ?></label>
                <?php echo $this->Form->input('order', array('class'=>'form-control','label'=>false, 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Administrador') ?></label>
                <?php echo $this->Form->input('admin', array('label'=>false,'class'=>'checkbox', 'type'=>'checkbox', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <?php
                    echo $this->Html->link(__('Cancelar'), array('action'=>'index'),array('class'=>'btn btn-default')).'&nbsp;';
                    echo $this->Form->button(__('Editar'), array('class'=>'btn btn-orange', 'div'=>array('class'=>'form-group')));
                ?>
            </div>
            <?php echo $this->Form->end(); ?>
                
            </div>
        </div>
    </div>
</div>
