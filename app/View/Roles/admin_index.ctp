
	<h1><?php echo __('Roles'); ?></h1>
	

	<nav class="navbar navbar-inverse" role="navigation">
	<div class="navbar-header">
		<?=$this->Html->link('Agregar',array('action'=>'add'), array('class'=>'navbar-brand'))?>
	</div>
	</nav>
        
	<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>
        
	<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
	<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('id','#'); ?></th>
			<th><?php echo $this->Paginator->sort('name','Rol'); ?></th>
			<th><?php echo $this->Paginator->sort('code','Código'); ?></th>
			<th><?php echo $this->Paginator->sort('created','Creado'); ?></th>
			<th><?php echo $this->Paginator->sort('modified','Modificado'); ?></th>
			<th class="actions"><?php echo __('Actividad'); ?></th>
		</tr>
	</thead>
	<?php foreach ($roles as $role): ?>
	<tr>
		<td><?php echo h($role['Role']['id']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['name']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['code']); ?>&nbsp;</td>
		<td><?php echo h(date('d-m-Y h:i', strtotime($role['Role']['created']))); ?>&nbsp;</td>
		<td><?php echo h(date('d-m-Y h:i', strtotime($role['Role']['modified']))); ?>&nbsp;</td>
		<td class="actions">
                    <div class="label label-success"><?php echo $this->Html->link(__('Detalle'), array('action' => 'view', $role['Role']['id'])); ?></div>
                    <div class="label label-info"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $role['Role']['id'])); ?></div>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
