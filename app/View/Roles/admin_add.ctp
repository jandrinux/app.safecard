
<h3>Agregar Rol</h3>

<?php 
    echo $this->Form->create('Role', array('class'=>'form-horizontal','role'=>'form'));
    echo $this->Form->input('name', array('label'=>'Nombre rol', 'class'=>'medium'));
    echo $this->Form->input('code', array('label'=>'Código', 'class'=>'medium'));
    echo $this->Form->end(__('Agregar')); 
?>

