<div>
    <table class="table table-hover">
	<tr>
            <td><?php echo __('#'); ?></td>
            <td>
                <?php echo h($role['Role']['id']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Rol'); ?></td>
            <td>
                <?php echo h($role['Role']['name']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Creado'); ?></td>
            <td>
                <?php echo h(date('d-m-Y', strtotime($role['Role']['created']))); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Modificado'); ?></td>
            <td>
                <?php echo h(date('d-m-Y', strtotime($role['Role']['modified']))); ?>
                &nbsp;
            </td>
	</tr>
</div>
<div class="related">
	<h3><?php echo __('Usuarios relacionados'); ?></h3>
	<?php if (!empty($role['User'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered responsive">
	<thead>
            <tr>
		<th><?php echo __('#'); ?></th>
		<th><?php echo __('Nombre de usuario'); ?></th>
		<th><?php echo __('Activo'); ?></th>
		<th><?php echo __('Nombre'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Ultimo ingreso'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
        </thead>
	<?php
		$i = 0;
		foreach ($role['User'] as $user): ?>
		<tr>
			<td><?php echo $user['id']; ?></td>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['active']; ?></td>
			<td><?php echo $user['name'].' '.$user['father_surname'].' '.$user['mother_surname']; ?></td>
			<td><?php echo $user['email']; ?></td>
			<td><?php echo $user['last_login']; ?></td>
			<td class="actions">
                            <div class="label label-success"><?php echo $this->Html->link(__('Ver'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?></div>
                            <div class="label label-info"><?php echo $this->Html->link(__('Editar'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?></div>
                            <?php //echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), null, __('Are you sure you want to delete # %s?', $user['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
	<? else: ?>
	<center>No existen usuarios asociados a este rol</center>
	<?php endif; ?>
</div>
