
<h3><?php echo __('Editar Rol');?></h3>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
            
            
            <?php 
                echo $this->Form->create('Role', array('class'=>'form-horizontal','role'=>'form'));
                echo $this->Form->input('id');
            ?>
            
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Nombre') ?></label> 
                <?php echo $this->Form->input('name', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Código') ?></label> 
                <?php echo $this->Form->input('code', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label"></label> 
                <?php
                    echo $this->Html->link(__('Cancelar'), array('action'=>'index'), array('class'=>'btn btn-default')).'&nbsp;';
                    echo $this->Form->button(__('Editar'), array('class'=>'btn btn-orange'));
                ?>
            </div>
            <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

