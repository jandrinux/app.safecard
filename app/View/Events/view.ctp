<?php
    echo $this->Html->addCrumb('Eventos', array('action'=>'index'));
    echo $this->Html->addCrumb('Detalle evento');
?>

<h2><?php echo __('Evento'); ?></h2>
<table class="table table-hover responsive">
    <tr>
        <td><?php echo __('Casa-Depto'); ?></td>
        <td>
                <?php echo h($event['House']['name']); ?>
                &nbsp;
        </td>
    </tr>
    <tr>
        <td><?php echo __('Organizador'); ?></td>
        <td>
                <?php echo h($event['User']['name'].' '.$event['User']['father_surname']); ?>
                &nbsp;
        </td>
    </tr>
    <tr>
        <td><?php echo __('Asunto'); ?></td>
        <td>
                <?php echo h($event['Event']['subject']); ?>
                &nbsp;
        </td>
    </tr>
    <tr>
        <td><?php echo __('Descripción'); ?></td>
        <td>
                <?php echo nl2br($event['Event']['description']); ?>
                &nbsp;
        </td>
    </tr>
    <tr>
        <td><?php echo __('Desde'); ?></td>
        <td>
                <?php echo h(date('d M, Y H:i',strtotime($event['Event']['from']))); ?>
                &nbsp;
        </td>
    </tr>
    <tr>
        <td><?php echo __('Hasta'); ?></td>
        <td>
                <?php echo h(date('d M, Y H:i',strtotime($event['Event']['to']))); ?>
                &nbsp;
        </td>
    </tr>
    <tr>
        <td><?php echo __('Creado'); ?></td>
        <td>
                <?php echo h(date('d M, Y',strtotime($event['Event']['created']))); ?>
                &nbsp;
        </td>
    </tr>
    <tr>
        <td><?php echo __('Última modificación'); ?></td>
        <td>
                <?php echo h(date('d M, Y', strtotime($event['Event']['modified']))); ?>
                &nbsp;
        </td>
    </tr>
</table>

<div class="related">
<h3><?php echo __('Invitados'); ?></h3>
    <?php if (!empty($invitations)): ?>
    <table cellpadding = "0" cellspacing = "0" class="table table-bordered responsive">
    <thead>
        <tr>
            <th><?php echo __('Invitado'); ?></th>
            <th><?php echo __('Celular'); ?></th>
        </tr>
    </thead>
    <?php
        $i = 0;
        foreach ($invitations as $invitation):?>
        <tr>
            <td><?php echo $invitation['Contact']['name'].' '.$invitation['Contact']['father_surname']; ?></td>
            <td><?php echo $invitation['Contact']['mobile']; ?></td>
        </tr>
    <?php endforeach; ?>
    </table>
<?php else: ?>
    <div class="col-sm-12">
        <div class="panel panel-info" data-collapse="0">
            <div class="panel-heading"><div class="panel-title"><?php echo __('No tienes invitados a este evento')?></div></div>
            <div class="panel-body"><p><?php echo $this->Html->link(__('Comienza a invitar aquí'), array('action'=>'invite', $event['Event']['id'])); ?></p></div>
        </div>
    </div>
<?php endif; ?>

</div>
