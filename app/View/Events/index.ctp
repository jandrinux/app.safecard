
<h1><?php echo __('Eventos'); ?></h1>

<nav class="navbar navbar-inverse" role="navigation">
<div class="navbar-header">
	<?=$this->Html->link('Agregar',array('action'=>'add'), array('class'=>'navbar-brand'))?>
</div>
</nav>

<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>

<table cellpadding="0" cellspacing="0" class="table table-bordered responsive">
    <thead
        <tr>
            <th><?php echo $this->Paginator->sort('House.name','Casa-Depto');?>
            <th><?php echo $this->Paginator->sort('subject','Asunto'); ?></th>
            <th><?php echo $this->Paginator->sort('from','Desde'); ?></th>
            <th><?php echo $this->Paginator->sort('to','Hasta'); ?></th>
            <th><?php echo $this->Paginator->sort('created','Creado'); ?></th>
            <th class="actions"><?php echo __('Actividad'); ?></th>
        </tr>
    </thead>
<?php foreach ($events as $event): ?>
<tr>
    <td><?php echo h($event['House']['name']); ?>&nbsp;</td>
    <td><?php echo h($event['Event']['subject']); ?>&nbsp;</td>
    <td><?php echo h(date('d M, Y H:i',strtotime($event['Event']['from']))); ?>&nbsp;</td>
    <td><?php echo h(date('d M, Y H:i',strtotime($event['Event']['to']))); ?>&nbsp;</td>
    <td><?php echo h(date('d M, Y',strtotime($event['Event']['created']))); ?>&nbsp;</td>
    <td class="actions">
        <div class="label label-success"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $event['Event']['id'])); ?></div>
        <div class="label label-info"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $event['Event']['id'])); ?></div>
        <div class="label label-primary"><?php echo $this->Html->link(__('Invitar'), array('action'=>'invite',$event['Event']['id'])) ?></div>
        <div class="label label-warning"><?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $event['Event']['id']), null, __('Está seguro que desea eliminar evento "%s"?', $event['Event']['subject'])); ?></div>
    </td>
</tr>
<?php endforeach; ?>
</table>

<div class="paging">
<?php
        echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
?>
</div>
