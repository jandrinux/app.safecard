<script type="text/javascript">
$(function(){
	$(".per_day").hide();
	
	$("#EventPerDay").on('click', function(){
		if($(this).is(':checked')){
			$(".per_day").show();
			$(".lineal").hide('slow');
		} else {
			$(".per_day").hide('slow');
			$(".lineal").show();
		}
	})
})
</script>
<?php	
    $this->Html->addCrumb('<i class="entypo-home"></i>'.__('Eventos'), array('action'=>'index', 'admin'=>false), array('escape'=>false));
    $this->Html->addCrumb('Nuevo evento');
?>

<h3><?php echo __('Agregar evento'); ?></h3>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
            <?php echo $this->Form->create('Event', array('class'=>'form-horizontal','role'=>'form')); ?>
            
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Casa-Depto') ?></label>
            <?php echo $this->Form->input('house_id', array('label'=>false, 'type'=>'select', 'options'=>array(''=>'Seleccione')+$houses,'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
                
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Asunto') ?></label>
            <?php echo $this->Form->input('subject', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Descripción') ?></label>
            <?php echo $this->Form->input('description', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
            
			<div class="form-group">
			<label class="col-sm-3 control-label"><?=__('Permiso por días')?></label>
			<?php echo $this->Form->input('per_day', array('type'=>'checkbox', 'label'=>false, 'div'=>array('class'=>'col-sm-6')));?>
			</div>
			
			<div class="form-group per_day">
			<label class="col-sm-3 control-label"><?=__('Días') ?></label>
			<?php echo $this->Form->input('DaysEvent.day_id', array('label'=>false, 'options'=>$days, 'class'=>'select2','multiple'=>'multiple','div'=>array('class'=>'col-sm-6'))); ?>
			</div>
			
			<div class="form-group per_day" style="display: <?=@$this->request->data['Event']['per_day']?'':'none'?>">
			<label class="col-sm-3 control-label"><?=__('Horario') ?></label>
			<?php echo $this->Form->input('DaysEvent.hfrom', array(
                'label'=>false,
                'data-template'=>'dropdown',
                'data-show-seconds'=>false,
                'data-show-meridian'=>false,
                'data-minute-step'=>15,
                'type'=>'text', 
                'class'=>'form-control timepicker', 
                'div'=>array('class'=>'col-sm-2'))); ?>
			<?php echo $this->Form->input('DaysEvent.hto', array(
                'label'=>false,
                'data-template'=>'dropdown',
                'data-show-seconds'=>false,
                'data-show-meridian'=>false,
                'data-minute-step'=>15,
                'type'=>'text', 
				'value'=>date('h:i A',strtotime("+1 hour")),
                'class'=>'form-control timepicker', 
                'div'=>array('class'=>'col-sm-2'))); ?>
			</div>
			
			<!--<div class="form-group per_day">
			<label class="col-sm-3 control-label"><?=__('Horario') ?></label>
			
			</div>-->
			
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Desde') ?></label>
            <?php echo $this->Form->input('dfrom', array(
                'label'=>false,
                'type'=>'text', 
                'data-format'=>'dd-mm-yyyy',
                'data-start-date'=>'0d',
                'value'=>isset($this->request->data['Event']['dfrom']) ? date('d-m-Y', strtotime($this->request->data['Event']['dfrom'])):date('d-m-Y'),
                'class'=>'form-control datepicker', 
                'div'=>array('class'=>'col-sm-2'))); ?>
            
            <?php echo $this->Form->input('hfrom', array(
                'label'=>false,
                'data-template'=>'dropdown',
                'data-show-seconds'=>false,
                'data-show-meridian'=>false,
                'data-minute-step'=>15,
                'type'=>'text', 
                'class'=>'form-control timepicker lineal', 
                'div'=>array('class'=>'col-sm-2'))); ?>
            
            </div>
            
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Hasta') ?></label>
            
            <?php echo $this->Form->input('dto', array(
                'label'=>false,
                'type'=>'text', 
                'data-format'=>'dd-mm-yyyy',
                'data-start-date'=>'0d',
                'value'=>isset($this->request->data['Event']['dto']) ? date('d-m-Y', strtotime($this->request->data['Event']['dto'])):date('d-m-Y'),
                'class'=>'form-control datepicker', 
                'div'=>array('class'=>'col-sm-2'))); ?>
            
            <?php echo $this->Form->input('hto', array(
                'label'=>false,
                'data-template'=>'dropdown',
                'data-show-seconds'=>false,
                'data-show-meridian'=>false,
                'data-minute-step'=>15,
                'type'=>'text', 
				'value'=>date('h:i A',strtotime("+1 hour")),
                'class'=>'form-control timepicker lineal', 
                'div'=>array('class'=>'col-sm-2'))); ?>
            
            </div>
                
            <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <?php 
            echo $this->Html->link(__('Cancelar'), array('action'=>'index'),array('class'=>'btn btn-default')).'&nbsp;';
            echo $this->Form->button(__('Guardar'), array('class'=>'btn btn-orange'));
            ?>
            </div>
                
            <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
