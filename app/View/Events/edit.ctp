<?php	
    $this->Html->addCrumb('<i class="entypo-home"></i>Eventos', array('action'=>'index', 'admin'=>false), array('escape'=>false));
    $this->Html->addCrumb('Nuevo evento');
?>

<h3><?php echo __('Editar evento'); ?></h3>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">
            <?php 
                echo $this->Form->create('Event', array('class'=>'form-horizontal', 'role'=>'form')); 
                echo $this->Form->input('id');
                echo $this->Form->input('user_id', array('type'=>'hidden'));
            ?>
            
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Casa-Depto') ?></label>
            <?php echo $this->Form->input('house_id', array('label'=>false, 'type'=>'select', 'options'=>array(''=>'Seleccione')+$houses,'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>
                
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Asunto') ?></label>
            <?php echo $this->Form->input('subject', array('label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
            
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Descripción') ?></label>
            <?php echo $this->Form->input('description', array('label'=>false,'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
            </div>
                
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Desde') ?></label>
            <?php echo $this->Form->input('dfrom', array(
                'label'=>false,
                'type'=>'text', 
                'data-format'=>'dd-mm-yyyy',
                'data-start-date'=>'0d',
                'value'=>date('d-m-Y', strtotime($this->request->data['Event']['from'])),
                'class'=>'form-control datepicker', 
                'div'=>array('class'=>'col-sm-2'))); ?>
            
            <?php echo $this->Form->input('hfrom', array(
                'label'=>false,
                'data-template'=>'dropdown',
                'data-show-seconds'=>false,
                'data-show-meridian'=>false,
                'data-minute-step'=>15,
                'data-default-time'=>date('h:i A', strtotime($this->request->data['Event']['from'])),
                'type'=>'text', 
                'class'=>'form-control timepicker', 
                'div'=>array('class'=>'col-sm-2'))); ?>
            </div>
            
            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Hasta') ?></label>
            <?php echo $this->Form->input('dto', array(
                'label'=>false,
                'type'=>'text', 
                'data-format'=>'dd-mm-yyyy',
                'data-start-date'=>'0d',
                'value'=>date('d-m-Y', strtotime($this->request->data['Event']['to'])),
                'class'=>'form-control datepicker', 
                'div'=>array('class'=>'col-sm-2'))); ?>
            
            <?php echo $this->Form->input('hto', array(
                'label'=>false,
                'data-template'=>'dropdown',
                'data-show-seconds'=>false,
                'data-show-meridian'=>false,
                'data-minute-step'=>15,
                'data-default-time'=>date('h:i A', strtotime($this->request->data['Event']['to'])),
                'type'=>'text', 
                'class'=>'form-control timepicker', 
                'div'=>array('class'=>'col-sm-2'))); ?>
            </div>
            
            <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <?php
                echo $this->Html->link(__('Cancelar'), array('action'=>'index'), array('class'=>'btn btn-default')).'&nbsp;';
                echo $this->Form->button('Editar', array('class'=>'btn btn-orange'));
            ?>
            </div>
            <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
