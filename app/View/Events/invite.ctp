<?php 
    echo $this->Html->script('select2.min');
?>

<?php	
    $this->Html->addCrumb('<i class="entypo-home"></i>Eventos', array('action'=>'index', 'admin'=>false), array('escape'=>false));
    $this->Html->addCrumb('Invitaciones');
?>

<h3><?php echo __('Invitaciones');?></h3>

<div class="col-sm-6">
    <div class="panel panel-default" data-collapse="0">
        <div class="panel-heading"><div class="panel-title"><?php echo $event['Event']['subject'] ?></div></div>
        <div class="panel-body">
            <table class="table table-hover responsive">
                <tr>
                    <td><?php echo __('Casa-Depto'); ?></td>
                    <td>
                            <?php echo h($event['House']['name']); ?>
                            &nbsp;
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('Organizador'); ?></td>
                    <td>
                            <?php echo h($event['User']['name'].' '.$event['User']['father_surname']); ?>
                            &nbsp;
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('Asunto'); ?></td>
                    <td>
                            <?php echo h($event['Event']['subject']?$event['Event']['subject']:'--'); ?>
                            &nbsp;
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('Descripción'); ?></td>
                    <td>
                            <?php echo nl2br($event['Event']['description']?$event['Event']['description']:'--'); ?>
                            &nbsp;
                    </td>
                </tr>
				<?php if(!$event['Event']['per_day']): ?>
                <tr>
                    <td><?php echo __('Desde'); ?></td>
                    <td>
                            <?php echo h(date('d M, Y H:i',strtotime($event['Event']['from']))); ?>
                            &nbsp;
                    </td>
                </tr>
                <tr>
                    <td><?php echo __('Hasta'); ?></td>
                    <td>
                            <?php echo h(date('d M, Y H:i',strtotime($event['Event']['to']))); ?>
                            &nbsp;
                    </td>
                </tr>
				
				<?php else: ?>
				<tr>
					<td><?=__('Días permitidos')?></td>
					<td>
				<?php				
					foreach($event['Day'] as $key=>$day){
						echo $day['name'].' ('.date('H:i', strtotime($day['DaysEvent']['from'])).' - '.date('H:i', strtotime($day['DaysEvent']['to'])).')<br>';
					}
				endif; ?>
					</td>				
				</tr>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-body">
                <?php echo $this->Form->create('Event', array('class'=>'form-horizontal','role'=>'form')); ?>
                
                <div class="form-group">
                <label class="col-sm-3 control-label"><?=__('Contactos') ?></label>
                <?php echo $this->Form->input('contact_id', array('label'=>false, 'class'=>'select2','multiple'=>'multiple','div'=>array('class'=>'col-sm-6'))); ?>
                </div>
                
                <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <?php 
                    echo $this->Html->link(__('Cancelar'), array('action'=>'index'), array('class'=>'btn btn-default')).'&nbsp;';
                    echo $this->Form->button(__('Invitar'), array('class'=>'btn btn-orange'));
                ?>
                </div>
                
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>