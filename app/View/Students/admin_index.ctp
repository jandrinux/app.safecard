
<h1><?php echo __('Alumnos'); ?></h1>

<nav class="navbar navbar-inverse" role="navigation">
<div class="navbar-header">
	<?php //echo $this->Html->link('Agregar',array('action'=>'add'), array('class'=>'navbar-brand'))?>
	<?php echo $this->Html->link('Carga masiva',array('action'=>'bulk'), array('class'=>'navbar-brand'))?>
	<?php echo $this->Html->link('Asociar apoderados', array('action'=>'parent_bulk'), array('class'=>'navbar-brand')); ?>
</div>
</nav>

<!-- filtros -->
<h3><?php echo __('Buscar Alumno'); ?></h3>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-body">

            <?php	echo $this->Form->create('Student', array('class'=>'form-horizontal', 'role'=>'form')); ?>

            <!--<div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Recinto') ?></label>
				<?php echo $this->Form->input('condo_id', array('label'=>false, 'options'=>array(''=>'Seleccione')+$condos_usuario,'class'=>'form-control', 'div'=>array('class'=>'col-sm-6'))); ?>
            </div>-->

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Colegio') ?></label>
				<?php echo $this->Form->input('condo_id', array(
					'label'=>false, 
					'options'=>array(''=>'Todos')+$colegios, // cambiar por variable del controlador
					'class'=>'form-control', 
					'div'=>array('class'=>'col-sm-6'),
					'selected' => isset($this->request->data['Student']['condo_id']) ? $this->request->data['Student']['condo_id']:@$this->Session->read('filter_condo_id')
				)); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"><?=__('Nombre') ?></label>
                <?php echo $this->Form->input('search_name', array('label'=>false, 'class'=>'form-control', 'value' => isset($this->request->data['Student']['search_name']) ? $this->request->data['Student']['search_name']:@$this->Session->read('filter_search_name'),'div'=>array('class'=>'col-sm-6'))); ?>
            </div>

            <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <?php
				echo $this->Form->button(__('Consultar'),  array('class'=>'btn btn-orange', 'div'=>array('class'=>'form-group')));
            ?>
            </div>

            <?php echo $this->Form->end(); ?>

            </div>
        </div>
    </div>
</div>
<!-- filtros -->

	<p class="count"><?php echo $this->Paginator->counter(array('format' => __('{:start} - {:end} de {:count}'))); ?></p>
	
	<table cellpadding="0" cellspacing="0" class="table table-bordered table-responsive">
	<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('name','Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('mobile','Celular'); ?></th>
			<th><?php echo $this->Paginator->sort('condo_id','Recinto'); ?></th>
		</tr>
	</thead>
	<?php foreach ($students as $student): ?>
	<tr>
		<td><?php echo h($student['Student']['full_name']); ?></td>
		<td><?php echo h($student['Student']['student_mobile']); ?>&nbsp;</td>
		<td><?php echo $student['School']['name']; ?></td>
	</tr>
	<?php endforeach; ?>
	</table>
	
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('siguiente') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
