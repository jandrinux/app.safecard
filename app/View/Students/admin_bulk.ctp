<?php
    //echo $this->Html->script('jquery.Rut');
    $this->Html->addCrumb('<i class="entypo-users"></i>Alumnos', array('action'=>'index', 'admin'=>true), array('escape'=>false));
    $this->Html->addCrumb('Carga masiva');
?>

<h3><?php echo __('Carga masiva de alumnos'); ?></h3>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-body">
				<?php echo $this->Form->create('Student', array('type'=>'file','class'=>'form-horizontal')); ?>
				
				<div class="form-group">
	            <label class="col-sm-3 control-label"><?=__('Pais') ?></label>
					<?php echo $this->Form->input('country_id', array('label'=>false, 'class'=>'form-control', 'selected'=>$country_selected, 'div'=>array('class'=>'col-sm-6'))); ?>
	            </div>

				<div class="form-group">
				<label class="col-sm-3 control-label"><?=__('Recinto') ?></label>
					<?php echo $this->Form->input('condo_id', array('type'=>'select', 'label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
				</div>

				<div class="form-group">
				<label class="col-sm-3 control-label"><?=__('Archivo') ?></label>
					<?php echo $this->Form->input('file', array('type'=>'file', 'label'=>false, 'class'=>'form-control', 'div'=>array('class'=>'col-sm-6')));?>
				</div>
			
				<div class="form-group">
	            <label class="col-sm-4 control-label"></label>
	            <?php
					echo $this->Html->link(__('Cancelar'),  array('action'=>'index'), array('class'=>'btn btn-default', 'div'=>array('class'=>'form-group'))).'&nbsp;';
					echo $this->Form->button(__('Guardar'),  array('class'=>'btn btn-orange', 'div'=>array('class'=>'form-group')));

	            ?>
	            </div>

	            <?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>

