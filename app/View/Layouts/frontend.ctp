<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('_dev', Configure::read('AppName'));
?>
<!DOCTYPE html>
<html>
<head>
	<?php //echo $this->Html->charset(); ?>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="description" content="Panel Administracion SafeCard" />
	<meta name="author" content="" />
	
	<title>
		<?php echo $cakeDescription ?>
		<?php //echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		
		// echo $this->Html->css('admin_style');
		
		echo $this->Html->css('jquery-ui-1.10.3.custom.min');
		echo $this->Html->css('font-icons/entypo/entypo');
		echo $this->Html->css('bootstrap');
		echo $this->Html->css('neon-core');
		echo $this->Html->css('neon-theme');
		echo $this->Html->css('admin_style');
		echo $this->Html->css('neon');
		echo $this->Html->css('custom');
		//echo //$this->Html->css('admin_style');
		
		echo $this->Html->script('jquery-1.9.1');
		echo $this->Html->script('jquery-1.9.1.min');
		// echo $this->Html->script('jquery-ui-1.10.0.custom.min');
		echo $this->Html->script('jquery-1.11.0.min');
	?>
		<!--[if lt IE 9]>
	<?	echo $this->Html->script('ie8-responsive-file-warning'); ?>
		<![endif]--> 
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	<?php
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>

<!--<body class="gradiente">-->
<body class="page-body body-download" data-url="http://demo.neontheme.com" >
<div class="wrap">

	<!--<div class="site-header-container container" style="background: #303641; margin: 0; width: 100%;">
		<div class="row">
			<div class="col-md-12">	
				<header class="site-header">
					<section class="site-logo">
						<?=$this->Html->link($this->Html->image('img_logo.png', array('class'=>'fleft')),array('controller'=>'users', 'action'=>'login','admin'=>false), array('escape'=>false))?>
					</section>
					<nav class="site-nav">
						<ul class="main-menu hidden-xs" id="main-menu">
							<li>
							<?//=$this->Html->image('apps.png', array('class'=>'fright'));?>
							</li>
						</ul>
					</nav>
				</header>
			</div>
		</div>
	</div>-->

	<div class="site-header-container container" style="background: #303641; padding: 15px; margin: 0; width: 100%;">
		<?php echo $this->Html->image('img_logo.png', array('class'=>'img-responsive pull-left')); ?>
		<?php echo $this->Html->image('apps.png', array('class'=>'pull-right'));?>
	</div>
	
	<div class="page-container">
		<div class="main-content">
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>
		</div>

		
	</div>
	
		
	<!-- Bottom Scripts -->
	<?php echo $this->Html->script('main-gsap'); ?>
	<?php //echo $this->Html->script('jquery-ui-1.10.3.minimal.min'); ?>
	<?php echo $this->Html->script('bootstrap'); ?>
	<?php echo $this->Html->script('joinable'); ?>
	<?php echo $this->Html->script('resizeable'); ?>
	<?php echo $this->Html->script('neon-api'); ?>
	<?php echo $this->Html->script('jquery-jvectormap-1.2.2.min'); ?>
	<?php echo $this->Html->script('jquery-jvectormap-europe-merc-en'); ?>
	<?php echo $this->Html->script('jquery.sparkline.min'); ?>
            
	<?php echo $this->Html->script('bootstrap-datepicker'); ?>
	<?php echo $this->Html->script('bootstrap-timepicker'); ?>
	<?php echo $this->Html->script('daterangepicker'); ?>
            
	<?php //echo $this->Html->script('/vendor/d3.v3'); ?>
	<?php //echo $this->Html->script('rickshaw/rickshaw.min'); ?>
	<?php echo $this->Html->script('raphael-min'); ?>
	<?php echo $this->Html->script('morris.min'); ?>
	<?php echo $this->Html->script('toastr'); ?>
	<?php //echo $this->Html->script('neon-login'); ?>
	<?php echo $this->Html->script('jquery.peity.min'); ?>
	<?php //echo $this->Html->script('neon-charts'); ?>
	<?php echo $this->Html->script('fileinput'); ?>
	<?php echo $this->Html->script('jquery.form.min'); ?>
	<?php echo $this->Html->script('neon-custom'); ?>
	<?php echo $this->Html->script('neon-demo'); ?>
            
	<?php echo $this->Html->css('../js/select2-bootstrap'); ?>
	<?php echo $this->Html->css('../js/select2'); ?>
	<?php echo $this->Html->script('select2.min'); ?>		
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60599521-1', 'auto');
  ga('send', 'pageview');

</script>
<!--<footer class="main">
		<?php //echo $this->Html->image('img_Footer.jpg', array('class'=>'img responsive center-block')); ?>
	</footer>-->
</body>
</html>
