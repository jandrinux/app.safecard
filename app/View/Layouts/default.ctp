<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('_dev', Configure::read('AppName'));
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>
		<?php //echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('admin_style');
		echo $this->Html->script('jquery-1.9.1');
		echo $this->Html->script('jquery-1.9.1.min');
		echo $this->Html->script('jquery-ui-1.10.0.custom.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="headers">
		<!--<h1><?php //echo $this->Html->link($cakeDescription, 'http://cakephp.org'); ?></h1>-->
	</div>
	<div id="wrapper">
		
		<div class="nav">
		<?if($this->Session->read('Auth.User.id') > 0):?>
			<ul>
				<li><?=$this->Html->link("Mi Perfil", array('controller'=>'users', 'action'=>'view', $this->Session->read('Auth.User.id')))?></li>
			</ul>
		<?endif;?>
		</div>
		

		<?php echo $this->Session->flash(); ?>
		<div id="container">
		<?php echo $this->fetch('content'); ?>
		</div>
	</div>
	<!--<div id="footer">
		
	</div>-->
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
