<?php  
App::uses('AppController', 'Controller');
App::import('Vendor', 'AES');
App::uses('AuthComponent', 'Controller/Component');

class WsMobileController extends AppController{ 
	public $uses = array('Invitation', 'User', 'Owner', 'Contact', 'HousesUser', 'House','Relation','Condo');
	 
	public $components = array('Soap'); 
	
	function beforeFilter() {
		$this->Auth->allow();
		$this->Soap->__settings = array( 
            'wsdl' => "WSDLMobile", 
            'wsdlAction' => 'wsdl', 
            'prefix' => 'soap', 
            'action' => array('service'), 
        );  
		parent::beforeFilter();
	}
	
	public function soap_wsdl(){ 
		//will be handled by SoapComponent 
	} 

	public function soap_service(){ 
		//will be handled by SoapComponent 
	} 
	
	/*
	* Creación de usuario
	*/
	public function soap_AddUser($data){
		$obj = new stdClass(); 
		$obj->result = "NACK";
		
		$this->User->create();
		$role = $this->User->Role->findByCode('PROP');
		$this->request->data['role_id'] = $role['Role']['id'];
		
		$country = $this->User->Country->findByIso2($data->iso2);
		
		if(!empty($country)){
			$this->request->data['country_id'] = $country["Country"]["id"];
			
			$this->request->data['mobile'] = $data->mobile;
			$this->request->data['name'] = $data->name;
			$this->request->data['father_surname'] = $data->father_surname;
			$this->request->data['password'] = $data->pin;
			$this->request->data['device'] = $data->device;
			
			if ($this->User->save($this->request->data)) {
				//Merge entre propietarios cargados por Administradora y usuarios registrados
				$this->loadModel('Owner');
				$owners = $this->Owner->find('all', array('conditions'=>array('Owner.mobile'=>$this->User->field('mobile'))));
				$user = $this->User->read(null,$this->User->id);
				
				if(!empty($owners)){
					$houses = array();
					$i = 0;
					foreach($owners as $key=>$owner){
						$this->Owner->House->HousesUser->create();
						$houses[$i]['HousesUser'] = array(
							'user_id'=>$this->User->id,
							'house_id'=>$owner['Owner']['house_id'],
							'owner'=>1,
							'active'=>1
						);
						$i++;
					}
					if($this->Owner->House->HousesUser->saveMany($houses)){
						$obj->result = "ACK";
					} else {
						$this->User->deleteAll(array('User.id'=>$this->User->id));
						$obj->result = "NACK";
					}
				} else {
					$obj->result = "ACK";
				}
			}
		}
		
		return $obj;
	}
	
	/*
	* Detalle de usuario y/o contacto
	*/
	public function soap_DataContact($data){
		$obj = new stdClass();
		
		$contacto = $this->User->findByMobile($data->mobile);
		
		if(!empty($contacto)){
			$obj->name = $contacto['User']['name'];
			$obj->father_surname = $contacto['User']['father_surname'];
			$obj->mobile = $contacto['User']['mobile'];
			$obj->email = $contacto['User']['email']?$contacto['User']['email']:"NULL";
			$obj->image = $contacto['User']['image'];
			
			$houses = $this->HousesUser->find('all', array(
				'conditions'=>array(
					'HousesUser.user_id'=>$contacto['User']['id'],
					'HousesUser.active' => 1
			)));
			
			$houses_list = array();
			foreach($houses as $key=>$house){
				
				$house_ = $this->House->read(null,$house['HousesUser']['house_id']);
				
				$invitation = $this->Invitation->find('first', array('conditions'=>array(
					'Invitation.organizer_id' => $contacto['User']['id'],
					'Invitation.guest_id' => $contacto['User']['id'],
					'Invitation.house_id' => $house_['House']['id'],
					'OR'=>array(
						array('Invitation.relation_id' => 1),
						array('Invitation.relation_id'=>2)
					),
					'status' => 1
				)));
				
				$aes = $invitation['Invitation']['aes']?$invitation['Invitation']['aes']:"NULL";
				
				$houses_list['houses'][$key] = array("id"=>$house_["House"]["id"], "house_name"=>$house_["House"]["name"], "condo_name"=>$house_["Condo"]["name"], "aes"=>$aes);
			}
			
			$obj->houses = json_encode($houses_list);
		} else {
			$obj->name = $obj->father_surname = $obj->mobile = $obj->email = $obj->image = $obj->houses = '';
		}
		
		return $obj;
	}
	
	/*
	* Invitaciones recibidas
	*/
	public function soap_InvitationsReceived($data){
		$obj = new stdClass();
		$invitations_received = array();
		$invitations = array();
		
		$guest = $this->User->findByMobile($data->mobile_guest);
		
		if($data->mobile_guest != ""){		
			$invitations = $this->Invitation->find("all",array(
			'conditions'=>array(
				'OR'=>array(
					array('Invitation.guest_id' => ($guest['User']['id']>0)?$guest['User']['id']:'NULL'),
					array('Invitation.mobile' => $data->mobile_guest)
				),
				'Invitation.sent_mobile' => 0,
				'Event.to >= '=> date('Y-m-d H:i:s'),
				'NOT'=>array(
					'Invitation.relation_id' => array(1,2)
				)
			)));
		}
		
		if(!empty($invitations)){
			foreach($invitations as $key=>$invitation){	
				$invitations_received['invitations'][$key] = array(
						'id' => $invitation['Invitation']['id'],
						'organizer_name' => $invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname'],
						'organizer_mobile' => $invitation['Organizer']['mobile'],
						'start_date' => $invitation['Event']['from'],
						'end_date' => $invitation['Event']['to'],
						'condo_name' => $invitation['Condo']['name'],
						'condo_address'=>$invitation['Condo']['address'],
						'house_name' => $invitation['House']['name'],
						'code' => $invitation['Invitation']['code'],
						'aes' => $invitation['Invitation']['aes'],
						'status' => $invitation['Invitation']['status'],
						'lat' => $invitation['Condo']['lat'],
						'lon' => $invitation['Condo']['long'],
						'created' => $invitation['Invitation']['created']
				);
			}
		}
		
		$obj->invitations_received = json_encode($invitations_received);
		return $obj;
	}
	
	
	/*
	* Invitaciones enviadas
	*/
	public function soap_InvitationsSent($data){
		$obj = new stdClass();
		$invitations_sent = array();
		
		$organizer = $this->User->findByMobile($data->mobile_organizer);
		if(!empty($organizer)){
		
			$invitations = $this->Invitation->find("all",array(
			'conditions'=>array(
				'OR'=>array(
					'Invitation.requested' => 0,
					'AND'=>array(
						'Invitation.requested' => 1, 
						'Invitation.status'=>1
					)
				),
				'Invitation.organizer_id'=>$organizer['User']['id'],
				'Invitation.sent_mobile' => 0,
				'Event.to >= '=> date('Y-m-d H:i:s'),
				'NOT'=>array(
					'Invitation.relation_id' => array(1,2)
				)
			)));
			
			if(!empty($invitations)){
				foreach($invitations as $key=>$invitation){	
					$guest_name = '';
					$guest_mobile = $invitation['Invitation']['mobile']?$invitation['Invitation']['mobile']:'';
					if(!empty($invitation['Guest']['mobile'])){
						$guest_name = $invitation['Guest']['name'].' '.$invitation['Guest']['father_surname'];
						$guest_mobile = $invitation['Guest']['mobile'];
					} else {
						$guest_name = $invitation['Contact']['name'];
						$guest_mobile = $invitation['Contact']['mobile'];
					}
					
					$invitations_sent['invitations'][$key] = array(
							'id' => $invitation['Invitation']['id'],
							'guest_name' => $guest_name,
							'guest_mobile' => $guest_mobile,
							'start_date' => $invitation['Event']['from'],
							'end_date' => $invitation['Event']['to'],
							'condo_name' => $invitation['Condo']['name'],
							'condo_address'=>$invitation['Condo']['address'],
							'house_name' => $invitation['House']['name'],
							'code' => $invitation['Invitation']['code'],
							'aes' => $invitation['Invitation']['aes'],
							'status' => $invitation['Invitation']['status'],
							'lat' => $invitation['Condo']['lat'],
							'lon' => $invitation['Condo']['long'],
							'created' => $invitation['Invitation']['created']
					);
				}
			}
		}
		
		$obj->invitations_sent = json_encode($invitations_sent);
		return $obj;
	}
	
	/*
	* Invitaciones solicitadas
	*/
	public function soap_InvitationsRequested($data){
		$obj = new stdClass();
		$invitations_requested = array();
		
		$organizer = $this->User->findByMobile($data->mobile_organizer);
		
		if(!empty($organizer)){		
			$invitations = $this->Invitation->find("all",array('conditions'=>array(
				'Invitation.organizer_id'=>$organizer['User']['id'],
				'Invitation.sent_mobile' => 0,
				'Invitation.requested' => 1,
				'Invitation.status' => 0,
				'Event.to >= '=> date('Y-m-d H:i:s')
			)));
			
			if(!empty($invitations)){
				foreach($invitations as $key=>$invitation){	
					
					$invitations_requested['invitations'][$key] = array(
						'id' => $invitation['Invitation']['id'],
						'guest_name' => $invitation['Guest']['name'].' '.$invitation['Guest']['father_surname'],
						'guest_mobile' => $invitation['Guest']['mobile'],
						'start_date' => $invitation['Event']['from'],
						'end_date' => $invitation['Event']['to'],
						'condo_name' => $invitation['Condo']['name'],
						'condo_address'=>$invitation['Condo']['address'],
						'house_name' => $invitation['House']['name'],
						'code' => $invitation['Invitation']['code'],
						'aes' => $invitation['Invitation']['aes'],
						'status' => $invitation['Invitation']['status'],
						'lat' => $invitation['Condo']['lat'],
						'lon' => $invitation['Condo']['long'],
						'created' => $invitation['Invitation']['created']
					);
				}
			}
		}
		$obj->invitations_requested = json_encode($invitations_requested);
		return $obj;
	}
	
	/*
	* Creación de invitación
	*/
	public function soap_AddInvitations($data){
		$obj = new stdClass();
		
		$this->loadModel('Day');
		$this->loadModel('DaysEvent');
		$event['Event']['subject'] = $data->subject;
		
		$house = $this->House->findById($data->house_id);
			
		if($data->repeat){
			
			$days = explode(',', $data->days);
			sort($days);
			$actual_day = date('w');
			
			$day_name = "";
			$flag = false;
			
			if(in_array($actual_day, $days)){
				$event['Event']['from'] = date('Y-m-d H:i:s', strtotime($data->start_date." ".$data->start_time));
				$event['Event']['to'] = date('Y-m-d H:i:s', strtotime($data->start_date." ".$data->end_time));
			} else {
				foreach($days as $key=>$day){
					if($day > $actual_day){
						$day_name = getDayName($day);
						$flag = true;
					}
					if($flag) break;
				}
				
				if($day_name == ""){
					$day_name = getDayName($days[0]);
				}
				
				$event['Event']['from'] = date('Y-m-d H:i:s', strtotime("next $day_name ".$data->start_time, strtotime($data->start_date)));
				$event['Event']['to'] = date('Y-m-d H:i:s', strtotime("next $day_name ".$data->end_time, strtotime($data->start_date)));
			}
			
		} else {
			$event['Event']['from'] = date('Y-m-d H:i:s', strtotime($data->start_date.' '.$data->start_time));
			$event['Event']['to'] = date('Y-m-d H:i:s', strtotime($data->end_date.' '.$data->end_time));
		}
			
		$event['Event']['house_id'] = $data->house_id;
		
		$organizer = $this->User->findByMobile($data->mobile_organizer);
		$event['Event']['user_id'] = $organizer['User']['id'];
		
		$obj->result = "NACK";
		
		if($this->Invitation->Event->save($event)){
			
			//Guardar días en que evento se repetirá semanalmente
			foreach($days as $key=>$day){
				$dia = $this->Day->findByCode($day);
				$data_day_event[$key]['DaysEvent']['event_id'] = $this->Invitation->Event->id;
				$data_day_event[$key]['DaysEvent']['day_id'] = $dia['Day']['id'];
				$data_day_event[$key]['DaysEvent']['from'] = $data->start_time;
				$data_day_event[$key]['DaysEvent']['to'] = $data->end_time;
			}
			
			$this->DaysEvent->saveMany($data_day_event);
				
			//$condo = $this->Invitation->House->getCondominioByHouse($event['Event']['house_id']);
			$house = $this->Invitation->House->findById($data->house_id);
			$mobile_guests = explode(",", $data->mobile_guests);
			$name_guests = explode(",", $data->name_guests);
			
			foreach($mobile_guests as $key=>$mobile_guest){
			
				$mobile_guest = str_replace(' ','',$mobile_guest);
				
				/* Se registra contacto para disponibilidad en web */
				$contacto = $this->Contact->find('first', array('conditions'=>array(
					'Contact.mobile' => $mobile_guest,
					'Contact.organizer_id' => $this->Invitation->Event->field('user_id')
				)));
				
				if(empty($contacto)){
					$contacto = array(
						'organizer_id' => $this->Invitation->Event->field('user_id'),
						'name' => $name_guests[$key],
						'mobile' => $mobile_guest
					);
					$this->Contact->create();
					$this->Contact->save($contacto);
					$invitation[$key]['Invitation']['contact_id'] = $this->Contact->id;
				} else {
					$invitation[$key]['Invitation']['contact_id'] = $contacto['Contact']['id'];
				}
				
				$invitado = $this->User->findByMobile($mobile_guest);
				
				$invitation[$key]['Invitation']['accessed'] = date('y').str_pad(1,9,"0",STR_PAD_LEFT);
				$invitation[$key]['Invitation']['organizer_id'] = $this->Invitation->Event->field('user_id');
				$invitation[$key]['Invitation']['house_id'] = $data->house_id;
				$invitation[$key]['Invitation']['condo_id'] = $house['Condo']['id'];
				$invitation[$key]['Invitation']['event_id'] = $this->Invitation->Event->id;
				$invitation[$key]['Invitation']['mobile'] = $mobile_guest;
				$invitation[$key]['Invitation']['relation_id'] = $data->relation_id;
				$invitation[$key]['Invitation']['repeat'] = $data->repeat ? $data->repeat:0;
				
				if($data->repeat){
					$invitation[$key]['Invitation']['repeat_from'] = date('Y-m-d', strtotime($data->start_date));
					$invitation[$key]['Invitation']['repeat_to'] = date('Y-m-d', strtotime($data->end_date));
				}
				
				if(!empty($invitado)){
					$invitation[$key]['Invitation']['guest_id'] = $invitado['User']['id'];	
					$invitation[$key]['Invitation']['manual'] = 0;	
				} else {
					//Envio SMS con clave de 4 dígitos
					$pin = rand(1000,9999);
					$invitation[$key]['Invitation']['manual'] = 1;	
					$invitation[$key]['Invitation']['pin'] = $pin;
					/*$ch = curl_init();
					//extract data from the post
					extract($_POST);
					$text = $organizer['User']['name'].' te ha realizado una invitacion. Condominio: '.$house['Condo']['name'].' - '.$house['House']['name'].'. Pin acceso: '.$pin;
					
					//set POST variables
					$url = 'http://sms.sistek.cl/sms.asmx/Sms';
					
					$fields = array(
						'id' => '60673',
						'text' => $text,
						'to_number' => str_replace('+','',$mobile_guest),
						'username' => '4634738',
						'password' => '3238183',
						'type'=>'SMS'
					);
					
					$fields_string = http_build_query($fields);
					
					//set the url, number of POST vars, POST data
					curl_setopt($ch,CURLOPT_URL, $url);
					curl_setopt($ch,CURLOPT_POST, count($fields));
					curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

					//execute post
					$result = curl_exec($ch);

					//close connection
					curl_close($ch);*/
				}
				
			}
			
			if($this->Invitation->saveMany($invitation)){
				foreach($this->Invitation->inserted_ids as $key=>$invitation_id){
					$this->Invitation->saveCode($invitation_id);					
					$this->sendNotification($invitation_id,'create');
					$inv = $this->Invitation->read(null, $invitation_id);
				}
				$obj->aes = $inv['Invitation']['aes'];
				$obj->result = "ACK";		
			}
		}
		
		return $obj;
	}
	
	/*
	* Solicitar Invitacion
	*/
	public function soap_RequestInvitation($data){
		$obj = new stdClass();
		
		$event['Event']['subject'] = $data->subject;
		$event['Event']['from'] = date('Y-m-d H:i:s', strtotime($data->start_date.' '.$data->start_time));
		$event['Event']['to'] = date('Y-m-d H:i:s', strtotime($data->end_date.' '.$data->end_time));
		$event['Event']['house_id'] = $data->house_id;
		
		$organizer = $this->User->findByMobile($data->mobile_organizer);
		$event['Event']['user_id'] = $organizer['User']['id'];
		
		$obj->result = "NACK";
		
		if($this->Invitation->Event->save($event)){
			$house = $this->Invitation->House->findById($data->house_id);
			// $name_guests = explode(",", $data->name_guest);
			
			$guest = $this->User->findByMobile($data->mobile_guest);
			
			$invitation['Invitation']['organizer_id'] = $this->Invitation->Event->field('user_id');
			$invitation['Invitation']['house_id'] = $data->house_id;
			$invitation['Invitation']['condo_id'] = $house['Condo']['id'];
			$invitation['Invitation']['event_id'] = $this->Invitation->Event->id;
			$invitation['Invitation']['guest_id'] = $guest['User']['id'];
			//$invitation['Invitation']['relation_id'] = $data->relation_id;
			$invitation['Invitation']['requested'] = 1;
			$invitation['Invitation']['status'] = 0;
			
			if($this->Invitation->save($invitation)){
				$this->Invitation->saveInvalidCode($this->Invitation->id);				
				$this->sendNotification($this->Invitation->id,'request');
				$obj->result = "ACK";
			}
		}
		
		return $obj;
	}
	
	public function soap_UpdatePin($data){
		$obj = new stdClass();
		$obj->out = "NACK";
		
		$old_pin = AuthComponent::password($data->old_pin);
		
		$user = $this->User->find('first', array('conditions'=>array('password'=>$old_pin, 'mobile'=>$data->mobile)));
		
		if(!empty($user)){
			$this->User->id = $user['User']['id'];
			$this->User->read(null,$this->User->id);
			if($this->User->saveField('password', $data->new_pin)){
				$obj->out = "ACK";
			}
		}
		
		return $obj;
	}
	
	public function soap_RevokeInvitation($data){
		$obj = new stdClass();
		$obj->result = "NACK";
		
		$this->Invitation->id = $data->invitation_id;
		$invitation = $this->Invitation->read(null, $this->Invitation->id);
		if ($this->Invitation->exists($data->invitation_id)) {
			if($this->Invitation->saveField('status', -1)){
				$this->Invitation->saveInvalidCode($data->invitation_id);
				$obj->result = "ACK";
			}
		} else {
			$obj->result  = "NACK";
		}
		
		return $obj;
	}
	
	public function soap_AcceptInvitation($data){
		$obj = new stdClass();
		$obj->result = "NACK";
		
		$this->Invitation->id = $data->invitation_id;
		$invitation = $this->Invitation->read(null, $this->Invitation->id);
		if ($this->Invitation->exists($data->invitation_id)) {
			if($this->Invitation->saveField('status', 1)){
				$this->Invitation->saveCode($data->invitation_id);
				$this->sendNotification($this->Invitation->id,'accept');
				$obj->result = "ACK";
			}
		} else {
			$obj->result  = "NACK";
		}
		
		return $obj;
	}
	
	public function soap_Access($data){	
		$obj = new stdClass();
		$obj->access_code = "NACK";
		
		$user = $this->User->findByMobile($data->mobile);
		
		if(!empty($user)){
			$invitation = $this->Invitation->find('first', array('conditions'=>array(
				'Invitation.organizer_id' => $user['User']['id'],
				'Invitation.guest_id' => $user['User']['id'],
				'Invitation.house_id' => $data->house_id,
				'OR'=>array(
					array('Invitation.relation_id' => 1),
					array('Invitation.relation_id'=>2)
				),
				'status' => 1
			)));
			
			$obj->access_code = $invitation['Invitation']['aes']?$invitation['Invitation']['aes']:"NULL";
		}
		
		return $obj;
	}
	
	public function soap_EditProfile($data){
		$obj = new stdClass();
		$obj->result = "NACK";
		
		$user = $this->User->findByMobile($data->mobile);
		if(!empty($user)){
			$this->User->id = $user['User']['id'];
			$this->User->read(null, $this->User->id);
			
			$this->User->saveField('name',$data->name);
			$this->User->saveField('father_surname',$data->last_name);
			$this->User->saveField('email',$data->email);
			
			$obj->result = "ACK";
		}
		
		return $obj;
	}
	
	public function soap_RegisterDevice($data){
		$obj = new stdClass();
		$obj->result = "NACK";
		$user = $this->User->findByMobile($data->mobile);
		if(!empty($user)){
			
			$this->User->read(null, $user['User']['id']);
			
			$this->User->saveField('OS',$data->os);
			$this->User->saveField('full_device',$data->full_device);
			
			$obj->result = "ACK";
		}
		
		return $obj;
	}
	
	
	public function soap_Logs($data){	
		$obj = new stdClass();
		$obj->logs = 'NACK';
		
		$user = $this->User->findByMobile($data->mobile);
		if(!empty($user)){
			
			$houses = $this->HousesUser->find('all', array(
				'conditions'=>array(
					'HousesUser.user_id'=>$user['User']['id'],
					'HousesUser.active' => 1
				)
			));
			
			$houses_list = array();
			foreach($houses as $key=>$house){
				$houses_list[] = $house["HousesUser"]["house_id"];
			}
			$logs = $this->Invitation->Log->find('all', array(
				'conditions'=>array(
					'Invitation.house_id'=>$houses_list,
					'Log.created >=' => date('Y-m-d 00:00:00', strtotime('-2 days'))
				)
			));
			
			//Nombre invitado, telefono invitado, datetime log, categoria de invitacion, name organizador, mobile organizador, tipo logs (entrada, salida, alerta)
			foreach($logs as $key=>$log){
				
				$guest = $this->User->read(null,$log['Invitation']['guest_id']);
				$organizer = $this->User->read(null,$log['Invitation']['organizer_id']);
				$relation = $this->Relation->read(null,$log['Invitation']['relation_id']);
				
				$log_tmp['logs'][$key] = array(
					'id'=>$log['Log']['id'],
					'house_id'=>$log['Invitation']['house_id'],
					'guest_name' => $guest['User']['name'].' '.$guest['User']['father_surname'],
					'guest_mobile' => $guest['User']['mobile'],
					'datetime' => $log['Log']['created'],
					'relation' => $relation['Relation']['name'],
					'organizer_name' => $organizer['User']['name'].' '.$organizer['User']['father_surname'],
					'organizer_mobile' => $organizer['User']['mobile'],
					'type' => $log['Log']['type']
				);
			}
			$obj->logs = json_encode($log_tmp);
		}
		
		return $obj;
	}
	
	public function sendNotification($invitation_id, $type = 'create'){
		$this->Invitation->id = $invitation_id;
		$invitation = $this->Invitation->read(null, $this->id);
		
		switch($type){
			case 'create':
				$message = $invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname'].' te ha invitado.';
				$OS = $invitation['Guest']['OS'];
				$registrationID = $invitation['Guest']['full_device'];
				$type = 'INV';
				break;
			case 'request':
				$message = $invitation['Guest']['name'].' '.$invitation['Guest']['father_surname'].' te ha solicitado una invitación.';
				$OS = $invitation['Organizer']['OS'];
				$registrationID = $invitation['Organizer']['full_device'];
				$type = 'REQ';
				break;
			case 'accept':
				$message = $invitation['Organizer']['name'].' '.$invitation['Organizer']['father_surname'].' ha aceptado tu solicitud.';
				$OS = $invitation['Guest']['OS'];
				$registrationID = $invitation['Guest']['full_device'];
				$type = 'ACC';
				break;
		}
		$flag = false;
		// echo $invitation['Guest']['OS'];exit;
		switch($OS){
			case 'AND':
				// API access key from Google API's Console
				define( 'API_ACCESS_KEY', 'AIzaSyCuTFWabl9ExkEWKrd4oYDB6j3s_8JUVo4' );
				$registrationIds = array($registrationID);
				 
				// prep the bundle
				$msg = array(
					'title'			=> 'Safecard',
					'message' 		=> $message,
					'subtitle'		=> 'Revisa tu listado de invitaciones.',
					'tickerText'	=> $message,
					'type'			=> $type
				);
				 
				$fields = array(
					'registration_ids' 	=> $registrationIds,
					'data'				=> $msg
				);
				 
				$headers = array(
					'Authorization: key=' . API_ACCESS_KEY,
					'Content-Type: application/json'
				);
				
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$result = curl_exec($ch );
				
				curl_close( $ch );
				$flag = true;
				break;
				
			case 'IOS':
				$deviceToken = $registrationID;
				// Put your private key's passphrase here:
				$passphrase = 'beth.,2409';
				////////////////////////////////////////////////////////////////////////////////

				$ctx = stream_context_create();
				stream_context_set_option($ctx, 'ssl', 'local_cert', __DIR__.'/ck.pem');
				stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
				
				// Open a connection to the APNS server
				$fp = stream_socket_client(
					'ssl://gateway.sandbox.push.apple.com:2195', $err,
					$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

				// Create the payload body
				$body['aps'] = array(
					'alert' => array(
								"action-loc-key" => "para abrir Safecard",
								"body" => $message
							),
					'sound' => 'default',
					'badge' => 1
				);

				// Encode the payload as JSON
				$payload = json_encode($body);

				// Build the binary notification
				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
				
				// Send it to the server
				$result = fwrite($fp, $msg, strlen($msg));
				
				// Close the connection to the server
				fclose($fp);
				
				$flag = true;
				break;
			case 'WPH':
			
				break;
		}
		return $flag;
	}
} 
?>