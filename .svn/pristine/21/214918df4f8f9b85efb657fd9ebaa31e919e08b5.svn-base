<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'AES');
/**
 * Invitations Controller
 *
 * @property Invitation $Invitation
 */
class InvitationsController extends AppController {
	
	public $helpers = array("QrCode");
	public $components = array('Soap'); 
	
	public $paginate = array(
		'Invitation' => array(
			'limit'=>20,
		),
		'Log' => array(
			'limit' => 15,
			'order' => array(
				'Log.created' => 'DESC'
			)
		),
	);
	
	function beforeFilter() {
		$this->Auth->allow(array('update_repeat', 'QRGenerator'));
		parent::beforeFilter();
	}
	
	public function admin_dashboard(){
		$this->Invitation->Log->recursive = 1;
		
		$this->loadModel('User');
		$condos_user = ClassRegistry::init('CondosUser')->getCondominios(); 
		
		$filtro = array();
		
		if($this->request->is('post')){
			
			if(isset($this->request->data['Log']['condo_id']) && $this->request->data['Log']['condo_id']>0){
				$filtro[] = array('Log.condo_id'=>$this->request->data['Log']['condo_id']);
				$_SESSION['filter_condo_id'] = $this->request->data['Log']['condo_id'];
			}
			
			$_SESSION['filter_start_date'] = $this->request->data['Log']['start_date'];
			$_SESSION['filter_end_date'] = $this->request->data['Log']['end_date'];
			
			if((isset($this->request->data['Log']['start_date']) && $this->request->data['Log']['start_date']!='') && (isset($this->request->data['Log']['end_date']) && $this->request->data['Log']['end_date']!='')){
				$filtro[] = array(
					'Log.created >= '=> date('Y-m-d 00:00:00', strtotime($this->request->data['Log']['start_date'])),
					'Log.created <= '=> date('Y-m-d 00:00:00', strtotime($this->request->data['Log']['end_date'])),
				);
			} else {
				$this->Session->setFlash(__('Debe seleccionar ambas fechas'), 'flash_warning');
			}
		} else {
			$filtro[] = array(
				'Log.condo_id' => isset($_SESSION['filter_condo_id'])? $_SESSION['filter_condo_id']:$condos_user,
				'Log.created >= ' => isset($_SESSION['filter_start_date'])? $_SESSION['filter_start_date']:date('Y-m-d 00:00:00', strtotime('-7 days')),
				'Log.created <= ' => isset($_SESSION['filter_end_date'])? $_SESSION['filter_end_date']:date('Y-m-d 23:59:59')
			);
		}
		
		$logs = $this->paginate($this->Invitation->Log,$filtro);
		
		$last_id = 0;
		foreach($logs as $key=>$log){
			$inv = $this->Invitation->read(null, $log['Log']['invitation_id']);
			$logs[$key]['Log']['house_name'] = $inv['House']['name'];
			$logs[$key]['Log']['organizer_name'] = $inv['Organizer']['name'].' '.$inv['Organizer']['father_surname'];
			$logs[$key]['Log']['guest_name'] = $inv['Guest']['name'].' '.$inv['Guest']['father_surname'];
			$logs[$key]['Log']['relation'] = $inv['Relation']['name'];
			
			$last_id = $log['Log']['id']>$last_id ? $log['Log']['id'] : $last_id;
		}
		$this->set('logs',$logs);
		
		$last_log = $this->Invitation->Log->find('first', array('conditions'=>$filtro, 'order'=>'Log.id DESC'));
		$this->set('last_id',empty($last_log['Log']['id'])? '' : $last_log['Log']['id']);
		
		$fecha = date('Y-m-d', strtotime(date('Y-m-d').' - 7 days'));
		$dias = array();
		for($i = 0; $fecha <= date('Y-m-d'); $i++){
			$dias[$i]['Day']['glosa'] = date('d F', strtotime($fecha));
			$dias[$i]['Day']['total_visitas'] = $this->Invitation->Log->find('count', 
													array('conditions'=>
														array(
															'Log.created <='=>$fecha.' 23:59:59', 
															'Log.created >='=>$fecha.' 00:00:00',
															'Log.condo_id' => isset($_SESSION['filter_condo_id'])?$_SESSION['filter_condo_id']:$condos_user
														)
												));
			
			$fecha = date('Y-m-d', strtotime($fecha.' +1 day'));
		}
		$this->set('dias',$dias);
		
		$relations = $this->Invitation->Relation->find('list');
		$relations_details = array();
		foreach($relations as $key=>$relation){
			$relations_details[$key]['Relation']['glosa'] = $relation;
			$relations_details[$key]['Relation']['total_visitas'] = $this->Invitation->Log->find('count', 
																	array('conditions'=> array(
																		'Invitation.relation_id'=>$key,
																		'Log.created >= ' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' -24 hours')),
																		'Log.created <= ' => date('Y-m-d H:i:s'),
																		'Log.condo_id' => isset($_SESSION['filter_condo_id'])?$_SESSION['filter_condo_id']:$condos_user
																	)));
		}
		$this->set('relations_details',$relations_details);
		
		//$this->set('condos', $this->Invitation->Condo->find('list', array('order'=>'Condo.name ASC')));
		
	}
	
	public function get_last_logs($last_id = null){
		$this->layout = false;
		$this->autoRender = false;
		
		$this->Invitation->Log->recursive = 1;
		
		if(!is_numeric($last_id) || $last_id < 1){
			$last_id = 0;
		}
		
		//Propiedades del usuario
		if(!in_array($this->Session->read('Auth.User.Role.code'), Configure::read('AppAdmin'))){
			$houses = $this->Invitation->Organizer->HousesUser->find('all',array('conditions'=>array('HousesUser.active'=>1, 'HousesUser.user_id' => $this->Session->read('Auth.User.id'))));
		} else {
			$houses = $this->Invitation->Organizer->HousesUser->find('all',array('conditions'=>array('HousesUser.active'=>1)));
		}
		$house_ids = array();
		foreach($houses as $house){
			$house_ids[] = $house['HousesUser']['house_id'];
		}
		
		$filtro = array();
		if(isset($_SESSION['filter_condo_id']) && $_SESSION['filter_condo_id']>0){
			$filtro[] = array('Log.condo_id' => $_SESSION['filter_condo_id']);
		}
		
		if(in_array($this->Session->read('Auth.User.Role.code'), 'PROP')){
			if(isset($_SESSION['filter_house_id']) && $_SESSION['filter_house_id']>0){
				$filtro[] = array('Invitation.house_id' => $_SESSION['filter_house_id']);
			}
		}	
		
		$logs = $this->Invitation->Log->find('all', array(
			'conditions'=>array(
				'Log.id > '=>$last_id,
				$filtro
			),
			'order' => array('Log.id'=>'ASC')
		));
		$logstmp = array();
		foreach($logs as $key=>$log){
			$inv = $this->Invitation->read(null, $log['Log']['invitation_id']);
			
			$logstmp[$key]['condo_name'] = $inv['Condo']['name'];
			$logstmp[$key]['created'] = date('d M, H:i',strtotime($log['Log']['created']));
			
			switch($log['Log']['type']){
				case 0: 
					$logstmp[$key]['type'] = 'ic_arrow_out.png';
					$logstmp[$key]['type_id'] = 0;
					break;
				case 1:
					$logstmp[$key]['type'] = 'ic_arrow_in.png';
					$logstmp[$key]['type_id'] = 1;
					break;
				case 2:
					$logstmp[$key]['type'] = 'ic_warning.png';
					$logstmp[$key]['type_id'] = 2;
					break;
			}
			$logstmp[$key]['house_name'] = $inv['House']['name'];
			$logstmp[$key]['organizer_name'] = $inv['Organizer']['name'].' '.$inv['Organizer']['father_surname'];
			$logstmp[$key]['guest_name'] = $inv['Guest']['name'].' '.$inv['Guest']['father_surname'];
			$logstmp[$key]['relation'] = $inv['Relation']['name'];
			$logstmp[$key]['id'] = $log['Log']['id'];
		}
		echo json_encode($logstmp);
	}
	
	public function dashboard(){
		$this->Invitation->Log->recursive = 1;
		
		//Propiedades del usuario
		$houses = $this->Invitation->Organizer->HousesUser->find('all',array('conditions'=>array('HousesUser.active'=>1, 'HousesUser.user_id' => $this->Session->read('Auth.User.id'))));
		$house_ids = array();
		foreach($houses as $house){
			$house_ids[] = $house['HousesUser']['house_id'];
		}
		$filtro = array(); 
		
		if($this->request->is('post')){
			
			if(isset($this->request->data['Log']['house_id']) && $this->request->data['Log']['house_id']>0){
				$filtro[] = array('Invitation.house_id'=>$this->request->data['Log']['house_id']);
				$_SESSION['filter_house_id'] = $this->request->data['Log']['house_id'];
			}
			
			$_SESSION['filter_start_date'] = $this->request->data['Log']['start_date'];
			$_SESSION['filter_end_date'] = $this->request->data['Log']['end_date'];
			
			if((isset($this->request->data['Log']['start_date']) && $this->request->data['Log']['start_date']!='') && (isset($this->request->data['Log']['end_date']) && $this->request->data['Log']['end_date']!='')){
				$filtro[] = array(
					'Log.created >= '=> date('Y-m-d 00:00:00', strtotime($this->request->data['Log']['start_date'])),
					'Log.created <= '=> date('Y-m-d 00:00:00', strtotime($this->request->data['Log']['end_date'])),
				);
			} else {
				$this->Session->setFlash(__('Debe seleccionar ambas fechas'), 'flash_warning');
			}
		} else {
			$filtro[] = array(
				'Invitation.house_id' => isset($_SESSION['filter_house_id'])? $_SESSION['filter_house_id']:$house_ids,
				'Log.created >= ' => isset($_SESSION['filter_start_date'])? $_SESSION['filter_start_date']:date('Y-m-d 00:00:00', strtotime('-7 days')),
				'Log.created <= ' => isset($_SESSION['filter_end_date'])? $_SESSION['filter_end_date']:date('Y-m-d 23:59:59')
			);
		}
		
		//Filtro por propiedades
		$logs = $this->paginate($this->Invitation->Log, $filtro);
		$last_id = 0;
		foreach($logs as $key=>$log){
			$inv = $this->Invitation->read(null, $log['Log']['invitation_id']);
			$logs[$key]['Log']['house_name'] = $inv['House']['name'];
			$logs[$key]['Log']['organizer_name'] = $inv['Organizer']['name'].' '.$inv['Organizer']['father_surname'];
			$logs[$key]['Log']['guest_name'] = $inv['Guest']['name'].' '.$inv['Guest']['father_surname'];
			$logs[$key]['Log']['relation'] = $inv['Relation']['name'];
			
			$last_id = $log['Log']['id']>$last_id ? $log['Log']['id'] : $last_id;
		}
		$this->set('logs',$logs);
		$this->set('last_id',$last_id);
		
		$fecha = date('Y-m-d', strtotime(date('Y-m-d').' - 7 days'));
		$dias = array();
		for($i = 0; $fecha <= date('Y-m-d'); $i++){
			$dias[$i]['Day']['glosa'] = date('d F', strtotime($fecha));
			$dias[$i]['Day']['total_visitas'] = $this->Invitation->Log->find('count', 
													array('conditions'=>array(
														'Log.created <='=>$fecha.' 23:59:59', 'Log.created >='=>$fecha.' 00:00:00',
														'Invitation.house_id' => isset($_SESSION['filter_house_id'])? $_SESSION['filter_house_id']:$house_ids
														)
												));
			
			$fecha = date('Y-m-d', strtotime($fecha.' +1 day'));
		}
		$this->set('dias',$dias);
		
		$relations = $this->Invitation->Relation->find('list');
		$relations_details = array();
		foreach($relations as $key=>$relation){
			$relations_details[$key]['Relation']['glosa'] = $relation;
			$relations_details[$key]['Relation']['total_visitas'] = $this->Invitation->Log->find('count', 
																	array('conditions'=> array(
																		'Invitation.relation_id'=>$key,
																		'Log.created >= ' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' -24 hours')),
																		'Log.created <= ' => date('Y-m-d H:i:s'),
																		'Invitation.house_id' => $house_ids
																	)));
		}
		// pr($relations_details);exit;
		$this->set('relations_details',$relations_details);
		
		$this->set('condos', $this->Invitation->Condo->find('list', array('order'=>'Condo.name ASC')));
		
	}
	
/**
 * admin_index method
 *
 * @return void
 */
	public function index() {
		$this->Invitation->recursive = 1;
			
		$invitations_received = $this->Invitation->find('all', array('conditions'=> array(
			'OR'=>array(
				array('Invitation.guest_id' => $this->Session->read('Auth.User.id')),
				array('Invitation.mobile' => $this->Session->read('Auth.User.mobile'))
			),
			'Invitation.sent_mobile' => 0,
			'Event.to >= '=> date('Y-m-d H:i:s'),
			'Invitation.relation_id <> '=>1,
			'Invitation.status' => 1
		)));
		
		$invitations_sent = $this->Invitation->find('all', array('conditions'=> array(
			'Invitation.organizer_id'=>$this->Session->read('Auth.User.id'),
			'Invitation.sent_mobile' => 0,
			'OR'=>array(
				'Invitation.requested' => 0,
				'AND'=>array('Invitation.requested' => 1, 'Invitation.status'=>1)
			),
			'Invitation.relation_id <>' => 1,
			'Event.to >= '=> date('Y-m-d H:i:s'),
			'Invitation.status' => 1
		)));
		
		$invitations_request = $this->Invitation->find('all', array('conditions'=> array(
			'OR'=>array(
				array('Invitation.organizer_id'=>$this->Session->read('Auth.User.id')),
				array('Invitation.guest_id'=>$this->Session->read('Auth.User.id'))
			),
			'Invitation.sent_mobile' => 0,
			'Invitation.requested' => 1,
			'Invitation.status' => 0,
			'Event.to >= '=> date('Y-m-d H:i:s')
		)));
		
		$this->set('invitations_received', $invitations_received);
		$this->set('invitations_sent', $invitations_sent);
		$this->set('invitations_request', $invitations_request);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Invitation->exists($id)) {
			throw new NotFoundException(__('Invitación Invalida'));
		}
		$options = array('conditions' => array('Invitation.' . $this->Invitation->primaryKey => $id));
		$this->set('invitation', $this->Invitation->find('first', $options));
	}
	
	public function update_repeat(){
		$this->layout = false;
		$this->autoRender = false;
		
		$invitations = $this->Invitation->find('all', array('conditions'=>array('status'=>1,'repeat'=>1, 'repeat_from <= '=>date('Y-m-d'), 'repeat_to >= '=>date('Y-m-d'))));
		//pr($invitations);
		foreach($invitations as $key=>$invitation){
			$event = $this->Invitation->Event->read(null, $invitation['Event']['id']);
			
			$diferencia = 99;
			$next_day = $next_day_aux = date('Y-m-d');
			foreach($event['Day'] as $key=>$days){
				$days_start_time = $days['DaysEvent']['from'];
				$days_end_time =  $days['DaysEvent']['to'];
			
				$day_name = getDayName($days['code']);
				
				$datetime1 = new DateTime(date('Y-m-d'));
				$next_day_aux = date('Y-m-d', strtotime("next $day_name", strtotime(date('Y-m-d'))));
				$datetime2 = new DateTime($next_day_aux);
				$interval = $datetime1->diff($datetime2);
				
				if(date('w') == $days['code']){
					$diferencia = 0;
					$next_day = date('Y-m-d');
				} else {
					if($interval->format('%a') < $diferencia){
						$diferencia = $interval->format('%a');
						$next_day = date('Y-m-d', strtotime("next $day_name", strtotime(date('Y-m-d'))));
					}
				}
			}
			
			$this->Invitation->Event->saveField('from', $next_day.' '.$days_start_time);
			$this->Invitation->Event->saveField('to', $next_day.' '.$days_end_time);
			
			$this->Invitation->read(null, $invitation['Invitation']['id']);
			$this->Invitation->saveCode($invitation['Invitation']['id']);
			mail("fajardo.esteban@gmail.com","Actualización de invitaciones repetitivas", "ID invitacion: ".$invitation['Invitation']['id']." dia ".$next_day." ".$days_start_time." - ".$days_end_time);
		}
	}
	
	public function QrGenerator(){
		
		if($this->request->data){
			$opts = array( 'http'=>array( 'user_agent' => 'PHPSoapClient'));
			$context = stream_context_create($opts);
			$client = new SoapClient('http://dev.dribble.cl/~efajardo/safecard/soap/WsMobile/wsdl',
									 array('stream_context' => $context,
										   'cache_wsdl' => WSDL_CACHE_NONE));
			
			$result = $client->soap_AddInvitations(array(
				'subject'=> 'testing Benavides',
				'start_date' => $this->request->data['Event']['dfrom'],
				'start_time' => $this->request->data['Event']['hfrom'],
				'end_date' => $this->request->data['Event']['dto'],
				'end_time' => $this->request->data['Event']['hto'],
				'house_id' => $this->request->data['Event']['house_id'],
				'mobile_organizer' => $this->request->data['Event']['mobile_organizer'],
				'mobile_guests' => $this->request->data['Event']['mobile_guests'],
				'name_guests' => $this->request->data['Event']['name_guests'],
				'relation_id' => $this->request->data['Event']['relation_id'],
				'repeat' => 0,
				'days' => ''
			));
				
			$this->set('aes',$result->aes);
		}
	}
	
}
