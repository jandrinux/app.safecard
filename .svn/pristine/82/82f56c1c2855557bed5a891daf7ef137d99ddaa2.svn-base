<?php

App::uses('AesCrypt', 'AesCrypt.Lib');

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public  $components = array(/*'Security',*/ 
		'Acl', 
		'Auth' => array(
			'authenticate' => array(
				'Form' => array(
					'fields' => array('username' => 'mobile')
				)
			)
		), 
		'Session', 'RequestHandler');
		
    public  $helpers = array('Form', 'Html', 'Session');

    public function beforeFilter() {

        Security::setHash('md5');
        $this->_initAuth();
		
		if(in_array($this->params['controller'],array('rest_users'))){
			// For RESTful web service requests, we check the name of our contoller
			$this->Auth->allow();
			// this line should always be there to ensure that all rest calls are secure
			/* $this->Security->requireSecure(); */
			//$this->Security->unlockedActions = array('edit','delete','add','view');
			//echo "akaka";
		}
		
        //authorization to requested action
        if ($this->Auth->loggedIn()) {
            $this->layout = 'dashboard';
            if ($this->_isAdminMode()) {
                $aco = '/'.$this->params['action'];
                if ($this->params['plugin']) {
                    $aco = ucfirst($this->params['plugin']).'/'.$this->params['controller'].$aco;
                } else {
                    $aco = ucfirst($this->params['controller']).$aco;
                }

                if (!$this->_adminCheck($this->Session->read('Auth.User.role_id'), $aco)) {
                    // $this->Session->setError('No posee acceso');
                    // $this->layout = 'error';
                    $this->Session->setFlash('No posee acceso o privilegios de administrador','flash_error');
                    throw new NotFoundException(utf8_encode('No posee acceso o privilegios de administrador'));
                }
            } else {
                $aco = ucfirst($this->params['controller']).'/'.$this->params['action'];
                if (!$this->_adminCheck($this->Session->read('Auth.User.role_id'), $aco)) {
                    //$this->Session->setError('No posee acceso');
                    $this->layout = 'admin';
                    throw new NotFoundException(utf8_encode('No posee los permisos necesarios para esta sección'));
                }		
            }
        } else {
            $this->layout = 'frontend';
        }
    }

    /**
     * Sets standarts redirections for logged and normal users
     * @link http://book.cakephp.org/2.0/en/core-libraries/components/authentication.html
     */
    private function _initAuth() {
        $adminMode = false;
        $layout = 'default';
        $redirect = 'index';
        if ($this->_isAdminMode()) {
            $adminMode = true;
            $layout = 'frontend';
            $redirect = 'index';
        }

        $this->set('adminMode',$adminMode);

        // Cambio
        //Roles de usuario
        $roles_usuario = ClassRegistry::init('UsersRole')->find('all', array('fields'=>array('Role.*'), 'conditions'=>array('user_id'=>$this->Auth->user('id'))));
        $this->set('roles_usuario',$roles_usuario);
        
        //Condos de usuario         
        $condos_ids = ClassRegistry::init('CondosUser')->getCondominios();
        $this->set("condos_ids",$condos_ids);
        $condos_usuario = ClassRegistry::init('Condo')->find('list', array('conditions'=>array('Condo.id'=>$condos_ids)));
        $this->set('condos_usuario',$condos_usuario);
        // 
        
        $this->layout = $layout;
        $this->Auth->loginError = 'Usuario o password inválido. Intente nuevamente';
        $this->Auth->authError = 'No posee acceso';
        $this->Auth->loginAction = array('admin' => $adminMode, 'controller' => 'users', 'action' => 'login');
        $this->Auth->loginRedirect = array('admin' => $adminMode, 'controller'=>'users', 'action' => $redirect);
        $this->Auth->logoutRedirect = array('controller'=>'users', 'action' => 'login');
        $this->Auth->unauthorizedRedirect = array('admin' => $adminMode, 'controller'=>'users', 'action'=>$redirect);
    }

    /**
     * Determines if app is on admin or web mode based on prefixes
     */
    private function _isAdminMode() {
        $adminRoute = Configure::read('Routing.prefixes');
        if (isset($this->params['prefix']) && in_array($this->params['prefix'], $adminRoute)) {
            return true;
        }
        return false;
    }

    /**
     * Checks acl permissions
     */
    public function _adminCheck($rol_id, $aco = null, $action = '*'){
		$aco = str_replace('.','/',$aco);
		return $this->Acl->check(array('model'=>'Role','foreign_key' => $rol_id), $aco, $action);
    }

    /**
     * This functions executes before every view render
     * Configures error layouts in case of exception
     * Gets menu array to be displayed, with acl filters
     * Sets superadmin variable
     */
    public function beforeRender() {
        // $this->_configureErrorLayout();
        $isAdmin = $this->_isAdminMode();

        if (is_numeric($this->Session->read('Auth.User.role_id'))) {
            /* menu */
            $mainMenu = ClassRegistry::init('Menu')->getMainMenu($isAdmin);

            //filter acl
            foreach($mainMenu as $i=>$menu) {
                foreach($menu['childrens'] as $j=>$childMenu) {
                    $aco = ucfirst($childMenu['Menu']['controller']).'/';
                    if ($isAdmin)
                            $aco .= 'admin_';
                    $aco .= $childMenu['Menu']['action'];
                    if (!$this->_adminCheck($this->Session->read('Auth.User.role_id'), $aco)){
                            unset($mainMenu[$i]['childrens'][$j]);
                    }
                }

                if (count($mainMenu[$i]['childrens']) <= 0){
                        unset($mainMenu[$i]);
                }
            }

            //superadmin variable
            $rol = ClassRegistry::init('Role')->findById($this->Auth->user('role_id'));
            $superadmin = false;
            if (!empty($rol) && strtolower($rol['Role']['name']) == 'superadmin')
                $superadmin = true;

            $this->_superadmin = $superadmin;
            //superadmin not passed to view, security issues
            $this->set(compact('mainMenu'/*, 'superadmin'*/));

            //Cantidad de registros
            
        $filtro = array();
        if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){           
            $condos_id = ClassRegistry::init('CondosUser')->getCondominios();
            $filtro = array('Owner.condo_id'=>$condos_id);
        }
        
        if(in_array($this->Session->read('Auth.User.Role.code'),Configure::read('AppAdmin'))){
            $this->set('total_registrados', ClassRegistry::init('Owner')->find('count', array('conditions'=>$filtro)));
        } else {
            $this->set('total_registrados', ClassRegistry::init('Contact')->find('count', array('conditions'=>array('Contact.organizer_id'=>$this->Session->read('Auth.User.id')))));
        }
        
        //Notificaciones
        $this->set('notifications', ClassRegistry::init('Notification')->find('all', array('limit'=>'10')));            
        
        //Roles de usuario
        $roles_usuario = ClassRegistry::init('UsersRole')->find('all', array('fields'=>array('Role.*'), 'conditions'=>array('user_id'=>$this->Auth->user('id'))));
        $this->set('roles_usuario',$roles_usuario);
        
        //Condos de usuario         
        $condos_ids = ClassRegistry::init('CondosUser')->getCondominios();
        $this->set("condos_ids",$condos_ids);
        $condos_usuario = ClassRegistry::init('Condo')->find('list', array('conditions'=>array('Condo.id'=>$condos_ids)));
        $this->set('condos_usuario',$condos_usuario);
        
        /*$condos_usuario = ClassRegistry::init('CondosUser')->find('all', array( 'fields'=>array('Condo.id','Condo.name'),'conditions'=>array('CondosUser.user_id'=>$this->Auth->user('id'))));
        if(!empty($condos_usuario)){
            foreach($condos_usuario as $key=>$condo){
                $condos_ids[] = $condo['Condo']['id'];
            }
            
            $condos_usuario = ClassRegistry::init('Condo')->find('list', array('conditions'=>array('Condo.id'=>$condos_ids)));
            $this->set('condos_usuario',$condos_usuario);
        }*/
        
        //Propiedades
        ClassRegistry::init('House')->bindModel(array('hasOne' => array('HousesUser')), false);
        $houses = ClassRegistry::init('House')->find('all',array('conditions'=>array('HousesUser.house_id NOT'=>NULL, 'HousesUser.active'=>1, 'HousesUser.user_id' => $this->Auth->user('id'))));
        
        //Students
        ClassRegistry::init('Student')->bindModel(array('hasOne' => array('HousesUser')), false);
        $students = ClassRegistry::init('Student')->find('all',array('conditions'=>array('HousesUser.student_id NOT'=>NULL, 'HousesUser.active'=>1, 'HousesUser.user_id' => $this->Auth->user('id'))));
        

        //Días 
        $days = ClassRegistry::init('Day')->find('list');
        $this->set('days',$days);
        
        //Relaciones
        if($this->Session->read('Auth.User.Role.code') == "ADM" || $this->Session->read('Auth.User.Role.code') == "CSJ"){
            $relations = ClassRegistry::init('Relation')->find('list', array('conditions'=>array('Relation.id'=>array(4,6,7))));
        } else {
            $relations = ClassRegistry::init('Relation')->find('list');
        }
        $this->set('relations',$relations);
        
        //Visitas Diarias
        $daily_visits = ClassRegistry::init('Log')->find('count', array('conditions'=>array('Log.created >= ' => date('Y-m-d 00:00:00'),'Log.created <= ' => date('Y-m-d 23:59:59'))));
        $this->set('daily_visits',$daily_visits);
        
        $user_houses = array();
        foreach($houses as $house){
            $user_houses[$house['House']['id']] = $house['Condo']['name'].' - '.$house['House']['name'];
        }
        $this->set(compact('user_houses'));

        $user_students = array();
        foreach($students as $student){
            $user_students[$student['Student']['id']] = $student['School']['name'].' - '.$student['Student']['full_name'];
        }
        $this->set(compact('user_houses','user_students'));
        }
    }
}
