<?php
App::uses('AppController', 'Controller');
/**
 * Houses Controller
 *
 * @property House $House
 */
class HousesController extends AppController {
    
    public function beforeFilter() {
		parent::beforeFilter();
	}
	
    public function index() {
        $this->House->recursive = 1;
        
        $this->House->bindModel(array('hasOne' => array('HousesUser')), false);
        $houses = $this->paginate(array('HousesUser.active'=>1, 'HousesUser.user_id' => $this->Session->read('Auth.User.id')));
        
        $this->loadModel('Event');
        foreach($houses as $key=>$house){
            $houses[$key]['House']['total_eventos'] = $this->Event->totalEventosByHouse($house['House']['id']);
        }
        $this->set('houses', $houses);
    }
/**
 * admin_index method
 *
 * @return void
 */
    public function admin_index() {
		
        $this->House->recursive = 1;
		
		$filtro = array();
		if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){			
			$condos_id = ClassRegistry::init('CondosUser')->getCondominios();
			$filtro = array('House.condo_id'=>$condos_id);
		}
		
        $this->set('houses', $this->paginate($filtro));
    }

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_view($id = null) {
        if (!$this->House->exists($id)) {
            throw new NotFoundException(__('Vivienda Invalida'));
        }
		$house = $this->House->read(null,$id);
		if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){			
			$condos_id = ClassRegistry::init('CondosUser')->getCondominios();
			if(!in_array($house['House']['condo_id'],$condos_id)){
				throw new NotFoundException(__('Vivienda Invalida'));
			}
		}
		
        $options = array('conditions' => array('House.' . $this->House->primaryKey => $id));
        $this->set('house', $this->House->find('first', $options));
    }

/**
 * admin_add method
 *
 * @return void
 */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->House->create();
            if ($this->House->save($this->request->data)) {
                
                $this->Session->setFlash(__('Vivienda creada exitosamente'),'flash_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Vivienda no pudo ser guardada. Intentelo nuevamente'),'flash_error');
            }
        }
		
		$filtro = array();
		if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){			
			$condos_id = ClassRegistry::init('CondosUser')->getCondominios();
			$filtro = array('Condo.id'=>$condos_id);
		}
		
        $condos = $this->House->Condo->find('list', array('conditions'=>$filtro));
        $this->set(compact('condos'));
    }

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_edit($id = null) {
        if (!$this->House->exists($id)) {
            throw new NotFoundException(__('Invalid house'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->House->save($this->request->data)) {
                    $this->Session->setFlash(__('Vivienda editada exitosamente'), 'flash_success');
                    $this->redirect(array('action' => 'index'));
            } else {
                    $this->Session->setFlash(__('La vivienda no pudo ser editada, intentalo nuevamente'), 'flash_error');
            }
        } else {
            $options = array('conditions' => array('House.' . $this->House->primaryKey => $id));
            $this->request->data = $this->House->find('first', $options);
        }
		
		$filtro = array();
		if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){			
			$condos_id = ClassRegistry::init('CondosUser')->getCondominios();
			$filtro = array('Condo.id'=>$condos_id);
		}
		
        $condos = $this->House->Condo->find('list', array('conditions'=>$filtro));
        $this->set(compact('condos'));
    }

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_delete($id = null) {
        $this->House->id = $id;
        if (!$this->House->exists()) {
            throw new NotFoundException(__('Vivienda invalida'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->House->delete()) {
            $this->Session->setFlash(__('Vivienda eliminada exitosamente'),'flash_success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Vivienda no pudo ser eliminada'), 'flash_error');
        $this->redirect(array('action' => 'index'));
    }
    
    public function admin_getHouseByCondo($condo_id = null){
        $this->autoRender = false;
        $this->layout = false;
        if($this->request->is('ajax')){
            $houses = array();
            if($this->House->Condo->exists($condo_id)){
                $houses = $this->House->find('list', array('conditions'=>array('condo_id'=>$condo_id)));
            }
            echo json_encode($houses);
        }
    }
}
