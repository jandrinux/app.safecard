<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'recaptchalib');

/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {
	
	public $components = array('Email');
	
	public $paginate = array(
		'User'=>array(
			'limit' => 20,
			'contain' => array('UsersRole','CondosUser'),
			'group' => array('User.id')
		),
		'Contact'=>array(
			'limit' => 20,
		),
		'HousesUser'=>array(
			'limit'=>20,
			'contain' => array('House','User','Student'),
			'order' => array('User.name' => 'ASC','User.father_surname' => 'ASC')
		)
	);
	
	function beforeFilter() {
		$this->Auth->allow(array('getCallingCodeByCountry','set_role','index','add','pass_recovery','new_password','send_password'));
		parent::beforeFilter();
	}
	
	public function view($id = null) {
            
		if (!isset($id)) {
			$id = $this->Session->read('Auth.User.id');
		}
		
		if(!$this->User->exists($id)) {
			throw new NotFoundException(__('Usuario invalido'));
		}
			 
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}
	
	public function profile() {
		$id = $this->Session->read('Auth.User.id');
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Usuario invalido'));
		}
		
		if ($this->request->is('ajax')) {
			
			$this->autoRender = false;
			$success = true;
			// pr($this->request->data);exit;
			if(!empty($this->request->data['User']['image'])){
				$file = $this->request->data['User']['image'];
				$extension = getExtension($file['name']);
				$nombre_imagen = md5($file['name']).'.'.$extension;
				$dir = WWW_ROOT.'files/profile/'.$nombre_imagen;
				// echo $dir; exit;
				$this->request->data['User']['image'] = $nombre_imagen;
				$success = @move_uploaded_file($file['tmp_name'], $dir);
				unlink(WWW_ROOT.'files/profile/'.$this->Session->read('Auth.User.image'));
			} else {
				$this->request->data['User']['image'] = $this->Session->read('Auth.User.image');
			}
			
			if($success){
				if ($this->User->save($this->request->data)) {
					$retorno = array('success'=>true, 'message'=>__('El perfil ha sido editado exitosamente'));
					
					$user = $this->User->read(null,$this->User->id);
					$this->Session->write('Auth.User.image', $user['User']['image']);
					$this->Session->write('Auth.User.name', $user['User']['name']);
					$this->Session->write('Auth.User.father_surname', $user['User']['father_surname']);
					$this->Session->write('Auth.User.mother_surname', $user['User']['mother_surname']);
				} else {
					$retorno = array('success'=>false, 'message'=>__('No pudo ser editado tu perfil. Intentalo nuevamente'));
				}
			} else {
				$retorno = array('success'=>false, 'message'=>__('Ocurrió un error al cargar imagen. Intentelo nuevamente'));
			}
			echo json_encode($retorno);
			exit;
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
			
			$roles = $this->User->Role->find('list');
			$this->set(compact('roles'));
		}
	}
	
	public function editpin(){
		$this->autoRender = false;
		$this->layout = false;

		if ($this->request->is('ajax')) {
			$this->User->id = $this->Session->read('Auth.User.id');

			$this->autoRender = false;
			$success = true;
			// pr($this->request->data);exit;
			if(AuthComponent::password($this->request->data['User']['old_password']) ==  $this->User->field('password') 
				&& $this->request->data['User']['new_password'] == $this->request->data['User']['password']){
				
				$this->User->saveField('password', $this->request->data['User']['password']);

				//Reinicio contador de administradores y conserjes
				$this->User->CondosUser->updateAll(
					array('CondosUser.sent_gate' => 0),
    				array('CondosUser.user_id' => $this->User->id)
    			);

    			//Reinicio contador de propietarios - residentes
    			$this->User->Invitation->updateAll(
    				array('Invitation.sent_gate' => 0),
    				array(
    					'Invitation.relation_id IN' => array(1,2),
    					'Invitation.status' => 1,
    					'Invitation.organizer_id' => $this->User->id,
    					'Invitation.guest_id' => $this->User->id
    				)
    			);

				$retorno = array('success'=>true,'message'=>__('Su password ha sido modificado exitosamente.'));

				$this->User->sendCustomNotification($this->User->id,"Su password ha sido modificado","Actualización de password");

			} else {
				$retorno = array('success'=>false, 'message' => __('Su password no ha sido actualizado. Revise los datos ingresados en el formulario.'));
				//$this->request->data['User']['image'] = $this->Session->read('Auth.User.image');
			}
			echo json_encode($retorno);
		}
	}

	/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			$role = $this->User->Role->findByCode('PROP');
			$this->request->data['User']['role_id'] = $role['Role']['id'];
			
			$this->User->Country->id = $this->request->data['User']['country_id'];
			$country = $this->User->Country->read(null, $this->User->Country->id);
			
			if ($this->User->save($this->request->data)) {
			
				// $this->request->data['User']['mobile'] = '+'.$country['Country']['calling_code'].'9'.$this->request->data['User']['mobile'];
				$this->User->saveField('mobile', '+'.$country['Country']['calling_code'].'9'.$this->request->data['User']['mobile']);
				
				//Merge entre propietarios cargados por Administradora y usuarios registrados
				$this->loadModel('Owner');
				$owners = $this->Owner->find('all', array('conditions'=>array('Owner.mobile'=>$this->User->field('mobile'))));
				
				if(!empty($owners)){
					$data = array();
					$i = 0;
					foreach($owners as $key=>$owner){
						$this->Owner->House->HousesUser->create();
						$data[$i]['HousesUser'] = array(
							'user_id'=>$this->User->id,
							'house_id'=>$owner['Owner']['house_id'],
							'owner'=>1,
							'active'=>1
						);
						$i++;
					}
					if($this->Owner->House->HousesUser->saveMany($data)){
						$this->Session->setFlash(__('Usuario registrado exitosamente'), 'flash_success');
						$this->redirect(array('action' => 'index'));
					} else {
						$this->User->deleteAll(array('User.id'=>$this->User->id));
						$this->Session->setFlash(__('Usuario no registrado. Intentalo nuevamente'), 'flash_success');
					}
					
				} else {
					$this->Session->setFlash(__('Usuario registrado exitosamente. Ingresa con tu celular y PIN'), 'flash_success');
					$this->redirect(array('action' => 'login'));
				}
				
			} else {
				// debug($this->User->validationErrors);
				$this->Session->setFlash(__('Usuario no registrado. Intentalo nuevamente'),'flash_error');
			} 
		}
		
		$country = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
		$country_sel = $this->User->Country->findByIso2($country['geoplugin_countryCode']);
		$this->set('countries',$this->User->Country->find('list'));
		$this->set('country_selected',$country_sel['Country']['id']);
		
		$roles = $this->User->Role->find('list');
		$this->set(compact('roles'));
	}
		
	public function login(){
		//$this->layout = 'login';
		if($this->request->is('post')){
			$this->User->Country->id = $this->request->data['User']['country_id'];
			$country = $this->User->Country->read(null, $this->User->Country->id);
			$mobile = $this->request->data['User']['mobile'];
			$this->request->data['User']['mobile'] = '+'.$country['Country']['calling_code'].$this->request->data['User']['mobile'];
			$user = $this->User->findByMobile($this->request->data['User']['mobile']);
			
			if(!empty($user)){
				$this->User->id = $user['User']['id'];
				if(is_null($user['User']['role_id'])){
					$user_role = $this->User->UsersRole->find('first', array('conditions'=>array('user_id'=>$user['User']['id'])));
					$this->User->saveField('role_id',$user_role['UsersRole']['role_id']);
				}
			}
			
			if($this->Auth->login()){
				$this->User->id = $this->Auth->user('id');
				$this->User->saveField('last_login', date('Y-m-d H:i:s') );
				//echo json_encode(array('exito'=>1,'msg'=>'', 'controller'=>'users','action'=>'index','admin'=>true));
				
				if(in_array($this->Session->read('Auth.User.Role.code'), Configure::read('AppAdmin')))
					$this->redirect(array('controller'=>'invitations', 'action' => 'dashboard', 'admin'=>true));
				else 
					$this->redirect(array('controller'=>'contacts','action' => 'index', 'admin'=>false));
			} else {
				//echo json_encode(array('exito'=>0, 'msg'=>'Su email o password es incorrecto.'));
				$this->request->data['User']['mobile'] = $mobile;
				$this->Session->setFlash(__('Su celular o password es incorrecto.'),'flash_error');
			}
		}

		if($this->Auth->loggedIn()){
			$this->layout = 'dashboard';
			//$this->redirect($this->Auth->redirect());
			$this->redirect(array('controller'=>'contacts', 'action'=>'index'));
		}
		
		$country = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
		$country_sel = $this->User->Country->findByIso2($country['geoplugin_countryCode']);
		$this->set('countries',$this->User->Country->find('list'));
		$this->set('country_selected',$country_sel['Country']['id']);
	}
	
	public function admin_login(){
		$this->login();
		$this->render('login');
	}
	
	public function logout(){
		$this->User->id = $this->Session->read('Auth.User.id');
		$this->Session->destroy();
		$this->redirect($this->Auth->logout());
	}
	
	public function pass_recovery(){
		//$this->layout = 'frontend';
		if (!empty($this->request->data)) {
			
			$privatekey = Configure::read('Captcha.PrivateKey');
			$resp = recaptcha_check_answer ($privatekey,
										$_SERVER["REMOTE_ADDR"],
										$_POST["recaptcha_challenge_field"],
										$_POST["recaptcha_response_field"]);

			if (!$resp->is_valid) {
				$this->Session->setFlash(__('Error: código de verificación.'),'flash_error');
			} else {
			
				switch($this->request->data['User']['via']){
					case "viaSms": 
						$country = $this->User->Country->findById($this->request->data['User']['country_id']);
						
						$mobile = '+'.$country['Country']['calling_code'].$this->request->data['User']['mobile'];
						$user = $this->User->findByMobile($mobile);
						
						if(!empty($user)){
							$short_url = get_bitly_short_url(Router::url(array('controller'=>'users','action'=>'new_password', $user['User']['password'],$user['User']['id']), true),Configure::read('BitlyUsername'),Configure::read('BitlyApiKey'));

							//Enviar SMS
							//set POST variables
							$ch = curl_init();
							$url = 'http://sms.sistek.cl/sms.asmx/Sms';
							$text = 'Para reestablecer su pin ingrese a '.$short_url.'. Omita este mensaje en caso que no hayas solicitado recuperación de pin.';
							
							$fields = array(
								'id' => Configure::read('IdSMS'),
								'text' => $text,
								'to_number' => str_replace('+','',$mobile),
								'username' => Configure::read('UsernameSMS'),
								'password' => Configure::read('PassSMS'),
								'type'=>'SMS'
							);
							
							$fields_string = http_build_query($fields);
							
							//set the url, number of POST vars, POST data
							curl_setopt($ch,CURLOPT_URL, $url);
							curl_setopt($ch,CURLOPT_POST, count($fields));
							curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

							//execute post
							$result = curl_exec($ch);
							if($result){
								$this->Session->setFlash(__('Se ha enviado un SMS con las instrucciones de recuperación de su pin.'),'flash_success');
							} else {
								$this->Session->setFlash(__('Ha ocurrido un error en el envío del SMS. Inténtelo nuevamente.'),'flash_error');
							}
							//close connection
							curl_close($ch);
							
						} else {
							$this->Session->setFlash(__('Número de celular no registrado en Safecard'),'flash_warning');
						}
					break;
					case "viaEmail": 
						$user = $this->User->findByEmail($this->request->data['User']['email']);
						if(!empty($user)){
							//Enviar Email
							$short_url = get_bitly_short_url(Router::url(array('controller'=>'users','action'=>'new_password', $user['User']['password'],$user['User']['id']), true),Configure::read('BitlyUsername'),Configure::read('BitlyApiKey'));
							
							$this->Email->smtpOptions = array( 
								'port'		=>'465', 
								'timeout'	=>'30', 
								'host' 		=> 'ssl://smtp.gmail.com', 
								'username'	=> 'esteban.fajardo@safecard.cl', 
								'password'	=>'sjdh58bwby52',   
							); 
							
							$this->Email->delivery 	= 'smtp';
							$this->Email->replyTo 	= 'esteban.fajardo@safecard.cl';
							$this->Email->from    	= Configure::read('AppName'). ' <no-reply@safecard.cl>'; 
							$this->Email->to      	= $user['User']['email']; 
							$this->Email->subject 	= 'Recuperar Pin'; 
							$this->Email->template 	= 'pass_recovery'; 
							$this->Email->sendAs 	= 'html'; 
							$this->set(compact('short_url','user')); 
							
							if($this->Email->send()){
								$this->Session->setFlash('Un email ha sido enviado a la cuenta '.$this->request->data['User']['email'], 'flash_success');
								$this->redirect(array('action' => 'login'));
							} else {
								$this->Session->setFlash('Ocurrió un error. Inténtelo nuevamente.', 'flash_success');
							}
						} else {
							$this->Session->setFlash(__('Email no registrado en Safecard'),'flash_warning');
						}
					break;
				}
			}
		}
		$country = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
		$country_sel = $this->User->Country->findByIso2($country['geoplugin_countryCode']);
		$this->set('countries',$this->User->Country->find('list'));
		$this->set('country_selected',$country_sel['Country']['id']);
	}
	
	public function new_password($token = null, $user_id = null) {
		
		$this->User->id = $user_id;
		if (!$this->User->exists()) {
			throw new NotFoundException(utf8_encode('Usuario inválido'));
		}
		// exit;
		if($this->request->is('post') || $this->request->is('put')){
			$this->User->recursive = 0;
			$user = $this->User->find('first', array('conditions'=>array('password'=>$this->request->data['User']['token'],'User.id'=>$this->request->data['User']['id'])));
			
			$privatekey = Configure::read('Captcha.PrivateKey');
			$resp = recaptcha_check_answer ($privatekey,
										$_SERVER["REMOTE_ADDR"],
										$_POST["recaptcha_challenge_field"],
										$_POST["recaptcha_response_field"]);

			if (!$resp->is_valid) {
				$this->Session->setFlash(__('Error: código de verificación.'),'flash_error');
			} else {
				if(!empty($user)){
					$this->User->id = $user['User']['id'];
					$this->User->read(null,$this->User->id);
					
					if($this->User->save($this->request->data)){
						if($this->User->sendCustomNotification($this->User->field('id'),'Su pin Safecard ha sido modificado.','')){					
							$this->Session->setFlash(__('Pin modificado exitosamente'),'flash_success');
							$this->redirect(array('action'=>'login'));
						}else{
							$this->Session->setFlash(__('envio erroneo'),'flash_success');
						}
					} else {
						$this->Session->setFlash(__('Ocurrió un error. Inténtelo nuevamente'),'flash_error');
					}
				} else {
					$this->Session->setFlash(__('Página inválida'),'flash_error');
					$this->redirect(array('action'=>'login'));
				}
			}
		}
		
		$this->set('token',$token);
		$this->set('user_id',$user_id);
	}
		
	/**
	 * Genera email con nueva password y la envia
	 */
	private function send_password($id, $newPass) {
				
		$this->User->id = $id;	
		if (!$this->User->exists()) {
			throw new NotFoundException(utf8_encode('Usuario inválido'));
		}	
		$user = $this->User->read();
		
		$rol = $this->User->Role->find('first',array('conditions'=>array('id'=>$user['User']['role_id'])));
		$isAdmin = true;
		if ($rol['Role']['name'] != 'SuperAdmin')
			$isAdmin = false;
		
		try {
			$email = new CakeEmail();
			$email->to($user['User']['email'])
				  ->replyTo('no-reply@'.$_SERVER['SERVER_NAME'])
				  ->from(array('no-reply@'.$_SERVER['SERVER_NAME']=>Configure::read('appname')))
				  ->emailFormat('html')
				  ->viewVars(compact('user', 'newPass', 'isAdmin'));
				  
			if ($this->params['action'] == 'admin_add') {
				$email->subject('Nuevo Usuario')
					  ->template('new_user', 'default');
			} else {
				$email->subject('Nuevo Password')
					  ->template('send_password', 'default');
			}
			$email->send();
			
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}	

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 1;
		
		//pr($this->viewVars['condos_usuario']);
		//$this->User->bindModel(array('hasOne' => array('Condo')));
		$this->User->bindModel(array('hasOne' => array('UsersRole','CondosUser')), false);
		//$this->User->bindModel(array('hasOne' => array('CondosUser')), false);
		//pr($this->viewVars['condos_ids']);
		if($this->Session->read("Auth.User.Role.code")=="SADM"){
			$filtro = array();
		} else {
			$filtro = array(
				'CondosUser.condo_id' =>$this->viewVars['condos_ids'],
				'UsersRole.role_id'	=> array('5','6')
			);
		}

		if($this->request->is('post')){
			if(isset($this->request->data['User']['role_id']) && $this->request->data['User']['role_id'] > 0){
				$filtro[] = array('UsersRole.role_id'=>$this->request->data['User']['role_id']);
			}
		}
		
		$users = $this->paginate('User', $filtro);
		//pr($users);
		$this->set('users', $users);
		
		
		if($this->Session->read("Auth.User.Role.code")=="ADM"){
			$this->set('roles',$this->User->Role->find('list', array("conditions"=>array("code"=>array("ADM","CSJ")))));
		} else {
			$this->set('roles',$this->User->Role->find('list'));
		}
	}
	
	public function admin_propietary(){
		$this->set('users',$this->paginate('User',array('Role.code'=>'PROP')));
	}
	
/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
        $this->view($id);
        $this->render('view');
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			
			if ($this->User->save($this->request->data)) {
				
				$new_user_role = array();
				foreach($this->request->data['UsersRole']['role_id'] as $key=>$role_id){
					$new_user_role[$key]['UsersRole']['role_id'] = $role_id;
					$new_user_role[$key]['UsersRole']['user_id'] = $this->User->id;
				}
				if(!empty($new_user_role)){
					$this->User->UsersRole->saveMany($new_user_role);
				}
				
				$new_condo_user = array();
				foreach($this->request->data['CondosUser']['condo_id'] as $key=>$condo_id){
					$new_condo_user[$key]['CondosUser']['condo_id'] = $condo_id;
					$new_condo_user[$key]['CondosUser']['user_id'] = $this->User->id;
				}
				if(!empty($new_condo_user)){
					$this->User->CondosUser->saveMany($new_condo_user);
				}
				
				$this->loadModel('Notification');
				$notification_data = array(
					'name'=>'Nuevo usuario registrado',
					'description'=>'El usuario '.$this->User->field('name').' '.$this->User->field('father_surname').' ha sido registrado.',
					'type'=>'user-add'
				);
				$this->Notification->save($notification_data);
				
				$this->Session->setFlash(__('Usuario agregado exitosamente'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				//debug($this->User->validationErrors);
				$this->Session->setFlash(__('Usuario no pudo ser agregado. Intentalo nuevamente'), 'flash_error');
			}
		}

		if($this->Session->read("Auth.User.Role.code") == 'ADM'){
			$roles = $this->User->Role->find('list', array("conditions"=>array("code"=>array("ADM","CSJ"))));
		} else {
			$roles = $this->User->Role->find('list');
		}

		$condos = $this->User->Condo->find('list');
		$this->set(compact('roles','condos'));

		$this->loadModel('Country');

		$countries = $this->Country->find('list', array('order'=>'short_name ASC'));
		
		$country = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
		$country_sel = $this->Country->findByIso2($country['geoplugin_countryCode']);
		$this->set('country_selected', $country_sel['Country']['id']);
		$this->set('callingCode','+'.$country_sel['Country']['calling_code'].'9');
		//$condos = $this->Owner->Condo->find('list', array('conditions'=>$filtro));
		//$houses = $this->Owner->House->find('list');
		$this->set(compact('condos', 'houses','countries'));

	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        
		if (!$this->User->exists($id)) {
			   throw new NotFoundException(__('Usuario invalido'));
		}
		
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				
				$new_user_role = array();
				$this->User->UsersRole->deleteAll(array('user_id'=>$id));
				foreach($this->request->data['UsersRole']['role_id'] as $key=>$role_id){
					$new_user_role[$key]['UsersRole']['role_id'] = $role_id;
					$new_user_role[$key]['UsersRole']['user_id'] = $this->User->id;
				}
				if(!empty($new_user_role)){
					$this->User->UsersRole->saveMany($new_user_role,array('deep' => true));
				}
				
				$new_user_condo = array();
				$this->User->CondosUser->deleteAll(array('user_id'=>$id));
				foreach($this->request->data['CondosUser']['condo_id'] as $key=>$condo_id){
					$new_user_condo[$key]['CondosUser']['condo_id'] = $condo_id;
					$new_user_condo[$key]['CondosUser']['user_id'] = $this->User->id;
				}
				if(!empty($new_user_condo)){
					$this->User->CondosUser->saveMany($new_user_condo,array('deep' => true));
				}
				
				$this->Session->setFlash(__('El usuario ha sido editado exitosamente'), 'flash_success');
				$this->redirect(array('action' => 'index'));
				
			} else {
				$this->Session->setFlash(__('Usuario no modificado. Intentalo nuevamente'), 'flash_error');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		
		$condos = $this->User->Condo->find('list');
		$roles = $this->User->Role->find('list');
		
		$this->User->Condo->bindModel(array('hasOne' => array('CondosUser')), false);
		$condos_user = $this->User->Condo->find('all', array('conditions'=>array('CondosUser.user_id'=>$id)));
		//pr($condos_user);
		$condos_selected = array();
		foreach($condos_user as $condo){
			$condos_selected[] = $condo['Condo']['id']; 
		}
		//pr($condos_selected);exit;
		$this->User->Role->bindModel(array('hasOne' => array('UsersRole')), false);
		$roles_user = $this->User->Role->find('all', array('conditions'=>array('UsersRole.user_id'=>$id)));
		//pr($roles_user);
		$roles_selected = array();
		foreach($roles_user as $role){
			$roles_selected[] = $role['Role']['id']; 
		}
		//pr($roles_selected);
		$this->set(compact('condos', 'roles','roles_selected','condos_selected'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
				throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('Usuario eliminado'),'flash_success');
			$this->loadModel('Notification');
			$notification_data = array(
				'name'=>'Usuario eliminado',
				'description'=>'El usuario '.$this->User->field('name').' '.$this->User->field('father_surname').' ha sido eliminado.',
				'type'=>'user'
			);
			$this->Notification->save($notification_data);
			
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Usuario no eliminado. Intentelo nuevamente'),'flash_error');
		$this->redirect(array('action' => 'index'));
	}
        
	public function admin_profile(){
		$this->profile();
		$this->render('profile');
	}
	//Dar permisos en tabla de usuarios
	public function admin_changeRole($user_id = null, $role_id = null, $action = null){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			
			$this->User->UsersRole->recursive = 0;
			$results = $this->User->UsersRole->find('first', array('conditions'=>array('UsersRole.user_id'=>$user_id,'UsersRole.role_id'=>$role_id)));
			$data = array('user_id'=>$user_id, 'role_id'=>$role_id);
			
			if(!empty($results) && $action == 'add'){
				$this->User->UsersRole->id = $results['UsersRole']['id'];
				$this->User->UsersRole->read(null,$this->User->UsersRole->id);
				$this->User->UsersRole->save($data);
			} else if(!empty($results) && $action == 'delete'){
				$this->User->UsersRole->id = $results['UsersRole']['id'];
				//$this->User->UsersRole->read(null,$this->User->UsersRole->id);
				$this->User->UsersRole->delete($this->User->UsersRole->id);
			} else if ($action == 'add'){
				$this->User->UsersRole->save($data);
			}
		}
	}
	//Cambiar de Perfil al usuario logueado
	public function changeRole($role_id = null){
		$this->autoRender = false;
		$this->User->Role->id = $role_id;
		if (!$this->User->Role->exists()) {
			throw new NotFoundException(__('Perfil Inválido'));
		}
		
		$this->User->id = $this->Session->read('Auth.User.id');
		$this->User->saveField('role_id',$role_id);
		
		$this->User->Role->id = $role_id;
		$role = $this->User->Role->read(null, $role_id);
		
		$this->request->onlyAllow('post', 'delete');
		
		$user = $this->User->findById($this->User->id);
		$user = $user['User'];
		$this->Auth->login($user);
		
		$this->Session->write('Auth.User.Role', $role['Role']);
		
		if(in_array($this->Session->read('Auth.User.Role.code'), Configure::read('AppAdmin')))
			$this->redirect(array('controller'=>'invitations', 'action' => 'dashboard', 'admin'=>true));
		else 
			$this->redirect(array('controller'=>'contacts','action' => 'index', 'admin'=>false));
	}

	public function admin_owners(){
		$this->User->recursive = 1;
		
		//pr($this->viewVars['condos_usuario']);
		//$this->User->bindModel(array('hasOne' => array('Condo')));
		//$this->User->bindModel(array('hasOne' => array('UsersRole','CondosUser')), false);
		//$this->User->bindModel(array('hasOne' => array('CondosUser')), false);
		//pr($this->viewVars['condos_ids']);
		/*$filtro = array(
			'CondosUser.condo_id' =>$this->viewVars['condos_ids'],
			'UsersRole.role_id'	=> array('7')
		);*/
		$filtro = array(
			'OR'=>array(
				'House.condo_id' => $this->viewVars['condos_ids'],
				'Student.school_id' => $this->viewVars['condos_ids']
			),
			'User.role_id' => 7
		);
		$houses = $condo = array();
		//pr($this->params->querycondo_id);
		if($this->request->is('post')){

			//Reinicio paginador en caso que uno de los filtros haya cambiado
			if(
				($this->request->data['User']['condo_id']!=$this->Session->read('filter_condo_id')) OR
				($this->request->data['User']['house_id']!=$this->Session->read('filter_house_id')) OR
				($this->request->data['User']['search_name']!=$this->Session->read('filter_search_name'))
			){
				$this->request->params['named']['page'] = 1;
			}

			$filtro = array();
			$this->Session->write('filter_house_id',''); 
			$this->Session->write('filter_condo_id','');
			$this->Session->write('filter_search_name',''); 
			if(isset($this->request->data['User']['condo_id']) && $this->request->data['User']['condo_id'] != ''){
				$filtro[] = array(
					'OR'=> array( 
						'House.condo_id'=>$this->request->data['User']['condo_id'],
						'Student.school_id'=>$this->request->data['User']['condo_id'],
					),
					'HousesUser.active' => 1
				);
				$this->Session->write('filter_condo_id', $this->request->data['User']['condo_id']);
				$condo = $this->User->House->Condo->read(null, $this->request->data['User']['condo_id']);
			}

			if(isset($this->request->data['User']['house_id']) && $this->request->data['User']['house_id'] != ''){
				$filtro[] = array(
					'OR'=>array(
						'HousesUser.house_id' => $this->request->data['User']['house_id'],
						'HousesUser.student_id' => $this->request->data['User']['house_id']
					)
				);
				$this->Session->write('filter_house_id', $this->request->data['User']['house_id']);
			}

			if(isset($this->request->data['User']['search_name']) && $this->request->data['User']['search_name']!=''){
				$filtro[] = array(
					'OR' => array(
						'User.name LIKE'=>'%'.$this->request->data['User']['search_name'].'%',
						'User.father_surname LIKE'=>'%'.$this->request->data['User']['search_name'].'%'
					)
				);
				$this->Session->write('filter_search_name', $this->request->data['User']['search_name']);
			}

			
			//echo "house_id".$this->Session->read('filter_house_id').'<br/>';
		} else {
			if($this->Session->read('filter_search_name')!=''){
				$filtro[] = array(
					'OR' => array(
						'User.name LIKE'=>'%'.$this->Session->read('filter_search_name').'%',
						'User.father_surname LIKE'=>'%'.$this->Session->read('filter_search_name').'%'
					)
				);
			}

			if($this->Session->read('filter_condo_id')!=''){
				$filtro[] = array(
					'OR'=> array( 
						'House.condo_id'=>$this->Session->read('filter_condo_id'),
						'Student.school_id'=>$this->Session->read('filter_condo_id'),
					),
					'HousesUser.active' => 1
				);
				$condo = $this->User->House->Condo->read(null, $this->Session->read('filter_condo_id'));
			}

			if($this->Session->read('filter_house_id')!=''){
				$filtro[] = array(
					'OR'=> array(
						'HousesUser.house_id' => $this->Session->read('filter_house_id'),
						'HousesUser.student_id' => $this->Session->read('filter_house_id')
					)
				);
			}
		}
		//pr($filtro);
		if(!empty($condo)){
			if(!$condo['Condo']['type']){
				$houses = $this->User->House->find('list', array(
					'conditions'=>array(
						'condo_id'=>$condo['Condo']['id']
					),
					'order'=>array('name ASC')
				));
				$houses = array(''=>'Todos') + $houses;
			} else {
				$houses = $this->User->Student->find('list', array(
					'conditions'=>array(
						'Student.school_id'=>$condo['Condo']['id']
					),
					'order'=>array('full_name ASC')
				));
				$houses = array(''=>'Todos') + $houses;
			}
		}

		$users = $this->paginate('HousesUser', $filtro);
		
		$this->set('users', $users);
		$this->set(compact('houses'));
		
	}

	public function admin_addowner(){
		$houses = array();
		$this->loadModel('Country');
		
		if ($this->request->is('post')) {
			
			$condo = $this->User->Condo->findById($this->request->data['User']['condo_id']);
			$house_id = $this->request->data['User']['house_id'];
			
			if($condo['Condo']['type']==0){
				$this->request->data['User']['student_id'] = NULL;
			} else {
				$this->request->data['User']['student_id'] = $this->request->data['User']['house_id'];
				$this->request->data['User']['house_id'] = NULL;
			}
				
			$house = $this->User->House->findById($house_id);
			$user = $this->User->findByMobile($this->request->data['User']['mobile']);
			
			$country = $this->Country->findById($this->request->data['User']['country_id']);
			
			if(empty($user)){
				try{
					$opts = array( 'http'=>array( 'user_agent' => 'PHPSoapClient'));
					$context = stream_context_create($opts);
					$client = new SoapClient(Configure::read('Ws.url'),
											 array('stream_context' => $context,
												   'cache_wsdl' => WSDL_CACHE_NONE));
					
					/*$client = new SoapClient('http://app.safecard.cl/soap/WsMobile/wsdl',
											 array('stream_context' => $context,
												   'cache_wsdl' => WSDL_CACHE_NONE));*/
						

					$result = $client->soap_AddUser(array(
						'mobile'=> $this->request->data['User']['mobile'],
						'iso2'=> $country['Country']['iso2'],
						'name' => $this->request->data['User']['name'],
						'father_surname' => $this->request->data['User']['father_surname'],
						'pin' => '1234',
						'device' => NULL
					));
					
					if($result->result == 'ACK'){
						$user = $this->User->findByMobile($this->request->data['User']['mobile']);
					} else {
						$user = array();
					}
				} catch(Exception $e) {
					echo "Error WS: ".$e->getMessage();
					die();
				}
			}
			
			if(!empty($user)){
				
				$houses_user = $this->User->HousesUser->find('first', array(
					'conditions'=>array(
						'HousesUser.user_id'=>$user['User']['id'],
						'HousesUser.house_id'=>$house_id,
						'HousesUser.active' => 1
					)
				));

				if(empty($houses_user)){
					$flag = true;

					$data_condo_user = array(
						'condo_id'	=> $condo['Condo']['id'],
						'user_id'	=> $user['User']['id']
					);

					$condo_user = $this->User->CondosUser->find('first', array('conditions'=> $data_condo_user));

					if(empty($condo_user)){
						$this->User->CondosUser->create();
						$this->User->CondosUser->save($data_condo_user);
					} 
					
					if($flag){
						$this->User->HousesUser->create();
						$data['HousesUser'] = array(
							'user_id'=>$user['User']['id'], 
							'house_id'=> ($condo['Condo']['type']==0) ? $house_id:NULL,
							'student_id'=> ($condo['Condo']['type']==1) ? $house_id:NULL,
							'owner'=>1,
							'active'=>1
						);
						$this->User->HousesUser->save($data);
					}
					
					//Creación de invitación para propietario
					$this->User->Event->create();
					
					$data_event = array(
						'user_id' => $user['User']['id'],
						'house_id'=> ($condo['Condo']['type']==0) ? $house_id:NULL,
						'student_id'=> ($condo['Condo']['type']==1) ? $house_id:NULL,
						'from'=> date('Y-m-d H:i:s'),
						'to' => '2038-01-19 00:00:00'
					);
					if(!$this->User->Event->save($data_event)){
						pr($data_event);
						debug($this->User->Event->validationErrors);
					}
					
					$this->User->Invitation->create();
					$data_invitation = array(
						'event_id' => $this->User->Event->id,
						'organizer_id' => $user['User']['id'],
						'guest_id' => $user['User']['id'],
						'relation_id' => 1,
						'condo_id' => $this->request->data['User']['condo_id'],
						'house_id'=> ($condo['Condo']['type']==0) ? $house_id:NULL,
						'student_id'=> ($condo['Condo']['type']==1) ? $house_id:NULL,
						'mobile' => $user['User']['mobile']
					);
					
					$this->User->Invitation->save($data_invitation);
					$this->User->Invitation->saveCode($this->User->Invitation->id);
				} else {
					$this->Session->setFlash(__('Número de celular ya está vinculado con la propiedad.'),'flash_error');
					$this->redirect($this->referer());
				}
			}
			
			$this->Session->setFlash(__('Propietario guardado exitosamente'),'flash_success');
			$this->redirect($this->referer());
			
			$houses = $this->User->House->find('list',array('conditions'=>array('condo_id'=>$this->request->data['User']['condo_id'])));
		}
		
		$filtro = array();
		if($this->Session->read('Auth.User.Role.code') == 'CSJ' || $this->Session->read('Auth.User.Role.code') == 'ADM'){			
			$condos_id = ClassRegistry::init('CondosUser')->getCondominios();
			$filtro = array('Condo.id'=>$condos_id);
		}
		
		$countries = $this->Country->find('list', array('order'=>'short_name ASC'));
		
		$country = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
		$country_sel = $this->Country->findByIso2($country['geoplugin_countryCode']);
		$this->set('country_selected', $country_sel['Country']['id']);
		$this->set('callingCode','+'.$country_sel['Country']['calling_code'].'9');
		$condos = $this->User->Condo->find('list', array('conditions'=>$filtro));
		//$houses = $this->Owner->House->find('list');
		$this->set(compact('condos', 'houses','countries'));
	}

	public function admin_unlink_owner($user_id = null, $house_id = null){
		if($this->request->is('post')){
			$this->User->bindModel(array('hasOne'=>array('HousesUser')), false);

			$filtro = array(
				'HousesUser.house_id' => $house_id,
				'HousesUser.user_id' => $user_id 
			);

			$propiedad = $this->User->find('first', array(
				'conditions'=> $filtro
			));

			if(!empty($propiedad)){

				$update = $this->User->HousesUser->updateAll(
					array(
						'HousesUser.active' => 0,
						'HousesUser.modified' => "'".date('Y-m-d H:i:s')."'"
					),
					array(
						'HousesUser.house_id' => $house_id,
						'OR'=>array(
							'HousesUser.user_id'=>$user_id, 
							'HousesUser.responsible_id'=>$user_id
						)
					)
				);

				if($update){

					$residentes = $this->User->HousesUser->find('all', array(
						'conditions'=>array('HousesUser.responsible_id'=>$user_id)
					));
					
					if(!empty($residentes)){
						foreach ($residentes as $key => $residente) {
							$this->User->Invitation->updateAll(
								array('Invitation.status'=>'-1'),
								array(
									'Invitation.organizer_id'	=>$residente['HousesUser']['user_id'],
									'Invitation.house_id'		=>$house_id
								)
							);
						}
					}
					
					$this->User->Invitation->updateAll(
						array('Invitation.status' => '-1'),
						array(
							'Invitation.organizer_id' => $user_id,
							'Invitation.house_id' => $house_id
						)
					);

					$this->Session->setFlash(__('Propietario desvinculado exitosamente'),'flash_success');

				} else {
					$this->Session->setFlash(__('Propietario no desvinculado. Inténtelo nuevamente'),'flash_error');
				}
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('No existe vinculo entre propietario y propiedad.'),'flash_error');
				$this->redirect($this->referer());
			}
		}
	}

	public function admin_sms(){

		if($this->request->is('post') || $this->request->is('put')){

			$this->User->bindModel(array('hasOne' => array('CondosUser','UsersRole')), false);
			$this->User->Condo->id = $this->request->data['User']['condo_id'];

			$owners = $this->User->find('all', array(
				'conditions'=>array(
					'CondosUser.condo_id'=>$this->request->data['User']['condo_id'],
					'UsersRole.role_id' => 7
				))
			);

			foreach($owners as $key=>$owner){
				$ch = curl_init();
				//extract data from the post
				extract($_POST);
				//[Nombre Propietario] te invitó a [casa-condominio]. Ingresa descargando Safecard en Google Play y App Store o digita XXXXYYYY en el acceso
				$text = stripAccents(str_replace('|NAME|', $owner['User']['name'], $this->request->data['User']['text']));
				//echo $text; exit;
				//set POST variables
				$url = 'http://sms.sistek.cl/sms.asmx/Sms';
				
				$fields = array(
					'id' => Configure::read('IdSMS'),
					'text' => $text,
					'to_number' => str_replace('+','',$owner['User']['mobile']),
					'username' => Configure::read('UsernameSMS'),
					'password' => Configure::read('PassSMS'),
					'type'=>'SMS'
				);
				
				$fields_string = http_build_query($fields);
				
				//set the url, number of POST vars, POST data
				curl_setopt($ch,CURLOPT_URL, $url);
				curl_setopt($ch,CURLOPT_POST, count($fields));
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

				//execute post
				$result = curl_exec($ch);

				//close connection
				curl_close($ch);
				sleep(3);
			}
			$this->Session->setFlash(__('SMS enviados'),'flash_success');
		} //else {
			$this->set('condos', $this->User->Condo->find('list', array('conditions'=>array('id'=>$this->viewVars['condos_ids']))));
		//}
	}

	public function getCallingCodeByCountry($country_id = null) {
		$this->autoRender = false;
        $this->layout = false;
        if($this->request->is('ajax')){
            $countries = array();
            $this->loadModel('Country');
            if($this->Country->exists($country_id)){
				$country = $this->Country->read(null, $country_id);
				$callingCode = array('callingCode'=>'+'.$country['Country']['calling_code'].'9');
            }
            echo json_encode($callingCode);
        }
    }
	
}
